// partial application

// with implicitly declared closure
func createAdder0(x:Int) -> (Int) -> Int {
    var value = x // state keeping
    return { value = value + $0; return value }
}

// with exlicitly declared function ("named closure")
func createAdder1(x:Int) -> (Int) -> Int {
    func add(_ x:Int) -> (Int) -> Int {
        var x = x // state keeping
        return { y in
            x = x + y
            return x
        }
    }
    return add(x)
}

let adder0 = createAdder0(x: 5)
let adder1 = createAdder1(x: 5)
print(adder0(4)) // observe: state kept between calls
print(adder1(4)) // observe: state kept between calls
print(adder0(4)) // observe: state kept between calls
print(adder1(4)) // observe: state kept between calls

// currying

// with implicitly declared closure
func add0(_ x:Int) -> (_ y:Int) -> Int {
    { y in
        x + y
    }
}

// with exlicitly declared function ("named closure")
func add1(_ x:Int) -> (_ y:Int) -> Int {
    func plus(_ x:Int,_ y:Int) -> Int { x + y }
    return { y in plus(x,y) }
}

print(add0(3)(2)) // observe: no state kept between calls
print(add0(3)(2)) // observe: no state kept between calls
print(add1(3)(2)) // observe: no state kept between calls
print(add1(3)(2)) // observe: no state kept between calls



// Generic Stack using partial application anf implicilty declated closures in a tuple
typealias Stack<T> = (push:(T) -> (), pop:() -> (T?))
func createStack<T>() -> Stack<T> {
    var array:[T] = [];
    return (push: { array.append($0) },
             pop: { array.count > 0 ? array.remove(at: array.count - 1) : nil })
}

let s:Stack<Int> = createStack()
s.push(1)
s.push(2)
s.push(3)
