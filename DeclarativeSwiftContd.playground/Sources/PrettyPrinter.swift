import Foundation

public struct PrettyPrinter: TextOutputStream {
    public init (){}
    public mutating func write(_ string: String) {
        print("\(string.replacingOccurrences(of: "__lldb_expr_(.)+\\.", with: "", options: .regularExpression))",terminator: "")
    }
}
