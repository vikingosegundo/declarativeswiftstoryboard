public protocol TypedIdentifiable: Identifiable {
    associatedtype IDType: Hashable&Codable
    associatedtype ID = Identifier<IDType, Self>
}
extension TypedIdentifiable {
    fileprivate static var hasherInput: String { .init(describing: self) }
}
public struct Identifier<Value: Hashable&Codable, _Owner: TypedIdentifiable>: Hashable {
    public init(value v:Value) { value = v }
    public func hash(into hasher: inout Hasher) {
        hasher.combine(_Owner.hasherInput)
        hasher.combine(value)
    }
    public var _raw: Value { value }
    private let value: Value
}
extension Identifier: Codable {}
