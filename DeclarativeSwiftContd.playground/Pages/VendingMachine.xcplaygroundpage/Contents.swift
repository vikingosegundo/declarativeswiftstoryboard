//: [Previous](@previous)

struct VendingMachine {
    enum Change {
        case to(State)
        case cancel
    }
    enum State {
        case idle
        case coinInserted
        case fetching
        case serving
    }
    init() {
        self.init(.idle)
    }
    func alter(_ c:Change...) -> Self { c.reduce(self) { $0.alter($1) } }
    private
    func alter(_ c:Change) -> Self {
        switch c {                        // from state -> to state
        case let .to(d) where (state,d) == (.idle        ,.coinInserted): return Self(d)
        case let .to(d) where (state,d) == (.coinInserted,.fetching    ): return Self(d)
        case let .to(d) where (state,d) == (.fetching    ,.serving     ): return Self(d)
        case let .to(d) where (state,d) == (.serving     ,.idle        ): return Self(d)
        case    .cancel where  state    !=  .serving                    : return Self(.idle)
        case  _                                                         : return self
        }
    }
    private init(_ s:State) {
        state = s
    }
    let state:State
}


let vm = VendingMachine()
    .alter(
        .to(.coinInserted),
        .to(.fetching),
        .to(.idle), // ignored
        .to(.serving)
    )
print(vm.state)
//: [Next](@next)
