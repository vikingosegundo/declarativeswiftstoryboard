//: [Previous](@previous)
import Foundation

extension String {
    static func shuffeld(length: Int, alphabet:String = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789") -> String? {
        guard
            alphabet.count > 0
                && length > 0
        else { return nil }
        return alphabet.shuffled().prefix(length).map {String($0)}.joined()
    }
}

extension String {
    static func random(length: Int, alphabet:String = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789") -> String? {
        return shuffeld(length: length, alphabet: (0..<length).reduce("") { partialResult, _ in
            partialResult + alphabet
        })
    }
}


print(String.shuffeld(length: 15)!)
print(String.shuffeld(length: 3, alphabet: "abcdefg")!)

let lowerCaseAlphabet = Array(97...122).reduce("", {
    $0.appending(String(Character(UnicodeScalar($1))))
})
print(String.shuffeld(length: 26, alphabet: lowerCaseAlphabet)!)
print(String.random(length: 100)!)
print(String.random(length: 10, alphabet: "01")!)
print(String.random(length: 10, alphabet: "🙂☹️")!)

//: [Next](@next)
