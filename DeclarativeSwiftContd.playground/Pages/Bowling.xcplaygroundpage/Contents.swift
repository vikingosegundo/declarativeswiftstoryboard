//: [Previous](@previous)

import Foundation

enum Bowling {
    struct Player: Identifiable {
        enum Change {
            case set(Name); enum Name {
                case name(String)
            }
        }
        
        init() { self.init(UUID(), "") }
        private init(_ i:UUID,_ n:String) { id = i; name = n}
        
        let id  : UUID
        let name: String
        
        func alter(_ c:Change...) -> Self { c.reduce(self) { $0.alter($1) } }
        private
        func alter(_ c:Change) -> Self {
            switch c {
            case .set(.name(let n)): return Self(id,n)
            }
        }
    }
}
extension Bowling {
    enum Roll {
        case points(first:Int,second:Int)
        case strike
        case spare(first:Int)
    }
}
extension Bowling {
    struct PlayerPoints {
        enum Change {
            case add(_Roll); enum _Roll {case roll(Roll)}
        }
        
        init(player p:Player) { self.init(p, []) }
        private
        init(_ p:Player,_ r:[Roll]) { player = p; rolls = r }
        
        let player: Player
        let rolls : [Roll]
        func alter(_ c:Change) -> Self {
            switch c {
            case .add(.roll(let r)): return Self(player, rolls + [r])
            }
        }
    }
}

typealias Err = Error
extension Bowling {
    enum Error:Err {
        case cannotAdd(player:Player,toStarted:Game)
        case alreadyStarted(Game)
        case notOngoing(Game)
        case noPlayers(Game)
    }
}
extension Bowling {
    struct Game {
        enum State {
            case notStarted
            case ongoing
            case ended
        }
        enum Change {
            case add(Add); enum Add {
                case player(Player)
                case error(handler:(Error) -> ())
            }
            case start(It); enum It {
                case it
            }
            case execute((Game) -> ())
            case rolled(Roll)
            case register(Register); enum Register {}
        }
        private
        init(_ p:[Player],_ s:State,_ idx:Int, _ ps:[PlayerPoints],_ eh:[(Error) -> ()]) {
            players      = p
            state        = s
            playerIndex  = idx
            rolls        = ps
            errorHandler = eh
        }
        init() { self.init([], .notStarted, 0, [], [])}
        let state  : State
        let players: [Player]
        let rolls : [PlayerPoints]
        var current: Player? { guard !players.isEmpty else { return nil }; return players[ playerIndex    % players.count] }
        var next   : Player? { guard !players.isEmpty else { return nil }; return players[(playerIndex+1) % players.count] }
        
        private let errorHandler: [(Error) -> ()]
        private let playerIndex : Int
        private var nextPlayer  : Int { (playerIndex+1) % players.count }
        struct SubtotalMultiplier {
            let subtotal:Int
            let multiplier:(Int,Int)
        }

        func alter(_ c:Change...) -> Self { c.reduce(self) { $0.alter($1) } }
        func score(for player: Player) -> Int {
            return subtotal(from:(rolls.first(where: { $0.player.id == player.id }) ?? .init(player: player)).rolls.reduce(SubtotalMultiplier(subtotal: 0, multiplier:(0,0))) {
                var subtotal = $0.subtotal
                var multipliers = ($0.multiplier.0, $0.multiplier.1)
                switch $1 {
                case .strike:
                    subtotal += 10 * multipliers.0
                    multipliers = (multipliers.1 + 1, 2)
                case .spare(let first):
                    let second = 10 - first
                    subtotal += first * multipliers.0 + (second * multipliers.1)
                    multipliers = (2, 1)
                case .points(let first, let second):
                    subtotal += first * multipliers.0 + (second * multipliers.1)
                    multipliers = (multipliers.1, 1)
                case .bonusRoll(let first, let second):
                    subtotal += first * (multipliers.0 - 1) + (second ?? 0) * (multipliers.1 - 1)
                    multipliers = (0, 0) // game over
                }
                return SubtotalMultiplier(subtotal:subtotal, multiplier:(multipliers.0,multipliers.1))
            }) 
        }
        
        private
        func alter(_ c: Change) -> Self {
            switch (c,state,!players.isEmpty) {
            case let (.execute(callback)      ,  _        , _   ): callback(self)                                   ; return self
            case let (.add(.error(handler:eh)),  _        , _   ):                                                    return add(errorHandler:eh)
            case let (.add(.player(p))        ,.notStarted, _   ):                                                    return add(player:p)
            case let (.add(.player(p))        ,  _        , _   ): handle(error:.cannotAdd(player:p,toStarted:self)); return self
            case     (.start(.it)             ,.notStarted,true ):                                                    return startIt()
            case     (.start(.it)             ,.notStarted,false): handle(error:.noPlayers(self))                   ; return self
            case     (.start(.it)             ,  _        , _   ): handle(error:.alreadyStarted(self))              ; return self
            case let (.rolled(r)              ,.ongoing   ,true ):                                                    return rolled(r)
            case     (.rolled                 ,  _        , _   ): handle(error:.notOngoing(self))                  ; return self
            }
        }
        private func add    (player             p:Player                    ) -> Self { Self(players+[p],state   ,playerIndex,rolls     ,errorHandler    ) }
        private func startIt(                                   ) -> Self { Self(players    ,.ongoing,playerIndex,rolls     ,errorHandler    ) }
        private func rolled (_            r:Roll                ) -> Self { Self(players    ,state   ,nextPlayer ,add(roll:r),errorHandler    ) }
        private func add    (errorHandler h:@escaping(Error)->()) -> Self { Self(players    ,state   ,playerIndex,rolls     ,errorHandler+[h]) }

        private func add(roll r:Roll    ) -> [PlayerPoints] { rolls.filter{ $0.player.id != current!.id } + [ (rolls.first(where:{ $0.player.id == current!.id}) ?? PlayerPoints(player:current!)).alter(.add(.roll(r))) ] }
        private func handle (error:Error)                   { errorHandler.forEach{ $0(error) } }
    }
}
func subtotal(from:Bowling.Game.SubtotalMultiplier) -> Int {
    from.subtotal
}
func points(from r:Bowling.Roll) -> (Int,Int) {
    switch r {
    case let .points(first:f,second:s): return ( f,   s)
    case let .spare (first:f         ): return ( f,10-f)
    case     .strike                  : return (10,   0)
    }
}

let p0 = Bowling.Player().alter(.set(.name("Manuel")))
let p1 = Bowling.Player().alter(.set(.name("Chuck" )))
let p2 = Bowling.Player().alter(.set(.name("Noris" )))
let p3 = Bowling.Player().alter(.set(.name("Pieter")))
let g = Bowling.Game()
    .alter(
        .add(.error(handler:{ print("\nERROR: \($0)\n") })),
        .add(.player(p0)),
        .add(.player(p1)),
        .add(.player(p2)),
            .execute({ print("Noris is present: \($0.players.contains(where: {$0.id == p2.id})) ") }),
        .rolled(.points(first:1,second:7)),  // error: notOngoing
        .start(.it),
        .start(.it),                         // error: alreadyStarted
        .add(.player(p3)),                   // error: cannot add players to started game
        .rolled(.points(first:1,second:7)),  // Manuel
        .rolled(.strike),                    // Chuck
        .rolled(.spare(first:9)),            // Noris
        .rolled(.strike),                    // Manuel
            .execute({ print("\(Array(zip($0.rolls.map(\.player.name), $0.rolls.map(\.rolls))))") }),
            .execute({ print("correct points of rolls for \($0.players.map(\.name).joined(separator: ", ")):")  }),
            .execute({ print("\t* Manuel: \($0.rolls.first(where:{$0.player.id == p0.id})!.rolls.count == 2) \($0.rolls.filter{$0.player.id == p0.id}.map(\.rolls).flatMap{ $0.map{ (points(from:$0)) } })") }),
            .execute({ print("\t*  Chuck: \($0.rolls.first(where:{$0.player.id == p1.id})!.rolls.count == 1) \($0.rolls.filter{$0.player.id == p1.id}.map(\.rolls).flatMap{ $0.map{ (points(from:$0)) } })") }),
            .execute({ print("\t*  Noris: \($0.rolls.first(where:{$0.player.id == p2.id})!.rolls.count == 1) \($0.rolls.filter{$0.player.id == p2.id}.map(\.rolls).flatMap{ $0.map{ (points(from:$0)) } })") }))


//: [Next](@next)
