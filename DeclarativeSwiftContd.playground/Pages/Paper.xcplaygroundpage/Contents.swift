//: [Previous](@previous)
import SwiftUI

let p = Paper(title:"Manus Paper")
    .alter(
        .add(
            .chapter(.init(title:"Introduction")
                .alter(
                  .add(
                    .paragraph(.text(
"""
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam pulvinar lobortis nulla, eu egestas augue venenatis a. Morbi et lorem turpis. Nullam quis dignissim metus. Phasellus efficitur tincidunt nunc et egestas. Morbi sollicitudin dolor a ex pretium, eget volutpat libero egestas. Etiam non suscipit ex, quis convallis elit. Praesent ut tristique lorem. Pellentesque vitae blandit lacus, vitae blandit tortor. Nam pretium vitae turpis eu egestas. Cras lacinia quam non tellus imperdiet sagittis. Donec vitae lectus mattis, ultricies odio in, consectetur tellus.
""", formatting: [.from(0,to:10,.bold)]))),
                  .add(
                    .paragraph(.text(
"""
Nunc scelerisque elit eu metus eleifend consectetur. Proin sapien nisl, convallis et viverra semper, iaculis ac nisl. In non nibh est. Vivamus massa tortor, feugiat eu mollis vel, rutrum vel felis. Phasellus sagittis eu mauris eu aliquam. Integer ante mauris, faucibus id eros sed, sagittis pretium nisl. Mauris eget velit nec massa scelerisque pulvinar.
""", formatting: [.from( 0,to:14,.bold),
                  .from(53,to:70,.italics)]))),
                  .add(
                    .paragraph(.text(
"""
Quisque nulla augue, posuere commodo sapien eu, aliquam gravida elit. Phasellus at est sapien. Aenean ut lectus feugiat, suscipit erat sed, placerat arcu. Nullam scelerisque laoreet arcu, sed facilisis augue sagittis vel. Donec scelerisque porttitor est quis aliquam. Maecenas accumsan tellus quis velit blandit vulputate. Phasellus consectetur nulla eu odio pretium, vitae faucibus elit eleifend. Phasellus consectetur quam sed purus consequat, ac tincidunt nisl faucibus. Interdum et malesuada fames ac ante ipsum primis in faucibus. Duis a dapibus dui, eu pharetra lectus. Donec accumsan ultrices tincidunt.
""", formatting:[.from(70,to:93,.bold)]))),
                    .add(
                        .paragraph(.text(
"""
Aliquam posuere, dolor a feugiat iaculis, felis tellus lobortis dolor, eget elementum leo magna sed sapien. Pellentesque bibendum tincidunt urna, quis congue est rutrum non. Donec at congue ligula. Vestibulum porttitor, nibh non sodales pharetra, diam nulla ultricies ligula, sit amet viverra augue elit nec ligula. Curabitur sed rutrum felis. Sed egestas congue risus at accumsan. Aenean sapien ante, dictum at ornare non, condimentum ac libero. Interdum et malesuada fames ac ante ipsum primis in faucibus. Fusce eu risus risus.
""", formatting: []))),
                    .add(
                        .paragraph(.code(
"""
struct Paper {
    enum Change {
        case setting(Setting); enum Setting {
            case title(String)
        }
        case add(Add); enum Add {
            case chapter(Chapter)
        }
        
    }
    let title: String
    let chapters: [Chapter]
    
    func alter(_ c:Change) -> Self {
        switch c {
        case let .setting(.title  (t)): return Self(t    , chapters      )
        case let .add    (.chapter(c)): return Self(title, chapters + [c])
        }
    }
    init(title t:String) { self.init(t, [])}

    private
    init(_ t:String,_ c:[Chapter]) { title = t; chapters = c  }
    
    struct Chapter {
        enum Change {
            case add(paragraph:Paragraph)
        }
        enum Paragraph {
            case text(String)
            case code(String)
            case list(String)
        }
        let title: String

        init(title: String) { self.init(title,[])}
        private
        init(_ t: String, _ c:[Paragraph]) { title = t; paragraphs = c }
        func alter(_ c:Change...) -> Self { c.reduce(self) { $0.alter($1) } }

        private
        func alter(_ c:Change) -> Self {
            switch c {
            case let .add(c): return Self(title,paragraphs + [c])
            }
        }
        
        let paragraphs:[Paragraph]
    }
}

"""
                            )))
                )
            )
        )
    )

print(p)

struct Paper {
    enum Change {
        case setting(Setting); enum Setting { case title  (String ) }
        case add    (Add    ); enum Add     { case chapter(Chapter) }
    }
    let title: String
    let chapters: [Chapter]
    
    func alter(_ c:Change) -> Self {
        switch c {
        case let .setting(.title  (t)): return Self(t    , chapters      )
        case let .add    (.chapter(c)): return Self(title, chapters + [c])
        }
    }
    init(title t:String) { self.init(t, [])}

    private
    init(_ t:String,_ c:[Chapter]) { title = t; chapters = c  }
    
    struct Chapter {
        enum Formatting {
            case normal
            case bold
            case italics
        }
        enum FormattingRange {
            case from(Int,  to:Int, Formatting)
        }
        enum Change {
            case add(Add); enum Add {
                case paragraph(Paragraph)
            }
        }
        enum Paragraph {
            enum List{
                enum Item {
                    case text(String)
                }
                case item(Item)
            }
            case text(String, formatting:[FormattingRange])
            case code(String)
            case list(List)
            var string:String {
                switch self {
                case .text            (let s,_): return s
                case .code            (let s  ): return s
                case .list(.item(.text(let s))): return s
                }
            }
            
            var attributedString:AttributedString {
                switch self {
                case let .text(s,formatting:a): return a.reduce(AttributedString(s)) { attrStr, format in
                    switch format {
                    case let .from(f,to:t,.bold   ): return { let r=Range(NSRange(location:f,length:t-f),in:attrStr)!;var a=attrStr;a[r].font = .boldSystemFont(ofSize: 17);return a}()
                    case let .from(f,to:t,.normal ): return { let r=Range(NSRange(location:f,length:t-f),in:attrStr)!;var a=attrStr;a[r].font =     .systemFont(ofSize: 17);return a}()
                    case let .from(f,to:t,.italics): return { let r=Range(NSRange(location:f,length:t-f),in:attrStr)!;var a=attrStr;a[r].font =  .system(size: 17).italic();return a}()
                    }
                }
                case let .list(.item(.text(s))): return AttributedString(s)
                case let .code            (s)  : return AttributedString(s)
                }
            }
        }
        let title: String

        init(title: String) { self.init(title,[])}
        private
        init(_ t: String, _ c:[Paragraph]) { title = t; paragraphs = c }
        func alter(_ c:Change...) -> Self { c.reduce(self) { $0.alter($1) } }

        private
        func alter(_ c:Change) -> Self {
            switch c {
            case let .add(.paragraph(p)): return Self(title,paragraphs + [p])
            }
        }
        
        let paragraphs:[Paragraph]
    }
}
//: [Next](@next)
