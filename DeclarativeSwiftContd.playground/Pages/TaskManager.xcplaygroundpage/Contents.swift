//: [Previous](@previous)

import Foundation

struct Project: TypedIdentifiable {
    typealias IDType = UUID
    let id: ID
    let title:String
    init(title:String) {
        self.init(Identifier(value: UUID()), title)
    }
    private init(_ _id: ID,_ _t:String) { id = _id; title = _t }
}

struct Task: TypedIdentifiable {
    typealias IDType = UUID
    let id: ID
    let project:Project.ID
    let title:String

    init(project:Project.ID, title:String) {
        self.init(Identifier(value: UUID()), project, title)
    }
    private init(_ _id:ID,_ _project:Project.ID,_ _title:String) {
        id = _id
        project = _project
        title = _title
    }
}
struct SubTask: TypedIdentifiable {
    typealias IDType = UUID
    enum Change {
        case title(String)
        case completed(Bool)
    }
    let id: ID
    let task:Task.ID
    let title:String
    let completed:Bool
    
    init(task:Task.ID, title:String) {
        self.init(Identifier(value: UUID()), task, title, false)
    }
    
    public func alter(_ changes: Change...) -> Self { changes.reduce(self) { $0.alter($1) } }
    public func alter(_ changes: [Change] ) -> Self { changes.reduce(self) { $0.alter($1) } }
    private func alter(change c:Change) -> Self {
        switch c {
        case .title(let title): return .init(id,task,title,completed)
        case .completed(let completed): return .init(id,task,title,completed)
        }
    }
    private init(_ _id:ID,_ _task:Task.ID,_ _title:String,_ _completed:Bool) {
        id = _id
        task = _task
        title = _title
        completed = _completed
    }
}
extension Task: CustomStringConvertible {
    var description: String {
        "\nTask \(id) in Project «\(project)»: \(title)"
    }
}

extension SubTask: CustomStringConvertible {
    var description: String {
        "\nSubTask \(id) on Task «\(task)»: \(title), completed: \(completed)"
    }
}

let p = Project(title:"neighbour hood gathering")
let t0 = Task(project:p.id, title:"rent pavillion")
let t1 = Task(project:p.id, title:"rent china &vcuttlery")

let t0s0 = SubTask(task: t0.id, title:"request quotes from local businesses")
let t1s0 = SubTask(task: t1.id, title:"request quotes from local businesses").alter(.completed(true))

print(p)
print([t0,t1])
print([t0s0,t1s0])


//: [Next](@next)
