//: [Previous](@previous)

import Foundation

let op0 = "99999999999999999999999999999"
let op1 = 1

let r = Array(op0.enumerated()).map {
    let x = Int("\($1)")!
    return ($0, x)
}.map {
    let y: Int = (($0 == 0) ? Int($1) + op1 : (Int($1) + Int(op0.map { String($0) }[$0 - 1])!))
    return ($0, y)
}.map { (
    $0,
    $1 > 9 ? $1 - 10 : $1,
    $1 > 9 ? $1 % 10 : 0)
}

print(r)
//: [Next](@next)
