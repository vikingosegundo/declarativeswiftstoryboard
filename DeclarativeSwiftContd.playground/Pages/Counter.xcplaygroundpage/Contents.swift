//: [Previous](@previous)

struct Counter {
    enum Change {
        case increment
        case decrement
        case up(UInt)
        case down(UInt)
        case step(Int)
    }
    
    func alter(_ c:Change) -> Self {
        switch c {
        case .increment     : return alter(  .up(1))
        case .decrement     : return alter(.down(1))
        case let   .up(step): return alter(.step( Int(step)))
        case let .down(step): return alter(.step(-Int(step)))
        case let .step(step): return Self(value:value+step)
        }
    }
    let value: Int
}

var c = Counter(value:0)
c = c.alter(.increment)
c = c.alter(.increment)
c = c.alter(.decrement) 
c = c.alter(  .up(5))
c = c.alter(.down(2))
print(c.value)

//: [Next](@next)
