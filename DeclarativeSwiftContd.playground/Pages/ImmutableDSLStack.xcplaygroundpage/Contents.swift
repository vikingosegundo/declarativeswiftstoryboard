//: [Previous](@previous)

struct Stack<T> {
    private let state: [T]
    private let notifyList:[(Result) -> ()]
    enum CMD {
        case push(T)
        case pop
        case peek
        case register((Result) -> ())
    }
    enum Result {
        case popped(T?)
        case peeked(T?)
    }
    init() { self.init([],[]) }
    private init(_ s:[T],_ nl:[(Result) -> ()]) { state = s; notifyList = nl }
    func alter(_ cmd:CMD) -> Self {
        switch cmd {
        case .register(let f): return doRegister(f)
        case .push    (let t): return doPush(t)
        case .pop            : return doPop()
        case .peek           : return doPeek()
        }
    }
    private func doRegister(_ f: @escaping (Stack<T>.Result) -> ()) -> Self { Self(state      , notifyList + [f]) }
    private func doPush    (_ t:T)                                  -> Self { Self(state + [t], notifyList      ) }

    private func doPop() -> Self {
        let el = state.last
        defer { notifyList.forEach{ $0(.popped(el)) } }
        return state.isEmpty ? self : Self(Array(self.state[0..<state.count-1]), notifyList)
    }
    private func doPeek() -> Self {
        defer { notifyList.forEach{ $0(.peeked(state.last))}}
        return Stack(state, notifyList)
    }
}
func listener(_ r:Stack<String>.Result) -> () {
    print(r)
}
func listener1(_ r:Stack<String>.Result) -> () {
    print(String(String(describing:r).reversed()))
}
let s0 = Stack<String>()
    .alter(.register(listener))
    .alter(.push("One"))
    .alter(.push("Two"))
    .alter(.push("Three"))
    .alter(.pop)
    .alter(.register(listener1))
    .alter(.pop)

print(s0)

//: [Next](@next)
