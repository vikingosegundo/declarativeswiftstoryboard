//: [Previous](@previous)
import Foundation

struct ImmutableNumber {
    enum Change {
        case increase
    }
    
    func alter(_ c:Change) -> Self {
        switch c {
        case .increase: return Self(value:value+1)
        }
    }
    let value: Int
}

final class MutableNumber {
    init(value v:Int) {
        value = v
    }
    var value:Int
    func increment() {
        value = value + 1
    }
}

let n = 100_000
for _ in 0 ..< 10 {
    var inumber = ImmutableNumber(value:0)
    
    let d00 = Date()
    (0 ..< n).forEach { _ in
        inumber = inumber.alter(.increase)
    }
    let d01 = Date()
    
    let d10 = Date()
    
    let mnumber = MutableNumber(value:0)
    (0 ..< n).forEach { _ in
        mnumber.increment()
    }
    let d11 = Date()
    
    print("immuable: \(inumber.value) \(d01.timeIntervalSince1970 - d00.timeIntervalSince1970)")
    print("  muable: \(mnumber.value) \(d11.timeIntervalSince1970 - d10.timeIntervalSince1970)")
    print("")
}
//: [Next](@next)
