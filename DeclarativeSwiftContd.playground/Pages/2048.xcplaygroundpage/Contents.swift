//: [Previous](@previous)

struct Game {
    
    init() {
        self.init([
            [0,0,0,0],
            [0,0,0,0],
            [0,0,0,0],
            [0,0,0,0]
        ])
    }
    
    private init(_ b:[[Int]]) {
        board = b
    }
    
    let board:[[Int]]
    
    enum Change {
        case move(Direction)
        enum Direction {
            case north, west, south, east
        }
    }
    
    func alter(_ c:Change) -> Self {
        switch c {
        case .move(.north): return self.recalculate(for:.north)
        case .move(.west ): return self.recalculate(for:.west )
        case .move(.south): return self.recalculate(for:.south)
        case .move(.east ): return self.recalculate(for:.east )
        }
    }
    func recalculate(for d:Change.Direction) -> Self {
    
        switch d {
        case .north: return moveNorth()
        case .west : return moveWest()
        case .south: return moveSouth()
        case .east : return moveEast()
        }
        
        func moveNorth() -> Self {
            return self
        }
        func moveWest() -> Self {
            return self
        }
        func moveSouth() -> Self {
            return self
        }
        func moveEast() -> Self {
            return self
        }
        
    }
    
    func addNewNumber() -> Self {
        
    }
}

var g = Game()
g = g.alter(.move(.north))

print(g)
//: [Next](@next)
