//: [Previous](@previous)

func phitonacci(n:Int) -> Int {
    if n % 3 == 0 { return n+3 }
    if n % 2 != 1 { return n-3 }
    return phitonacci(n:n-1) + phitonacci(n:n+1)
}

let s = (1...6).reversed().reduce("") {
    $0 + "\(phitonacci(n:$1))"
}

let url = "https://phito.de/jobs/\(s)"

print(url)
//: [Next](@next)
