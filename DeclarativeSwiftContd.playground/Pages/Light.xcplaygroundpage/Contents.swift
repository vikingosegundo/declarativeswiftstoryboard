//: [Previous](@previous)

import Foundation

struct Light:Identifiable {
    enum Temperature { case mirek(Int) }
    enum Turn        { case on, off    }

    let id        : String
    let name      : String
    let isOn      : Turn

    let hue       : Double
    let saturation: Double
    let brightness: Double
    let colortemp : Int
    
    init(id:String, name:String) {
        self.init(id, name, .off, 0, 0, 0, 0)
    }
    
    private init(_ x:String, // id
                 _ n:String, // name
                 _ o:Turn,   // on or off
                 _ h:Double, // hue
                 _ s:Double, // saturation
                 _ b:Double, // brightness
                 _ t:Int     // colortemp
    ) {
        id = x
        name = n
        isOn = o
        hue = h
        saturation = s
        brightness = b
        colortemp = t
    }
}
extension Light {
    enum Change {
        case renaming(RenameIt); enum RenameIt { case it(to:String) }
        case turning (TurnIt  ); enum TurnIt   { case it(Turn)      }
        case setting (Setting ); enum Setting  {
            case hue        (to:Double     )
            case saturation (to:Double     )
            case brightness (to:Double     )
            case temperature(to:Temperature)
        }
    }
    func alter(by changes:Change...) -> Self {
        changes.reduce(self) { $0.alter($1) }
    }
    private
    func alter(_ change:Change) -> Self {
        switch change {
        case let .renaming(n):return renaming(to:n)
        case let .turning (o):return turning(onOff:o)
        case let .setting (v):return settingColor(value:v)
        }
    }
    private func renaming(to name: Change.RenameIt) -> Self {
        switch name {
        case let .it(to:n):
            return .init(id,n,isOn,hue,saturation,brightness,colortemp)
        }
    }
    private func turning(onOff: Change.TurnIt) -> Self {
        switch onOff {
        case .it(.on):
            return .init(id,name,.on ,hue,saturation,brightness,colortemp)
        case .it(.off):
            return .init(id,name,.off,hue,saturation,brightness,colortemp)
        }
    }
    private func settingColor(value:Change.Setting) -> Self {
        switch value {
        case let .hue(to:h):
            return .init(id,name,isOn,h  ,saturation,brightness,colortemp)
        case let .saturation(to:s):
            return .init(id,name,isOn,hue,s         ,brightness,colortemp)
        case let .brightness(to:b):
            return .init(id,name,isOn,hue,saturation,b         ,colortemp)
        case let .temperature(to:.mirek(t)):
            return .init(id,name,isOn,hue,saturation,brightness,t        )
        }
    }
}

var l0 = Light(id:"001", name:"Kitchen 1")
l0 = l0.alter(by: .turning(.it(.off))                       )
l0 = l0.alter(by: .setting(.hue(to: 0.33))                  )
l0 = l0.alter(by: .setting(.saturation(to: 0.9))            )
l0 = l0.alter(by: .setting(.brightness(to: 1.0))            )
l0 = l0.alter(by: .setting(.temperature(to: .mirek(230)))   )

let l1 = Light(id:"001", name:"Kitchen 2")
    .alter(
        by:
            
            .setting(.hue(to: 0.33)),
            .setting(.saturation(to: 0.9)),
            .setting(.brightness(to: 1.0)),
            .setting(.temperature(to: .mirek(230))),
            .turning(.it(.on))
        
    )

print(l0)
//: [Next](@next)
