//: [Previous](@previous)
import Foundation

public struct CoffeeTruck: Identifiable {
    public enum Change {
        case opening(It)
        case closing(It); public enum It{ case it}
        case renaming(Renaming)
        case setting (Setting)
        public enum Setting {
            case location    (_Location   ); public enum _Location { case to(Location) }
            case openingTimes(OpeningTimes)
        }
        public enum Renaming {
            case it(To); public enum To { case to(String) }
        }
    }
    public let id      : UUID
    public let name    : String
    public let location: Location
    public let open    : Bool
    public let times   : OpeningTimes
    public init(name n:String) {
        self.init(UUID(),n,.unknown,false, OpeningTimes())
    }
    public  func alter(by changes:Change...) -> Self { changes.reduce(self) { $0.alter(by:$1) } }
    private func alter(by change :Change   ) -> Self {
        typealias __ = Self
        switch change {
        case     .closing(.it)              : return __(id,name,location,false,times)
        case     .opening(.it)              : return __(id,name,location,true ,times)
        case let .renaming(.it(.to(n)))     : return __(id,n,   location,open ,times)
        case let .setting(.location(.to(l))): return __(id,name,l       ,open ,times)
        case let .setting(.openingTimes(t)) : return __(id,name,location,open ,t    )
        }
    }
}
extension CoffeeTruck {
    public enum Location {
        case unknown
        case coordinate(Coordinate)
        case address(Address)
    }
    public struct Coordinate {
        public init(latitude lat:Double,longitude lon:Double) {
            latitude = lat
            longitude = lon
        }
        public let latitude : Double
        public let longitude: Double
    }
    public struct Address {
        public init(street s:String,zipCode z:String? = nil,country c:String? = nil) {
            street = s; zipCode = z; country = c
        }
        public let street : String
        public let zipCode: String?
        public let country: String?
    }
}
extension CoffeeTruck.Coordinate: Codable,Equatable {}
extension CoffeeTruck.Address   : Codable,Equatable {}
extension CoffeeTruck.Location  : Codable,Equatable {}
extension CoffeeTruck           : Codable,Equatable {}

//MARK: - private
private extension CoffeeTruck {
    init(_ i:UUID,_ n:String,_ l:Location,_ o:Bool,_ t:OpeningTimes) {
        id = i; name = n; location = l; open = o; times = t
    }
}
public struct OpeningTimes {
    public enum Modifier {
        public enum Selection {
            public enum Day {
                case mon, tue, wed, thu, fri, sat, sun
            }
            case days([Day])
        }
        public enum Reason {
            case unspecified
            case reason(String)
        }
        public struct Range {
            let from     : Foundation.Date
            let to       : Foundation.Date
            let start    : HourMinute
            let stop     : HourMinute
            let selection: Selection
            let reason   : Reason
        }
        public struct Date {
            let date  : Foundation.Date
            let start : HourMinute
            let stop  : HourMinute
            let reason: Reason
        }
        case timeSpan(Self.Range)
        case date    (Self.Date )
    }
    public enum OpenForBusiness { case closed, open(Timespan)     }
    public struct Timespan      {
        public init(start s:HourMinute,end e:HourMinute) { start = s;end = e }
        public let start,end:HourMinute
    }
    public struct HourMinute    {
        public init(_ h:Int,_ m:Int) {hour = h; minute = m }
        public let hour,minute:Int
    }
    public let monday   : OpenForBusiness
    public let tuesday  : OpenForBusiness
    public let wednesday: OpenForBusiness
    public let thursday : OpenForBusiness
    public let friday   : OpenForBusiness
    public let saturnday: OpenForBusiness
    public let sunday   : OpenForBusiness
    public let modifiers: [Modifier]
    public init() {
        self.init(.closed, .closed, .closed, .closed, .closed, .closed, .closed,[])
    }
    public init(
        monday    mo: OpenForBusiness,
        tuesday   tu: OpenForBusiness,
        wednesday we: OpenForBusiness,
        thursday  th: OpenForBusiness,
        friday    fr: OpenForBusiness,
        saturnday sa: OpenForBusiness,
        sunday    su: OpenForBusiness
    ) {
        self.init(mo,tu,we,th,fr,sa,su,[])
    }
    public init(_ mo: OpenForBusiness,_ tu: OpenForBusiness,_ we: OpenForBusiness,_ th: OpenForBusiness,_ fr: OpenForBusiness,_ sa: OpenForBusiness,_ su: OpenForBusiness,_ m:[Modifier]) {
        monday = mo; tuesday = tu; wednesday = we; thursday = th; friday = fr; saturnday = sa; sunday = su
        modifiers = m
    }
    public enum Change {
        case times(for:Day)
        case add(Add); public enum Add {
            case modifier(Modifier)
        }
        case remove(Remove); public enum Remove {
            case modifier(Modifier)
        }
        public enum Day {
            case monday   (to:OpenForBusiness)
            case tuesday  (to:OpenForBusiness)
            case wednesday(to:OpenForBusiness)
            case thursday (to:OpenForBusiness)
            case friday   (to:OpenForBusiness)
            case saturnday(to:OpenForBusiness)
            case sunday   (to:OpenForBusiness)
        }
    }
    public func alter(_ c:Change) -> Self {
        switch c {
        case let .times (for:.monday   (to:t)): return .init(t,     tuesday,wednesday,thursday,friday,saturnday,sunday,modifiers      )
        case let .times (for:.tuesday  (to:t)): return .init(monday,t,      wednesday,thursday,friday,saturnday,sunday,modifiers      )
        case let .times (for:.wednesday(to:t)): return .init(monday,tuesday,t,        thursday,friday,saturnday,sunday,modifiers      )
        case let .times (for:.thursday (to:t)): return .init(monday,tuesday,wednesday,t,       friday,saturnday,sunday,modifiers      )
        case let .times (for:.friday   (to:t)): return .init(monday,tuesday,wednesday,thursday,t,     saturnday,sunday,modifiers      )
        case let .times (for:.saturnday(to:t)): return .init(monday,tuesday,wednesday,thursday,friday,t,        sunday,modifiers      )
        case let .times (for:.sunday   (to:t)): return .init(monday,tuesday,wednesday,thursday,friday,saturnday,t     ,modifiers      )
        case let .add   (.modifier(m)        ): return .init(monday,tuesday,wednesday,thursday,friday,saturnday,sunday,modifiers + [m])
        case let .remove(.modifier(m)        ): return .init(monday,tuesday,wednesday,thursday,friday,saturnday,sunday,modifiers.filter {
            switch (m,$0) {
            case let (.date(d0)    ,.date(d1)    ): return !(d0 == d1)
            case let (.timeSpan(t0),.timeSpan(t1)): return !(t0 == t1)
            case     (.timeSpan    ,_            ): return true
            case     (_            ,.timeSpan    ): return true
            }
        })
        }
    }
}
extension OpeningTimes.HourMinute            : Codable, Equatable {}
extension OpeningTimes.Timespan              : Codable, Equatable {}
extension OpeningTimes.OpenForBusiness       : Codable, Equatable {}
extension OpeningTimes.Modifier.Selection.Day: Codable, Equatable {}
extension OpeningTimes.Modifier.Selection    : Codable, Equatable {}
extension OpeningTimes.Modifier.Reason       : Codable, Equatable {}
extension OpeningTimes.Modifier.Date         : Codable, Equatable {}
extension OpeningTimes.Modifier.Range        : Codable, Equatable {}
extension OpeningTimes.Modifier              : Codable, Equatable {}
extension OpeningTimes                       : Codable, Equatable {}


let t0 = CoffeeTruck(name:"Chez Manuel")

print(t0)
//: [Next](@next)
