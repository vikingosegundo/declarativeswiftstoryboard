//: [Previous](@previous)

struct ConventionalConter {
    let value: Int
    init() {
        self.init(value: 0)
    }
    private init(value: Int) {
        self.value = value
    }
    func increment() -> Self {
        return increment(by: 1)
    }
    func decrement() -> Self {
        return decrement(by: 1)
    }
    func increment(by x:UInt) -> Self {
        return Self(value: value + Int(x))
    }
    func decrement(by x:UInt) -> Self {
        return Self(value: value - Int(x))
    }
}
struct DeclarativeCounter {
    enum Change {
        enum Value {
            case value(UInt)
        }
        case increment
        case decrement
        case incrementBy(Value)
        case decrementBy(Value)
    }
    let value: Int
    init() {
        self.init(value: 0)
    }
    private init(value: Int) {
        self.value = value
    }
    func alter(_ c:Change) -> Self {
        return switch c {
        case     .increment             : alter(.incrementBy(.value(1)))
        case     .decrement             : alter(.decrementBy(.value(1)))
        case let .incrementBy(.value(x)): Self(value: value + Int(x))
        case let .decrementBy(.value(x)): Self(value: value - Int(x))
        }
    }
}
var conventionalCounter = ConventionalConter()
conventionalCounter = conventionalCounter.increment(by:5)
conventionalCounter = conventionalCounter.decrement(by:2)
print(conventionalCounter.value)

var declarativeCounter = DeclarativeCounter()
declarativeCounter = declarativeCounter.alter(.incrementBy(.value(5)))
declarativeCounter = declarativeCounter.alter(.decrementBy(.value(2)))
declarativeCounter = declarativeCounter.alter(.decrementBy(.value(5)))

print(declarativeCounter.value)

//: [Next](@next)
