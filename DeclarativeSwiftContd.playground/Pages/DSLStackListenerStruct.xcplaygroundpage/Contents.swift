//: [Previous](@previous)

import Foundation

typealias Subscriber<T> = (UUID, (Stack<T>.Result) -> ())

struct Stack<T> {
    private var state: [T] = []
    private var notifyList:[(UUID,(Result) -> ())] = []

    enum CMD {
        case push(T)
        case pop
        case peek
        case register(Subscriber<T>)
        case unregister(UUID)
    }
    enum Result {
        case popped(T?)
        case peeked(T?)
    }
    mutating func perform(_ cmd:CMD)  {
        switch cmd {
        case .push      (let t): state.append(t)
        case .pop              : { let el = state.popLast(); notifyList.forEach{$0.1(.popped(el))} }()
        case .peek             : notifyList.forEach({ $0.1(.peeked(state.last))})
        case .register  (let p): notifyList.append(p)
        case .unregister(let u): notifyList = notifyList.filter { $0.0 != u }
        }
    }
}

func listener1(_ r:Stack<String>.Result) -> () { print(" ++ ++  \(rev(cmd: r))  ++ ++ ") }
func listener2(_ r:Stack<String>.Result) -> () { print("  ---   \(cap(cmd: r))  ---   ") }
func listener3(_ r:Stack<String>.Result) -> () { print("  . .   \(low(cmd: r))  . .   ") }

func rev(cmd: Stack<String>.Result) -> String {
    switch cmd {
    case .peeked(let t): return "peeked " + (String((t ?? "").reversed()))
    case .popped(let t): return "popped " + (String((t ?? "").reversed()))
    }
}
func cap(cmd: Stack<String>.Result) -> String {
    switch cmd {
    case .peeked(let t): return (String((t ?? "").uppercased()))
    case .popped(let t): return (String((t ?? "").uppercased()))
    }
}
func low(cmd: Stack<String>.Result) -> String {
    switch cmd {
    case .peeked(let t): return (String((t ?? "").lowercased()))
    case .popped(let t): return (String((t ?? "").lowercased()))
    }
}
var s = Stack<String>()
let p0 = (UUID(),listener1)
let p1 = (UUID(),listener2)
let p2 = (UUID(),listener3)

s.perform(.register(p0))
s.perform(.register(p1))
s.perform(.register(p2))


s.perform(.push("One"))
s.perform(.push("Two"))
s.perform(.push("Three"))
s.perform(.push("Four"))
s.perform(.push("Five"))

s.perform(.pop)
s.perform(.peek)
s.perform(.pop)
s.perform(.unregister(p1.0))
s.perform(.unregister(p2.0))
s.perform(.pop)
s.perform(.pop)
s.perform(.pop)


//: [Next](@next)
