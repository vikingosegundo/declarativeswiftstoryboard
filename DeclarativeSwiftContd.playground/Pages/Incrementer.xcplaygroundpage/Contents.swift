//: [Previous](@previous)

struct ConventionalCounter{
    let value: UInt
    private init(value: UInt) {
        self.value = value
    }
    init() {
        self.init(value: 0)
    }
    func increment() -> Self {
        return Self(value:value + 1)
    }
    func decrement() -> Self {
        return Self(value: value - 1)
    }
}
struct DeclarativeCounter {
    enum Change {
        case increment
        case decrement
    }
    let value: UInt
    private init(value: UInt) {
        self.value = value
    }
    init() {
        self.init(value: 0)
    }
    
    func alter(_ c:Change) -> Self {
        return switch c {
        case .increment: Self(value: value + 1)
        case .decrement: Self(value: value - 1)
        }
    }
}
var conventionalCounter = ConventionalCounter()
conventionalCounter = conventionalCounter.increment()

print(conventionalCounter.value)

var declarativeCounter = DeclarativeCounter()
declarativeCounter = declarativeCounter.alter(.increment)
declarativeCounter = declarativeCounter.alter(.increment)
declarativeCounter = declarativeCounter.alter(.increment)
declarativeCounter = declarativeCounter.alter(.decrement)
print(declarativeCounter.value)

//: [Next](@next)
