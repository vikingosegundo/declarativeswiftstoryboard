//: [Previous](@previous)

let metersInOneMile = 1609.34
let feetInMeters = 3.28

struct Length {
    enum Value {
        case meters(Double)
        case kilometers(Double)
        case millimeters(Double)
        case feet(Double)
        case miles(Double)
    }
    let meters: Double
    private init (amountInMeters a: Double) { meters = a }
    init(_ value:Value) {
        switch value {
        case      .meters(let  m): meters = m
        case  .kilometers(let km): meters = km * 1000.0
        case .millimeters(let mm): meters = mm / 1000
        case         .feet(let f): meters = f / feetInMeters
        case        .miles(let l): meters = l * metersInOneMile
        }
    }
    static func +(lhs: Length, rhs:Length.Value) -> Length {
       switch rhs {
       case      .meters(let m): return Length(amountInMeters: lhs.meters + m)
       case  .kilometers(let m): return Length(amountInMeters: lhs.meters + m * 1000)
       case .millimeters(let m): return Length(amountInMeters: lhs.meters + m / 1000)
       case        .feet(let f): return Length(amountInMeters: lhs.meters + f / feetInMeters)
       case       .miles(let l): return Length(amountInMeters: lhs.meters + l * metersInOneMile)
       }
    }
    
    static func -(lhs: Length, rhs:Length.Value) -> Length {
        switch rhs {
        case      .meters(let m): return Length(amountInMeters: lhs.meters - m)
        case  .kilometers(let m): return Length(amountInMeters: lhs.meters - m * 1000)
        case .millimeters(let m): return Length(amountInMeters: lhs.meters - m / 1000)
        case        .feet(let f): return Length(amountInMeters: lhs.meters - f / feetInMeters)
        case       .miles(let l): return Length(amountInMeters: lhs.meters - l * metersInOneMile)
        }
     }
}

func +(lhs:Length.Value, rhs:Length.Value) -> Length { Length(lhs) + rhs }
func -(lhs:Length.Value, rhs:Length.Value) -> Length { Length(lhs) - rhs }

extension Double {
    var km:Length.Value { .kilometers(self) }
    var  m:Length.Value {     .meters(self) }
    var ft:Length.Value {       .feet(self) }
    var  l:Length.Value {      .miles(self) }

}

let l0:Length = 1.km + 3280.84.ft - 500.m
print(l0.meters) // 1500.2560975609758




//: [Next](@next)
