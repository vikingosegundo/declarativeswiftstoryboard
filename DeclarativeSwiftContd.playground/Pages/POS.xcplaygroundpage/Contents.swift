//: [Previous](@previous)

import Foundation

struct Table {
    let tableID:Int
}

struct Order {
    enum Change {
        case add(Add); enum Add {
            case position(Position)
        }
        case remove(Remove); enum Remove {
            case position(Position)
        }
        case inrease(Increase); enum Increase {
            case count(by:UInt, for: Position)
        }
        case derease(Increase); enum Decrease {
            case count(by:UInt, for: Position)
        }

        case change(to:Table)
    }
    let positions:[Position]
    let table:Table
    
    init(table t:Table) { self.init(t,[]) }
    
    func alter(_ c:Change...) -> Self {c.reduce(self) { $0.alter($1) }}
    var totalPrice:Int { positions.reduce(0) { $0 + $1.price } }
}


extension Order {
    private init(_ t:Table,_ p:[Position]) {
        table = t
        positions = p
    }
    private
    func alter(_ c:Change) -> Self {
        switch c {
        case let .add   (.position(p)       ): return add(position:p)
        case let .remove(.position(p)       ): return remove(position:p)
        case let .change(to: t              ): return change(table:t)
        case let .inrease(.count(by:i,for:p)): return increase(count:i, for:p)
        case let .derease(.count(by:d,for:p)): return decrease(count:d, for:p)
        }
    }
    private
    func add(position p:Position) -> Self {
        return p.count > 0 ? .init(table,sort(positions + [p])) : self
    }
    private
    func remove(position p:Position) -> Self {
        return .init(table,sort( positions.filter {!($0.count == p.count && $0.article == p.article) }                                   ))
    }
    private
    func change(table t:Table) -> Self {
        return .init(t,sort( positions))
    }
    private
    func increase(count c:UInt, for p:Position) -> Self {
        return .init(table,sort([positions.first(where: { $0.id == p.id})!.alter(.inrease(.by(c)))] + positions.filter({ $0.id != p.id })))
    }
    private
    func decrease(count c:UInt, for p:Position) -> Self {
        return .init(table,sort([positions.first(where: { $0.id == p.id})!.alter(.derease(.by(c)))] + positions.filter({ $0.id != p.id })))
    }
    
    private
    var positionsID: [UUID] {
        positions.map{$0.id}
    }
    private
    func postion(for id:UUID) -> Position? {
        positions.first(where: { $0.id == id})
    }
}
func sort(_ pos:[Position]) -> [Position] {
    pos.sorted(by: {$0.created < $1.created})
}

struct Position:Identifiable {
    enum Change {
        case pay
        case inrease(Increase); enum Increase {
            case by(UInt)
        }
        case derease(Increase); enum Decrease {
            case by(UInt)
        }
    }
    
    enum State {
        case unpaid
        case   paid
    }
    init(count c: Int, article a:Article) {
        self.init(UUID(),c, a, .unpaid, .now)
    }
    private
    init(_ i:UUID,_ c: Int,_ a:Article,_ s:State,_ cr:Date) {
        id = i
        count = c
        article = a
        state = s
        created = cr
    }
    
    func alter(_ c:Change) -> Self {
        switch c {
        case .pay                :
            return .init(id,count,article,.paid,created)
        case .inrease(.by(let i)):
            return .init(id,count+Int(i),article,.paid,created)
        case .derease(.by(let i)):
            return (i <= count)
            ? .init(id,count-Int(i),article,state,created)
            : self

        }
    }
    let id     : UUID
    let created: Date
    let count  : Int
    let article: Article
    let state  : State
    var price  : Int { count * article.price }
}

struct Article:Equatable {
    let name:String
    let price:Int
}
extension Table   :CustomStringConvertible { var description: String { "table: \(tableID)"} }
extension Article :CustomStringConvertible { var description: String { "\(name) (\(price))" } }
extension Position:CustomStringConvertible { var description: String { "\(count) \(article)" } }
extension Order   :CustomStringConvertible { var description: String { "\(table)\(positions.reduce("", {$0 + "\n\($1)"})) \ntotal: \(totalPrice)"} }
let spaghettiEis  = Article(name:"Spaghetti Eis",price:700)
let kaffee        = Article(name:"Kaffee"       ,price:270)
let sahneschnitte = Article(name:"Sahneschnitte",price:400)

let t0 = Table(tableID:1)
let t1 = Table(tableID:2)

var order = Order(table:t0)
    .alter(.add   (.position(Position(count:1,article:spaghettiEis ))),
           .add   (.position(Position(count:2,article:kaffee       ))),
           .add   (.position(Position(count:1,article:sahneschnitte))),
           .remove(.position(Position(count:1,article:spaghettiEis ))))
print(order)
print("--------------")
order = order.alter(.change(to: t1))
print(order)
print("--------------")
order = order.alter(.inrease(.count(by: 10, for: order.positions.first!)))
print(order)
print("--------------")
order = order.alter(.derease(.count(by: 2, for: order.positions.first!)))
print(order)
print("--------------")
order = order.alter(.derease(.count(by: 8, for: order.positions.first!)))
print(order)
print("--------------")


//: [Next](@next)
