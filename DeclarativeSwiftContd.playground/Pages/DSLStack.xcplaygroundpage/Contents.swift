//: [Previous](@previous)

enum Stack<T> {
    enum CMD {
        case push( T )
        case pop ((T?) -> ())
        case peek((T?) -> ())
    }
    static func create() -> (CMD) -> () {
        var state:[T] = []
        return { cmd in
            switch cmd {
            case .push(let t   ): state.append(t)
            case .pop (let pop ): pop (state.popLast())
            case .peek(let peek): peek(state.last)
            }
        }
    }
}

let s = Stack<String>.create()
var result: String?  { didSet { print("\(result ?? "nil")")}}
s(.push("hello"))
s(.push("world"))
s(.pop ({ result = $0 }))
s(.peek({ result = $0 }))
s(.pop ({ result = $0 }))
s(.pop ({ result = $0 }))

//: [Next](@next)
