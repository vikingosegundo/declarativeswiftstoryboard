//: [Previous](@previous)

import Foundation

func phitonaccci(n:Int) -> Int{
    if n % 3 == 0 { return n + 3 }
    if n % 2 != 1 { return n - 3 }
    
    let p1 = phitonaccci(n: n - 1 )
    let p2 = phitonaccci(n: n + 1 )
    return p1 + p2
}

let s = (1...6)
    .reversed()
    .reduce("") {
        $0 + "\(phitonaccci(n:$1))"
    }
let url = URL(string: "https://www.phito.de/jobs/\(s)")!
print(url)

//: [Next](@next)
