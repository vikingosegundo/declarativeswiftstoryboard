//: [Previous](@previous)

import Foundation

struct Account {
    enum Currency {
        case eur
        case usd
    }
    enum Transaction {
        case deposit   (value:Double, currency:Currency)
        case withdrawal(value:Double, currency:Currency)
    }
    enum Booking {
        case deposit   (eur:Double)
        case withdrawal(eur:Double)
    }
    enum Configuation {
        case rate(usdToEuro:Double, euroToUSD:Double)
        case state(State)
    }
    enum State {
        case inactive
        case active
    }
    
    let state: State
    let bookings: [Booking]
    let conversionRateUSDToEuro:Double
    let conversionRateEuroToUSD:Double
    
    init() {
        self.init(.inactive,[],0.0,0.0)
    }
    
    private init(_ state:State,_ bookings:[Booking],_ conversionRateUSDToEuro:Double, _ conversionRateEuroToUSD:Double ) {
        self.state = state
        self.bookings = bookings
        self.conversionRateUSDToEuro = conversionRateUSDToEuro
        self.conversionRateEuroToUSD = conversionRateEuroToUSD
    }
    
    func process(_ transaction:Transaction) -> Self {
        guard state == .active && (conversionRateUSDToEuro > 0.0 && conversionRateEuroToUSD > 0.0) else {
            return self
        }
        return switch transaction {
        case let .deposit   (value,currency): transactionDeposit   (value:value,currency:currency)
        case let .withdrawal(value,currency): transactionWithdrawal(value:value,currency:currency)
        }
    }
    private func transactionDeposit(value:Double, currency:Currency) -> Self {
        guard value > 0.0 else {
            return self
        }
        return switch currency {
        case .usd: Self(state, bookings + [.deposit(eur: value * conversionRateUSDToEuro)], conversionRateUSDToEuro, conversionRateEuroToUSD)
        case .eur: Self(state, bookings + [.deposit(eur: value                          )], conversionRateUSDToEuro, conversionRateEuroToUSD)
        }
    }
    private func transactionWithdrawal(value:Double, currency:Currency) -> Self {
        guard value > 0.0 else {
            return self
        }
        return switch currency {
        case .eur: Self(state, bookings + [.withdrawal(eur: value)]                          , conversionRateUSDToEuro, conversionRateEuroToUSD)
        case .usd: Self(state, bookings + [.withdrawal(eur: value / conversionRateEuroToUSD)], conversionRateUSDToEuro, conversionRateEuroToUSD)
        }
    }
    
    func configure(_ configuration:Configuation) -> Self {
        switch configuration {
        case let .state(state)             : Self(state, bookings, conversionRateUSDToEuro, conversionRateEuroToUSD)
        case let .rate(usdToEuro,euroToUSD): Self(state, bookings, usdToEuro, euroToUSD)
        }
    }
    var total:Double {
        bookings.reduce(0.0) {
            return switch $1 {
            case let .deposit   (euro): $0 + euro
            case let .withdrawal(euro): $0 - euro
            }
        }
    }
}


var account = Account()
    .configure(.rate(usdToEuro: 0.95, euroToUSD: 1.06))
    .configure(.state(.active))

account = account
    .process(.deposit   (value:100,currency:.eur))
    .process(.deposit   (value:100,currency:.usd))
    .process(.withdrawal(value:100,currency:.usd))

print(account.total)

//: [Next](@next)
