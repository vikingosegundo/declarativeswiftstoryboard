//: [Previous](@previous)

/// Example of a state machine for a vending machine
struct VendingMachine {

    /// Only allow valid state transitions
    enum Change {
        case fromIdleToCoinInserted
        case fromCoinInsertedToFetching
        case fromFetchingToServing
        case fromServingToIdle
    }

    enum State {
        case idle
        case coinInserted
        case fetching
        case serving
    }

    init() {
        self.init(.idle)
    }

    func alter(_ c:Change...) -> Self { c.reduce(self) { $0.alter($1) } }

    private
    func alter(_ c:Change) -> Self {
        switch c {
        case .fromIdleToCoinInserted:     return Self(.coinInserted)
        case .fromCoinInsertedToFetching: return Self(.fetching)
        case .fromFetchingToServing:      return Self(.serving)
        case .fromServingToIdle:          return Self(.idle)
        }
    }

    private init(_ s:State) {
        state = s
    }

    let state:State
}


let vm = VendingMachine()
    .alter(
          .fromIdleToCoinInserted
        , .fromCoinInsertedToFetching
      //  , .fromFetchingToServing
        , .fromServingToIdle
    )
print(vm.state)

//: [Next](@next)
