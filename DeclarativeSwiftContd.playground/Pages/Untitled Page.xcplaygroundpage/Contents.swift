//: [Previous](@previous)

print(Array(1...49).shuffled().prefix(6).sorted())

let shuffeledAlphabet = Array(97...122).shuffled().map{ Character(UnicodeScalar($0)!)}
let letter = shuffeledAlphabet[0]

print(letter)
//: [Next](@next)
