//: [Previous](@previous)

import Foundation

func checkIfSameCharacters(_ s0:String,_ s1:String) -> Bool {
    s0.count == s1.count
    && NSCountedSet(array: s0.split(separator:""))
        == NSCountedSet(array: s1.split(separator:""))
}

print(checkIfSameCharacters("AAATTCGAATGGTATGCC", "TTAACAGAAGTCCGGTAT"))
print(checkIfSameCharacters("AAATTCGAATGGTATGCCc", "TTAACAGAAGTCCGGTAT"))

//: [Next](@next)
