import PlaygroundSupport
import UIKit

protocol Applicable {
    associatedtype S = Self where S:Applicable
    func apply( _ action: ((_ view: S) -> Void)?)
}

extension Applicable {
    func apply( _ action: ((_ view: Self) -> Void)?) { action?(self) }
}

extension UIView: Applicable { }


let contenView = UIView(frame: .init(origin: .zero, size: .init(width: 200, height: 200)))
let label = UILabel(frame: .init(origin: .zero, size: .init(width: 200, height: 100)))
let button = UIButton(frame: .init(origin: .init(x: 0, y: 100), size: .init(width: 100, height: 44)))

contenView.apply { v in
    v.backgroundColor = .orange
}

label.apply { l in
    l.text = "Hello World!"
}

button.apply { b in
    b.setTitle("ACTION", for: .normal)
    b.backgroundColor = .cyan
    b.addAction(UIAction { _ in
        print("ACTION")
    }, for: .touchUpInside)
}

contenView.apply { v in
    v.addSubview(label)
    v.addSubview(button)
}



PlaygroundPage.current.liveView = contenView
