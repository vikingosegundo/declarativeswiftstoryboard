//: [Previous](@previous)

import UIKit

@IBDesignable
class ArrowView: UIView {
    override class var layerClass: AnyClass { CAShapeLayer.self }
    var shapeLayer: CAShapeLayer { layer as! CAShapeLayer }

    @IBInspectable var cornerRadius: CGFloat = 6 { didSet { setNeedsLayout() } }
    @IBInspectable var color: UIColor = .black { didSet { setNeedsLayout() } }

    override func layoutSubviews() {
        super.layoutSubviews()

        let rect = bounds.insetBy(dx: cornerRadius, dy: cornerRadius)
        let arrowInset = rect.height / 2

        let path = UIBezierPath()
        path.move(to: CGPoint(x: rect.minX, y: rect.minY))
        path.addLine(to: CGPoint(x: rect.maxX - arrowInset, y: rect.minY))
        path.addLine(to: CGPoint(x: rect.maxX, y: rect.midY))
        path.addLine(to: CGPoint(x: rect.maxX - arrowInset, y: rect.maxY))
        path.addLine(to: CGPoint(x: rect.minX, y: rect.maxY))
        path.close()

        shapeLayer.lineWidth = cornerRadius * 2
        shapeLayer.path = path.cgPath
        shapeLayer.fillColor = color.cgColor
        shapeLayer.strokeColor = color.cgColor
        shapeLayer.lineJoin = .round
    }
}

let v = ArrowView(frame: .init(x: 0, y: 0, width: 120, height: 20))
v.color = .orange

//: [Next](@next)
