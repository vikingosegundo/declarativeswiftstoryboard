//: [Previous](@previous)
import Foundation

struct Book {
    let title: String
    let authors:[Authorship]
    let chapters:[Chapter]
}

extension Book {
    enum Command {
        case changing(Changing)
        enum Changing {
            case title(to:String)
        }
        case adding(Adding)
        enum Adding {
            case chapter(Chapter)
            case author(Authorship)
        }
        case removing(Removing)
        enum Removing {
            case chapter(titled:String)
            case author(named:String)
        }
        case replacing(Replacing)
        enum Replacing {
            case chapter(titled:String, with:Chapter)
        }
    }
    
    func alter(by c:Command) -> Self {
        switch c {
        case let .changing (.title(to:t))             : Self(title:t    ,authors:authors    ,chapters:chapters                      )
        case let .adding   (.author (a))              : Self(title:title,authors:authors+[a],chapters:chapters                      )
        case let .adding   (.chapter(c))              : Self(title:title,authors:authors    ,chapters:chapters+[c]                  )
        case let .removing (.author (n))              : Self(title:title,authors:rm(named:n),chapters:chapters                      )
        case let .removing (.chapter(titled:t))       : Self(title:title,authors:authors    ,chapters:chapters.filter{$0.title != t})
        case let .replacing(.chapter(titled:t,with:c)): Self(title:title,authors:authors    ,chapters:chapters.reduce([], {
            switch $1.title == t {
            case true : $0 + [c]
            case false: $0 + [$1]
            }
        }))
        }
    }
    
    func rm(named name:String) -> [Authorship] {
        authors.reduce([], {
            if $1.author.name == name {
                $0
            } else {
                $0 + [$1]
            }
        })
    }
}

func alter(_ book:Book, by commands:Book.Command...) -> Book {
    commands.reduce(book, { $0.alter(by:$1) })
}

struct Authorship {
    enum Kind {
        case main
        case supporting
    }
    let author:Author
    let kind:Kind
}

struct Author {
    let name:String
}

struct Chapter {
    let title:String
    let paragraphs:[Paragraph]
}

struct Paragraph {
    let content:Content
}

enum Content {
    case text(Text)
    case list(List)
}

enum Text {
    case plain(String)
    case titled(String, text:String)
    case preformatted(Prefomatted)
}

enum List {
    case items([Item])
}

enum Item {
    case text(String)
    case list(String,List)
}

enum Prefomatted {
    case plain(String)
    case code(String, language:String)
}

var book = Book(title: "Declarative Revolution",
              authors: [Authorship(author: Author(name: "Manuel Meyer"), kind: .main)],
             chapters: [
                Chapter(title: "Chapter 1",
                        paragraphs: [
                            Paragraph(content: .text(.titled("Lorem", text: "Ipsum"))),
                            Paragraph(content: .text(.plain("Lorem ipsum"))),
                            Paragraph(content: .text(.preformatted(.code("let x = \"hello world\"", language: "swift"))))
                        ]),
                Chapter(title: "Chapter 2",
                        paragraphs: [
                            Paragraph(content:
                                .list(
                                    .items([
                                        .text("Item 1"),
                                        .text("Item 2"),
                                        .text("Item 3"),
                                        .list("Item 4",
                                              .items([
                                                .text("Item 4.1"),
                                                .text("Item 4.2"),
                                                .text("Item 4.3"),
                                                .list("Item 4.4",
                                                    .items([
                                                        .text("Item 4.4.1"),
                                                        .text("Item 4.4.2")])
                                                      )
                                              ])
                                        ),
                                        .text("Item 5")
                                    ])
                                )
                            )
                        ])
             ]
)

let c3 = Chapter(title: "Chapter 3",
                 paragraphs: [
                    .init(content: .text(.plain("dolor sit amet")))
                 ]
)
let c4 = Chapter(title: "Chapter 4",
                 paragraphs: [
                    .init(content: .text(.plain("Duis commodo sed lectus sit amet blandit.")))
                 ]
)
book = alter(book,
             by:
        .adding(.chapter(c3)),
        .adding(.chapter(c4)),
        .adding(.author(Authorship(author: Author(name: "John Doe"), kind: .supporting)))
)
print("----------")
print("+ dump 1 +")
print("==========")

var l = PrettyPrinter()
dump(book,to:&l)
//===================

let c2_revised = Chapter(title: "Chapter 2 revised",
                         paragraphs: [
                            .init(content:.text(.plain("consectetur adipiscing elit")))
                         ]
)

book = alter(book,
             by:
        .replacing(.chapter(titled: "Chapter 2",with: c2_revised)),
        .changing(.title(to:"Declarative Revolution!"))
)
print("----------")
print("+ dump 2 +")
print("==========")
dump(book,to:&l)
//===================

book = alter(book,
             by:
        .removing(.chapter(titled: "Chapter 4")),
        .removing(.author(named: "John Doe"))
)
print("----------")
print("+ dump 3 +")
print("==========")
dump(book,to:&l)

//===================
book = alter(book,
             by:
        .adding(.author(Authorship(author: Author(name:"vikingosegundo"), kind: .main))),
        .removing(.author(named: "Manuel Meyer")),
        .changing(.title(to: "¡Revolución Declarativa!"))
)
print("----------")
print("+ dump 4 +")
print("==========")
dump(book,to:&l)


//: [Next](@next)
