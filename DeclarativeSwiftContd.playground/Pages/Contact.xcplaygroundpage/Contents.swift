//: [Previous](@previous)
struct Contact {
    init(givenName g:String, familyName f:String) {
        self.init(g, f, .unknown)
    }
    private init(_ g:String,_ f:String, _ a:Address) {
        givenName = g
        familyName = f
        address = a
    }
    let givenName:String
    let familyName:String
    let address:Address
}
extension Contact {
    enum Change {
        case givenName(String)
        case familyName(String)
        case address(Address)
    }
    func alter(_ c:Change...) -> Self {
        c.reduce(self) {$0.alter($1)}
    }
    private
    func alter(_ c:Change) -> Self {
        switch c {
        case let .givenName (g): return Self(g        , familyName, address)
        case let .familyName(f): return Self(givenName, f         , address)
        case let .address   (a): return Self(givenName, familyName, a      )
        }
    }
}

enum Address {
    case unknown
    case postal(street:String, city:String)
}
extension Address {
    var isUnknown:Bool {
        switch self {
        case .unknown: return true
        default: return false
        }
    }
    var street:String? {
        switch self {
        case let .postal(street:s,city:_): return s
        default: return nil
        }
    }
    var city:String? {
        switch self {
        case let .postal(street:_,city:c): return c
        default: return nil
        }
    }
}

let c0 = Contact(givenName: "Manuel",familyName: "Meyer")
    .alter(
        .address(
            .postal(street: "A Street", city: "A City")
        )
    )
let c1 = Contact(givenName: "Manuel",familyName: "Meyer")
    .alter(
        .address(.postal(street: "Another Street", city: "AnotherCity")),
        .givenName("Manu")
    )
    




//: [Next](@next)
