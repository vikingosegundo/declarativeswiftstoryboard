//: [Previous](@previous)

extension Collection {
    func sorted<Value:Comparable>(on kp:KeyPath<Element,Value>, by increasinglyOrdered:(Value,Value) -> Bool = { $0 < $1 }) -> [Element] {
        sorted { increasinglyOrdered($0[keyPath:kp],$1[keyPath:kp]) }
    }
}
struct Person {
    let name:String
    let age:UInt
}
let people = [
    Person(name:"Ernie",    age:15),
    Person(name:"Bert",     age:54),
    Person(name:"Samson",   age:14),
    Person(name:"Hildegard",age:74)
]
let longestName  = people.sorted(on:\.name.count,by:>).first!
let shrotestName = people.sorted(on:\.name.count     ).first!
let oldest       = people.sorted(on:\.age,       by:>).first!
let youngest     = people.sorted(on:\.age,       by:<).first!

print("longest name:  " +  longestName.name)
print("shortest name: " + shrotestName.name)
print("oldest:        " +       oldest.name)
print("youngest:      " +     youngest.name)
//: [Next](@next)
