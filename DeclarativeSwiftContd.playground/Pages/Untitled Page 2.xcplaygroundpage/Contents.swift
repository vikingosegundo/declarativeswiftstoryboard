//: [Previous](@previous)

struct Lecture: Codable, Equatable {
    let title: String
    let times: [LectureTime]
    let color: [String: String]
}

struct LectureTime: Codable, Hashable {
    let start: Double
    let len: Double
}

var oldLecture = Lecture(title: "lecture1",
                         times: [.init(start: 1, len: 2), .init(start: 5, len: 2)],
                         color: ["fg": "#fff", "bg": "#000"])
var newLecture = Lecture(title: "lecture1",
                         times: [.init(start: 2, len: 3), .init(start: 5, len: 2)],
                         color: ["fg": "#fff", "bg": "#000"])

if oldLecture != newLecture {
    let oldTimes = Set(oldLecture.times)
    let newTimes = Set(newLecture.times)
    let x = oldTimes.symmetricDifference(newTimes)
    print(String(describing:x))
}
//: [Next](@next)
