//: [Previous](@previous)
import Foundation

typealias Subscriber<T> = (id:UUID, exe:(Stack<T>.Result) -> ())

enum Stack<T> {
    enum CMD {
        case push( T )
        case pop
        case peek
        case register(Subscriber<T>)
        case unregister(UUID)
    }
    enum Result {
        case popped(T?)
        case peeked(T?)
    }
    static func create() -> (CMD) -> () {
        var state:[T] = []
        var notifyList:[Subscriber<T>] = []
        return { cmd in
            switch cmd {
            case .push      (let t): state.append(t)
            case .pop              : { let el = state.popLast(); notifyList.forEach{$0.exe(.popped(el))} }()
            case .peek             : notifyList.forEach({ $0.exe(.peeked(state.last))})
            case .register  (let f): notifyList.append(f)
            case .unregister(let u): notifyList = notifyList.filter { $0.0 != u }
            }
        }
    }
}

let stack = Stack<String>.create()
func popped(_ s:String?) { print("popped: \(s ?? "<nothing>")") }
func peeked(_ s:String?) { print("peeked: \(s ?? "<nothing>")") }
func dispatcher(_ r:(Stack<String>.Result) ) {
    switch r {
    case .popped(let s): popped(s)
    case .peeked(let s): peeked(s)
    }
}
func logr(_ r:(Stack<String>.Result)) {
    print("... \(r)")
}
let s0 = (id:UUID(), dispatcher)
let s1 = (id:UUID(), logr)
stack(.register(s0))
stack(.register(s1))
stack(.push("hello"))
stack(.push("world"))
stack(.pop)
stack(.peek)
stack(.pop)
stack(.pop)
stack(.unregister(s0.id))
stack(.peek)

//: [Next](@next)
