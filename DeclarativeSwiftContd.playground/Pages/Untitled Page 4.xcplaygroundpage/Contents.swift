//: [Previous](@previous)
import Foundation

struct TodoItem:Identifiable {
    enum Change {
        case execute((TodoItem) -> ())
        case title(String)
        case due  (TodoDate)
        case   finish
        case unfinish
    }
    // members
    let id       : UUID
    let title    : String
    let completed: Bool
    let due      : TodoDate
    
    // initialisers
    init(title:String) { self.init(UUID(),title,false,.unknown) }
    private
    init(_ i:UUID,_ t:String,_ c:Bool,_ d:TodoDate) { id=i; title=t; completed=c; due=d }
    
    func alter(_ c:Change...) -> Self { c.reduce(self) { $0.alter($1) } }
    private
    func alter(_ c:Change   ) -> Self {
        switch c {
        case let .execute(f): f(self); return self
        case let   .title(t):          return Self(id,t    ,completed,due)
        case         .finish:          return Self(id,title,true     ,due)
        case       .unfinish:          return Self(id,title,false    ,due)
        case let        .due(.timeSpan(.start(.from(e),.to(l)))):
            return l > e
                ? Self(id,title,completed,.timeSpan(.start(.from(e),.to(l))))
                : self
        case let     .due(d):          return Self(id,title,completed,d  )
        }
    }
}
struct TodoList: Identifiable {
    enum Change {
        case execute((TodoList) -> ())
        case add   (Add   ); enum Add    { case item(TodoItem) }
        case remove(Remove); enum Remove { case item(TodoItem) }
        case update(Update); enum Update { case item(TodoItem) }
    }
    // members
    let id   : UUID
    let items: [TodoItem]
    
    // initializers
    init() { self.init(UUID(),[]) }
    private
    init(_ i:UUID,_ its:[TodoItem]) { id = i; items = its }
    
    func alter(_ c:Change...) -> Self { c.reduce(self) { $0.alter($1) } }
    private
    func alter(_ c:Change   ) -> Self {
        switch c {
        case let .execute     (f) : f(self); return self
        case let .add   (.item(i)):          return Self(id,items + [i])
        case let .remove(.item(i)):          return Self(id,items - [i])
        case let .update(.item(i)):          return
            items.contains(where:{ $0.id == i.id })
                ? self
                    .alter(
                        .remove(.item(i)),
                        .add   (.item(i)))
                : self
        }
    }
}
//Domain Data types
enum Location {
    var id:Int { hashValue }
    case unknown
    case address(Address)
    case coordinate(Coordinate)
    case directions(Directions)
}
struct Address {
    enum Change {
        case street (String)
        case city   (String)
        case country(String)
        case zipCode(String)
    }
    init(street:String, city:String, country:String, zipCode:String) { self.init(UUID(),street,city,country,zipCode) }
    private
    init(_ i:UUID,_ s:String,_ ci:String,_ co:String,_ z:String) { id=i; street=s; city=ci; country=co; zipCode=z }
    let id     : UUID
    let street : String
    let city   : String
    let country: String
    let zipCode: String
    
    func alter(_ c:Change...) -> Self { c.reduce(self) { $0.alter($1) } }
    private
    func alter(_ c:Change) -> Self {
        switch c {
        case let .street (s):return .init(id,s,     city,country,zipCode)
        case let .city   (c):return .init(id,street,c,   country,zipCode)
        case let .country(c):return .init(id,street,city,c,      zipCode)
        case let .zipCode(z):return .init(id,street,city,country,z      )
        }
    }
}
struct Coordinate {
    let id = UUID()
    let latitude : Double
    let longitude: Double
}
struct Directions {
    enum Step:Identifiable, Hashable {
        var id: Int { hashValue }
        case step(String)
    }
    enum Change {
        case replace(Replace); enum Replace{
            case steps([Step])
        }
    }
    let id: UUID
    let steps: [Step]
    func alter(_ c:Change) -> Self {
        switch c {
        case let .replace(.steps(s)): return .init(id:id,steps:s)
        }
    }
}
enum TodoDate {
    case unknown
    case date(Date)
    case timeSpan(TimeSpan)
    var timeSpan:TimeSpan? {
        switch self {
        case      .unknown   : return nil
        case         .date   : return nil
        case let .timeSpan(t): return t
        }
    }
    var date:Date? {
        switch self {
        case  .unknown   : return nil
        case .timeSpan   : return nil
        case let .date(d): return d
        }
    }
}
enum TimeSpan{
    enum TimeComponent {
        var id: Int { hashValue }
        case seconds
        case minutes
        case hours
        case days
    }
    enum Start {
        var id:Int { hashValue }
        case from(Date)
    }
    enum End {
        var id: Int { hashValue }
        case unknown
        case to(Date)
    }
    var id:Int { hashValue }
    case start   (Start,End)
    case duration(Start,for:Int,TimeComponent)
}
extension Address               :Identifiable, Hashable {}
extension Coordinate            :Identifiable, Hashable {}
extension Directions            :Identifiable, Hashable {}
extension TimeSpan              :Identifiable, Hashable {}
extension TimeSpan.Start        :Identifiable, Hashable {}
extension TimeSpan.End          :Identifiable, Hashable {}
extension TimeSpan.TimeComponent:Identifiable, Hashable {}
extension Location              :Identifiable, Hashable {}

// Utilities
extension Date {
    static var     today: Date { Date().midnight      }
    static var yesterday: Date { Date.today.dayBefore }
    static var  tomorrow: Date { Date.today.dayAfter  }
    
    var dayBefore: Date { cal.date(byAdding:.day,value:-1,to:self)! }
    var  dayAfter: Date { cal.date(byAdding:.day,value: 1,to:self)! }
    var  midnight: Date { at(00,00)! }
    var      noon: Date { at(12,00)! }

    func at(_ h:Int,_ m:Int,_ s:Int = 0) -> Date? { cal.date(bySettingHour:h, minute:m, second:s, of:self)}

    private var cal: Calendar { Calendar.current }
}
func -<T: RangeReplaceableCollection>(lhs:T,rhs:T) -> T where T.Iterator.Element:Identifiable { var lhs = lhs; rhs.forEach { el in if let idx = lhs.firstIndex(where:{$0.id == el.id}) { lhs.remove(at:idx) } }; return lhs }
func -<T: RangeReplaceableCollection>(lhs:T,rhs:T) -> T where T.Iterator.Element:Equatable    { var lhs = lhs; rhs.forEach { el in if let idx = lhs.firstIndex(of:el                 ) { lhs.remove(at:idx) } }; return lhs }
func ==(lhs:String.SubSequence,rhs:String) -> Bool { String(lhs) == rhs }
// ============================================
// Example usage
let i = TodoItem(title:"task 0")
let j = TodoItem(title:"task 1")
let k = TodoItem(title:"task 2")

let todos = TodoList()
    .alter(
            .execute({ l in print("«todos» contains no item    : \(l.items.isEmpty)") }),
        .add(.item(i)),
        .add(.item(j)),
        .add(.item(k)),
            .execute({ l in print("items have expected titles  : \(l.items.map{ $0.title } == ["task 0","task 1","task 2"])") }),
            .execute({ l in print("«todos» contains three items: \(l.items.count == 3)") }),
            .execute({ l in print(l) }),
            .execute({ _ in print(".................") }),
        .remove(.item(i)),
            .execute({ l in print("«todos» contains two item   : \(l.items.count == 2)") }),
            .execute({ l in print(l) }),
            .execute({ _ in print(".................") }),
        .update(
            .item(
                j.alter(
                    .title("first task !!"),
                    .due  (.timeSpan(.start(.from(.tomorrow.noon),.to(.tomorrow.dayAfter.noon)))),
                    .finish))),
            .execute({ l in print("«todos» contains two item           : \( l.items.count == 2)")       }),
            .execute({ l in print("first item is not completed         : \(!l.items.first!.completed)") }),
            .execute({ l in print("second item is completed            : \( l.items .last!.completed)") }),
            .execute({ l in print("second item's title ends in bangs   : \( l.items .last!.title.suffix(3) == " !!")") }),
            .execute({ l in print("second item's due date tomorrow noon: \( l.items .last!.due.timeSpan == .start(.from(.tomorrow.at(12,00)!),.to(.tomorrow.dayAfter.at(12,00)!)))") }),
            .execute({ l in print(l) }),
            .execute({ _ in print(".................") }))

//: [Next](@next)
