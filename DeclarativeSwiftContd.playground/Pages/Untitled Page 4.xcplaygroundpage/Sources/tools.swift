import Foundation

public func -<T: RangeReplaceableCollection>(lhs: T, rhs: T) -> T where T.Iterator.Element: Identifiable {
    var lhs = lhs
    for element in rhs {
        if let index = lhs.firstIndex(where: { $0.id == element.id}) { lhs.remove(at: index) }
    }
    return lhs
}
public func -<T: RangeReplaceableCollection>(lhs: T, rhs: T) -> T where T.Iterator.Element: Equatable {
    var lhs = lhs
    for element in rhs {
        if let index = lhs.firstIndex(of:element) { lhs.remove(at: index) }
    }
    return lhs
}
