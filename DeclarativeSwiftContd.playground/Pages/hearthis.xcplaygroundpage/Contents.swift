//: [Previous](@previous)
import Foundation
import PlaygroundSupport

struct Artist {
    enum Change {
        case username(String)
        case permalink(String)
        case permalinkURL(URL)
        case avatarURL(URL)
    }
    let id:String
    let permalink:String
    let permalinkURL:URL?

    let username:String
    let avatarURL:URL?
    init(id _id:String) {
        self.init(_id, "","", nil, nil)
    }
    
    private init(_ _id:String,
                 _ _username:String,
                 _ _permalink:String,
                 _ _permalinkURL:URL?,
                 _ _avatarURL:URL?)
    {
        id           = _id
        username     = _username
        permalink    = _permalink
        permalinkURL = _permalinkURL
        avatarURL    = _avatarURL
    }
    func alter(_ c:Change...) -> Self { c.reduce(self) { $0.alter($1) } }
    
    private func alter(_ c:Change) -> Self {
        switch c {
        case let .username    (u): return Self(id,u       ,permalink,permalinkURL,avatarURL)
        case let .permalink   (p): return Self(id,username,p        ,permalinkURL,avatarURL)
        case let .permalinkURL(p): return Self(id,username,permalink,p           ,avatarURL)
        case let .avatarURL   (a): return Self(id,username,permalink,permalinkURL,a        )
        }
    }
}
extension Artist:Identifiable, Codable {}
enum AError: Error {
    case unknown
}

struct Track {
    let id:String
    let createdAt: Date
    let releasedAt:Date
    let artistID: String
    let duration:Int
    let stream: URL
}
let df = {
    let df = DateFormatter()
    df.dateFormat = "yyyy-MM-dd HH:mm:ss"
    return df
}()

protocol HearthisAPI {
    func fetchArtist(name:String,       _ callback: @escaping (Result<Artist, AError>) -> ())
    func fetchTracks(for artist:Artist, _ callback: @escaping (Result<[Track],AError>) -> ())
}

class HearthisV2API:HearthisAPI {
    let baseURL:String
    init(baseURL bu:String = "https://api-v2.hearthis.at/") {
        baseURL = bu
    }
    func fetchTracks(for artist:Artist, _ callback: @escaping (Result<[Track], AError>) -> ()) {
        let url = URL(string: "\(baseURL)\(artist.permalink)/?type=tracks&page=1&count=5")!
        let request = URLRequest(url: url, cachePolicy: .reloadIgnoringLocalCacheData, timeoutInterval: 60.0)
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            if let error = error as NSError? { print("task transport error \(error.domain) / \(error.code)"); return }
            do {
                if
                 let data,
                 let json = try? JSONSerialization.jsonObject(with:String(decoding:data, as: UTF8.self).data(using: .utf8)!, options : .allowFragments) as? [Dictionary<String,Any>]
                {
                    let tracks = json.map {
                        Track(id: $0["id"] as! String,
                              createdAt : df.date(from:$0["created_at"  ] as! String)!,
                              releasedAt: df.date(from:$0["release_date"] as! String)!,
                              artistID  : artist.id,
                              duration  : Int($0["duration"] as! String)!,
                              stream    : URL(string:$0["stream_url"] as! String)!
                        )
                    }
                    callback(.success(tracks))
                } else {
                    callback(.failure(.unknown))
                }
            }
        }.resume()
    }
    
    func fetchArtist(name:String, _ callback: @escaping (Result<Artist, AError>) -> ()) {
        let url = URL(string: "\(baseURL)\(name)/")!
        let request = URLRequest(url: url, cachePolicy: .reloadIgnoringLocalCacheData, timeoutInterval: 60.0)
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            if let error = error as NSError? { print("task transport error \(error.domain) / \(error.code)"); return }
            if
                let data,
                let jsonDict = try? JSONSerialization.jsonObject(with:String(decoding:data,as:UTF8.self).data(using: .utf8)!, options : .allowFragments) as? Dictionary<String,Any>
            {
                let artist = Artist(id:jsonDict["id"] as! String)
                    .alter(
                        .username    (           jsonDict["username"     ] as! String),
                        .permalink   (           jsonDict["permalink"    ] as! String),
                        .permalinkURL(URL(string:jsonDict["permalink_url"] as! String)!),
                        .avatarURL   (URL(string:jsonDict["avatar_url"   ] as! String)!))
                callback(.success(artist))
            } else {
                callback(.failure(.unknown))
            }
             
        }.resume()
    }
}

let h:HearthisAPI = HearthisV2API()
var artist: Artist? = nil {
    didSet {
        artist != nil
        ? h.fetchTracks(for:artist!) {
            switch $0 {
            case .success(let tracks): tracks.forEach { print($0) }
            case .failure(let e     ): print(e)
            }
        }
        : ()
    }
}


h.fetchArtist(name:"beatsnbreaks") {
switch $0 {
case let .success(a): artist = a
case let .failure(e): print(e)
}
}


PlaygroundPage.current.needsIndefiniteExecution = true

//: [Next](@next)

