//: [Previous](@previous)
import Foundation

enum FontStyle {
    enum Range {
        case range(from:Int, to:Int)
    }
    case bold(Range)
    case italic(Range)
}
enum Fragment {
    case sentence(String)
}
enum Author {
    case single(name:String)
    case mutliple(names:[String])
}
enum Text {
    case plain([Fragment])
    case titled(String, text:[Fragment])
}
struct List {
    struct Item {
        let text:Fragment
    }
    let listItems:[Item]
    
}
enum Prefomatted {
    case plain(String)
    case code(String, kind:String)
}
enum Illustration {
    case image  (uri:URL)
    case video  (uri:URL)
    case table  (uri:URL)
    case website(uri:URL)
}
struct Paragraph {
    init(content c:Content, styles s:[FontStyle] = []) {
        content = c
        styles = s
    }
    let content:Content
    let styles:[FontStyle]
    
    enum Content {
        case text(Text)
        case list(List)
        case prefomatted(Prefomatted)
        case illustration(Illustration)
    }
}
struct Chapter {
    let title:String
    let paragraphs:[Paragraph]
}
struct Book {
    let author:Author
    let title:String
    let chapters:[Chapter]
}

let b = Book(author:
        .single(name: "Manuel Meyer"),
             title: "The Declarative Revolution",
             chapters: [
                .init(title: "Introduction", paragraphs: [
                    .init(content: .text(
                        .titled("Welcome",
                                text: [
                                    .sentence("Lorem ipsum dolor sit amet, consectetur adipiscing elit."),
                                    .sentence("Vivamus eleifend."),
                                    .sentence("Nulla id turpis ipsum, etiam ullamcorper sem at leo vehicula, non varius ante lobortis."),
                                    .sentence("Phasellus.")
                                ]
                               )
                    ), styles: [.bold(.range(from:0,to:11))]
                    ),
                    .init(content:.prefomatted(
                        .code(
                            """
                                typealias Stack<T> = (push:(T) -> (), pop:() -> (T?))
                                func createStack<T>() -> Stack<T> {
                                    var array:[T] = [];
                                    return (push: { array.append($0) },
                                            pop: { array.count > 0 ? array.remove(at: array.count - 1) : nil })
                                }
                                let s:Stack<Int> = createStack()
                                s.push(1)
                                s.push(2)
                                print(String(describing:s.pop())) // 2
                                print(String(describing:s.pop())) // 1
                                print(String(describing:s.pop())) // nil
                            """,
                            kind:"swift")))
                    
                ]
                )
             ]
)

print("Phasellus at est sapien".count)
