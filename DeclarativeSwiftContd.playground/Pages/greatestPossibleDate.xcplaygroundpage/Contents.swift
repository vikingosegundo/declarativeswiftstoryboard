//: [Previous](@previous)

import Foundation

extension Date {
    static func greatestPossibleDate() -> Self {
        var i = 0.0
        i = i + pow(2,47)
        i = i + pow(2,45)
        i = i + pow(2,42)
        i = i + pow(2,41)
        i = i + pow(2,40)
        i = i + pow(2,37)
        i = i + pow(2,36)
        i = i + pow(2,35)
        i = i + pow(2,34)
        i = i + pow(2,32)
        i = i + pow(2,30)
        i = i + pow(2,29)
        i = i + pow(2,26)
        i = i + pow(2,25)
        i = i + pow(2,24)
        i = i + pow(2,22)
        i = i + pow(2,20)
        i = i + pow(2,19)
        i = i + pow(2,17)
        i = i + pow(2,16)
        i = i + pow(2,15)
        i = i + pow(2,14)
        i = i + pow(2,13)
        i = i + pow(2,11)
        i = i + pow(2,10)
        i = i + pow(2, 9)
        return Date(timeIntervalSince1970:i)
    }
}


let f = DateFormatter()
f.timeZone = .gmt
f.dateStyle = .medium
f.timeStyle = .full

let d = Date.greatestPossibleDate()
dump(d)
dump(f.string(from: d))

//: [Next](@next)
