//: [Previous](@previous)

struct Weight {
    enum Value {
        case gramms(Double)
        case kilogramms(Double)
        case pounds(Double)
    }
    init(_ value:Value) {
        switch value {
        case     .gramms(let g  ): gramms = g
        case .kilogramms(let kg ): gramms = kg  * 1000.0
        case     .pounds(let lbs): gramms = lbs * 453.59
        }
    }
    private init (amountInGramms a: Double) { gramms = a }
    let gramms    :Double
    var kilogramms:Double { gramms / 1000.0 }
    var pounds    :Double { gramms / 453.59 }
    
    static func +(lhs: Weight, rhs:Weight.Value) -> Weight {
       switch rhs {
       case     .gramms(let gs ): return Weight(amountInGramms: lhs.gramms + gs)
       case .kilogramms(let kgs): return Weight(amountInGramms: lhs.gramms + kgs * 1000)
       case     .pounds(let ps ): return Weight(amountInGramms: lhs.gramms + ps * 453.59)
       }
    }
    static func -(lhs: Weight, rhs:Weight.Value) -> Weight {
       switch rhs {
       case     .gramms(let gs ): return Weight(amountInGramms: lhs.gramms - gs)
       case .kilogramms(let kgs): return Weight(amountInGramms: lhs.gramms - kgs * 1000)
       case     .pounds(let ps ): return Weight(amountInGramms: lhs.gramms - ps * 453.59)
       }
    }
}
func +(lhs:Weight.Value, rhs:Weight.Value) -> Weight { Weight(lhs) + rhs }
func -(lhs:Weight.Value, rhs:Weight.Value) -> Weight { Weight(lhs) - rhs }

extension Double {
    var kg:Weight.Value { .kilogramms(self) }
    var  g:Weight.Value {     .gramms(self) }
    var lb:Weight.Value {     .pounds(self) }
}

let w0:Weight = 2000.g + 1.kg + 2.2.lb + 2.102.g
let w1:Weight = .gramms(2000) + .kilogramms(1) + .pounds(2.2) + .gramms(2.102)

print(w0.kilogramms) // 4.0
print(w1.kilogramms) // 4.0

//: [Next](@next)
