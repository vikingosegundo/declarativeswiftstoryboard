//: [Previous](@previous)

typealias E = Error

struct VendingMachine {
    enum Error:E, Equatable {
        case invalidTransition(Transition, from:State)
    }
    enum Transition: Equatable {
        case start  // idle         -> coinInserted
        case fetch  // coinInserted -> fetching
        case serve  // fetching     -> serving
        case finish // serving      -> idle
        case cancel // (idle|coinInserted|fetching) -> idle
    }
    enum State: Equatable {
        case idle
        case coinInserted
        case fetching
        case serving
    }
    enum Register {
        case errorHandler((Error,VendingMachine) -> ())
        case listener    ((State,VendingMachine) -> ())
    }
    init() { self.init(.idle, [], []) }
    let state:State
    func change  (_ t:Transition...) -> Self { t.reduce(self) { $0.change  ($1) } }
    func register(_ r:Register  ...) -> Self { r.reduce(self) { $0.register($1) } }
    
    private let handlers : [(Error,VendingMachine) -> ()]
    private let listeners: [(State,VendingMachine) -> ()]
}
extension VendingMachine.Error:CustomStringConvertible {
    var description: String {
        switch self {
        case let .invalidTransition(t,from:s): return "invalid transition «\(t)» from «\(s)»"
        }
    }
}
private extension VendingMachine {
    init(_ s:State,
         _ h:[(Error,Self) -> ()],
         _ l:[(State,Self) -> ()]) {
        state = s; handlers = h; listeners = l
    }
    func change(_ t:Transition) -> Self {
        let next:Self
        switch (state,t) {
        case (.idle        ,.start )                    : next = Self(.coinInserted,handlers,listeners)
        case (.coinInserted,.fetch )                    : next = Self(.fetching    ,handlers,listeners)
        case (.fetching    ,.serve )                    : next = Self(.serving     ,handlers,listeners)
        case (.serving     ,.finish)                    : next = Self(.idle        ,handlers,listeners)
        case (let s        ,.cancel) where s != .serving: next = Self(.idle        ,handlers,listeners)
        case (let s        ,let t  )                    : next = self
            handlers.forEach { $0(.invalidTransition(t,from:s),next) }
            return self
        }
        state != next.state
            ? listeners.forEach { $0(next.state,next) }
            : ()
        return next
    }
    func register(_ r:Register) -> Self {
        switch r {
        case let .errorHandler(h): return Self(state,handlers+[h],listeners    )
        case let .listener    (s): return Self(state,handlers,    listeners+[s])
        }
    }
}


//=================================
var detected:VendingMachine.Error?
func handleError(_ e:VendingMachine.Error,on m:VendingMachine) {
    detected = e
}
func stateChanged(to s:VendingMachine.State,on m:VendingMachine) {
    print("# state updated:\(s) on:\(m)")
}
//=================================

print("\nhappy path\n==========")
let vm00 = VendingMachine()
    .register(.errorHandler(handleError (_: on:)))
    .register(.listener    (stateChanged(to:on:)),
              .listener    (stateChanged(to:on:)))
let vm01 = vm00.change(.start)
let vm02 = vm01.change(.fetch)
let vm03 = vm02.change(.serve)
let vm04 = vm03.change(.finish)

print(vm00.state); assert(vm00.state == .idle        )
print(vm01.state); assert(vm01.state == .coinInserted)
print(vm02.state); assert(vm02.state == .fetching    )
print(vm03.state); assert(vm03.state == .serving     )
print(vm04.state); assert(vm04.state == .idle        )
print("\(detected?.description ?? "no error")"); assert(detected == nil)

print("\ncancel fetch\n==========")
let vm10 = VendingMachine()
    .register(.errorHandler(handleError(_:on:)))
let vm11 = vm10.change(.start)
let vm12 = vm11.change(.fetch)
let vm13 = vm12.change(.cancel)
print(vm10.state); assert(vm10.state == .idle        )
print(vm11.state); assert(vm11.state == .coinInserted)
print(vm12.state); assert(vm12.state == .fetching    )
print(vm13.state); assert(vm13.state == .idle        )
print("\(detected?.description ?? "no error")"); assert(detected == nil)


print("\ncancel coinInserted\n==========")
let vm20 = VendingMachine()
    .register(.errorHandler(handleError(_:on:)))
let vm21 = vm10.change(.start)
let vm22 = vm11.change(.cancel)
print(vm20.state); assert(vm20.state == .idle        )
print(vm21.state); assert(vm21.state == .coinInserted)
print(vm22.state); assert(vm22.state == .idle        )
print("\(detected?.description ?? "no error")"); assert(detected == nil)

print("\ncancel idle\n==========")
let vm30 = VendingMachine()
    .register(.errorHandler(handleError (_: on:)))
    .register(.listener    (stateChanged(to:on:)))
let vm31 = vm10.change(.cancel)
print(vm30.state); assert(vm30.state == .idle)
print(vm31.state); assert(vm31.state == .idle)
print("\(detected?.description ?? "no error")"); assert(detected == nil)

print("\nunhappy path\n==========")
let vm40 = VendingMachine()
    .register(.errorHandler(handleError (_: on:)))
    .register(.listener    (stateChanged(to:on:)))
let vm41 = vm40.change(.start)
let vm42 = vm41.change(.fetch)
let vm43 = vm42.change(.serve)
let vm44 = vm43.change(.cancel) // invalid, errorhandler will be called

print(vm40.state); assert(vm40.state == .idle        )
print(vm41.state); assert(vm41.state == .coinInserted)
print(vm42.state); assert(vm42.state == .fetching    )
print(vm43.state); assert(vm43.state == .serving     )
print(vm44.state); assert(vm44.state == .serving     ) // canceling was ignored
print("\(detected?.description ?? "no error")"); assert(detected == VendingMachine.Error.invalidTransition(.cancel, from: .serving))
detected = nil
//: [Next](@next)
