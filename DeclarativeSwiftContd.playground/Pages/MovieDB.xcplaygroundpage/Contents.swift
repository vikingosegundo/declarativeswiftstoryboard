//: [Previous](@previous)

import Foundation

struct Person {
    let name:String
}

enum Director {
    case unknown
    case director(Person)
    case duo(Person,Person)
    case group([Person])
}
struct Movie {
    let title:String
    let director: Director
}

let m0 = Movie(title:"The Matrix",director: .duo(Person(name:"Lilly Wachowski"),Person(name:"Lana Wachowski")))
print(m0)
//: [Next](@next)
