//: [Previous](@previous)

import Foundation

typealias E = Error

struct Item {
    let title:String
    let price:Double
}

struct ShoppingBasket {
    let positions:[Position]
    let funds:Double
    let errorhandlers: [(Error) -> ()]
    enum Change {
        case add(Add); enum Add {
            case position(Position)
            case funds(Double)
        }
        case register(Register); enum Register {
            case errorHandler((Error) -> ())
        }
        case checkout
        case inspect((ShoppingBasket) -> ())
    }
    enum Error:E {
        case amount(Amount,for:ShoppingBasket.Change); enum Amount {
            case negative
        }
        case funds(Funds,for:ShoppingBasket.Change); enum Funds {
            case negative
            case unsufficient(needed:Double,provided:Double)
        }
    }
    enum Position {
        case amount(Int,of:Item)
        var amount: Int { switch self { case let .amount(a,of:_): return a } }
        var item  : Item{ switch self { case let .amount(_,of:i): return i } }
    }
    
    init() { self.init(0.0,[],[]) }
    func alter(_ c:Change...) -> Self { c.reduce(self) { $0.alter($1) } }
    func alter(_ c:[Change] ) -> Self { c.reduce(self) { $0.alter($1) } }
}

private extension ShoppingBasket {
    init(_ f:Double,_ p:[Position],_ e:[(Error) -> ()]) { positions = p; funds = f; errorhandlers = e }
    func alter(_ c:Change) -> Self {
        switch c {                            //     |<condition>| |<--- condition is true, alter instance ---->| : |<----------------------- condition is false, inform error handlers, return unaltered ---------------------->|
        case let .add(.position(p))         : return  p.amount>0 ? Self(funds   ,positions+[p],errorhandlers    ) : {errorhandlers.forEach{$0(.amount(.negative                                           ,for:c)) }; return self}()
        case let .add(.funds   (f))         : return         f>0 ? Self(funds+f ,positions    ,errorhandlers    ) : {errorhandlers.forEach{$0(.funds (.negative                                           ,for:c)) }; return self}()
        case     .checkout                  : return newFunds>=0 ? Self(newFunds,[]           ,errorhandlers    ) : {errorhandlers.forEach{$0(.funds (.unsufficient(needed:positions.total,provided:funds),for:c)) }; return self}()
        case let .register(.errorHandler(h)): return               Self(funds   ,positions    ,errorhandlers+[h])
        case let .inspect(f): f(self)       ; return self
        }
    }
    var newFunds:Double { funds - positions.total }
}

extension Sequence where Element == ShoppingBasket.Position {
    var total:Double { reduce(0.0) { $0 + Double($1.amount) * $1.item.price } }
}

func handle(error e:ShoppingBasket.Error) {
    switch e {
    case let .funds (.unsufficient(needed:n,provided:p),for:c): print("ERROR: unsufficient funds — needed:\(n), provided:\(p) on:\(c)")
    case let .funds (.negative                         ,for:c): print("ERROR: cannot process negative funds on:\(c)")
    case let .amount(.negative                         ,for:c): print("ERROR: cannot process negative amount on:\(c)")
    }
}
let beer = Item(title:"Beer", price: 3.0)
let cola = Item(title:"Cola", price: 2.5)

var basket = ShoppingBasket().alter(
    .register(.errorHandler(handle(error:))))

basket = basket.alter(
      .add(.position(.amount(1, of:beer)))
    , .add(.position(.amount(2, of:cola)))
    , .add(.position(.amount(-1, of:beer)))
)
basket = basket.alter(
      .add(.funds(5.0))
    , .add(.funds(2.0))
    , .add(.funds(-2.0))
    , .add(.funds(2.0))
)
basket = basket.alter(
      .inspect({ print($0)})
    , .checkout
    , .inspect({print("RESULT: \($0)")})
)
//: [Next](@next)
