//: [Previous](@previous)
import Foundation

struct Book {
    let title: String
    let authors:[Authorship]
    let chapters:[Chapter]
}

struct Authorship {
    enum Kind {
        case main
        case supporting
    }
    let author:Author
    let kind:Kind
}

struct Author {
    let name:String
}

struct Chapter {
    let title:String
    let paragraphs:[Paragraph]
}

struct Paragraph {
    let content:Content
}

enum Content {
    case text(Text)
    case list(List)
}

enum Text {
    case plain(String)
    case titled(String, text:String)
    case preformatted(Prefomatted)
}

enum List {
    case items([Item])
}

enum Item {
    case text(String)
    case list(String,List)
}

enum Prefomatted {
    case plain(String)
    case code(String, language:String)
}

var book = Book(title: "Declarative Revolution",
                authors: [Authorship(author: Author(name: "Manuel Meyer"), kind: .main)],
             chapters: [
                Chapter(title: "Chapter 1",
                        paragraphs: [
                            Paragraph(content: .text(.titled("Lorem", text: "Ipsum"))),
                            Paragraph(content: .text(.plain("Lorem ipsum"))),
                            Paragraph(content: .text(.preformatted(.code("let x = \"hello world\"", language: "swift"))))
                        ]),
                Chapter(title: "Chapter 2",
                        paragraphs: [
                            Paragraph(content:
                                .list(
                                    .items([
                                        .text("Item 1"),
                                        .text("Item 2"),
                                        .text("Item 3"),
                                        .list("Item 4",
                                              .items([
                                                .text("Item 4.1"),
                                                .text("Item 4.2"),
                                                .text("Item 4.3"),
                                                .list("Item 4.4",
                                                    .items([
                                                        .text("Item 4.4.1"),
                                                        .text("Item 4.4.2")])
                                                      )
                                              ])
                                        ),
                                        .text("Item 5")
                                    ])
                                )
                            )
                        ])
             ]
)

var l = PrettyPrinter()

print("----------")
print("+ dump 0 +")
print("==========")
dump(book,to:&l)
//===================
