//: [Previous](@previous)

import Foundation


func checkAnagram(str1: String, str2: String) -> Bool {
       NSCountedSet(array: str1.lowercased().split(separator: ""))
    == NSCountedSet(array: str2.lowercased().split(separator: ""))
}

print(checkAnagram(str1: "tried", str2: "tired"))
//: [Next](@next)
