//: [Previous](@previous)

import Foundation

extension Sequence {
    func compacted<T>() -> [T] where Element == T? {
        compactMap{$0}
    }
}

let l = [1,2,3,nil,4,5,nil,6,7,nil]
let l1 = l.compacted()
print(l1)

let u = [1,2,3,nil,4,5,nil,6,7,nil]
let u1 = u.compactMap{ $0 }.map{ $0 * 2}
let u2 = u1.reduce(0,+)
print(u1)
print(u2)

let s = "String"

