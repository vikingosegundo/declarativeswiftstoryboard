//: [Previous](@previous)

import CoreGraphics

typealias Size = (width:Int, height:Int)
struct Coordinate:Hashable, Equatable {
    enum Change {
        case dec(Value),
             inc(Value); enum Value { case x,y }
    }
    
    init(x:Int, y:Int) { self.x = x; self.y = y }
    
    let x, y:Int
    func alter(_ cs: Change...) -> Self { cs.reduce(self) { $0.alter($1) } }
    func inc(_ v:Change.Value...) -> Self { v.reduce(self) { $0.alter(.inc($1)) } }
    func dec(_ v:Change.Value...) -> Self { v.reduce(self) { $0.alter(.dec($1)) } }
    
    private
    func alter(_ c: Change) -> Self {
        switch c {
        case .dec(.x): return .init(x: x - 1, y: y    )
        case .dec(.y): return .init(x: x    , y: y - 1)
        case .inc(.x): return .init(x: x + 1, y: y    )
        case .inc(.y): return .init(x: x    , y: y + 1)
        }
    }
}
struct Cell {
    enum Change  { case content(Content), state(State) }
    enum State   { case unrevealed, revealed           }
    enum Content { case undecided, empty, mine         }
    
    func alter(_ cs: Change...) -> Self { cs.reduce(self) { $0.alter($1) } }
    
    let coordiante: Coordinate
    let content: Content
    let state: State
    
    private
    func alter(_ c: Change) -> Self {
        switch c {
        case let .content(content): return .init(coordiante: coordiante, content: content, state: state)
        case let   .state(state  ): return .init(coordiante: coordiante, content: content, state: state)
        }
    }
}
struct Board {
    enum State:Equatable {
        case ongoing
        case finshed(_Outcome); enum _Outcome:Equatable {
            case won, lost(stepedOnMineAt:Coordinate)
        }
    }
    enum Change {
        case unveal(_Unveal); enum _Unveal {
            case coordinate(Coordinate)
        }
    }
    let allCells: [Cell]
    let size: Size
    let state: State
    
    init(size: Size, numberOfMines: Int) {
        assert(size.width * size.height > (numberOfMines))
        self.init(createCells(for:size), size, numberOfMines, .ongoing)
    }
    func alter(_ cs: Change... ) -> Self  { cs.reduce(self)       { $0.alter($1) }        }
    func cell(for c: Coordinate) -> Cell? { allCells.first(where: { $0.coordiante == c }) }
    func neigbouringCells(of c: Coordinate) -> [Cell] { // get the - upto — 8 neighbours of a coordinate
        allCells.filter { [
            c.dec(.x,.y)     ,   c.dec(.y),   c.inc(.x).dec(.y),
            c.dec(.x)        ,   /*c*/        c.inc(.x)        ,
            c.dec(.x).inc(.y),   c.inc(.y),   c.inc(.x,.y)
        ].contains($0.coordiante) }
    }
    private init(_ allCells: [Cell],_ size: Size,_ numberOfMines: Int,_ state:State) {
        self.allCells      = allCells
        self.numberOfMines = numberOfMines
        self.size          = size
        self.state         = state
    }
    private let numberOfMines: Int
    private func alter(_ c: Change) -> Self {
        guard  state == .ongoing else { return self }
        print(c)
        switch c {
        case let .unveal(.coordinate(c)): return .init(newCells(coordinate: c), size, numberOfMines, newState(coordinate: c))
        }
    }
}
private extension Board {
    private func newCells(coordinate c:Coordinate) -> [Cell] {
        let old = allCells.first(where: { $0.coordiante == c })!
        if old.content == .undecided {
            let suffled    = allCells.filter { $0.coordiante != old.coordiante }.shuffled()
            let mines      = suffled.prefix(numberOfMines                   ).map { $0.alter(.content(.mine )) }
            let emptyCells = suffled.suffix(allCells.count - 1 - mines.count).map { $0.alter(.content(.empty)) }
            return (mines + emptyCells + [old.alter(.content(.empty), .state(.revealed))])
        }
        return allCells.filter {$0.coordiante != old.coordiante } + [old.alter(.state(.revealed))]
    }
    private func newState(coordinate c:Coordinate) -> Board.State {
        allCells.first(where: { $0.coordiante == c })!.content == .mine ? .finshed(
            allCells.filter { $0.state == .revealed }.count == (size.width * size.height) - 1
            ? .won
            : .lost(stepedOnMineAt: c)
        ) : state
    }
}

func createCells(for size:Size) -> [Cell] {
    (0 ..< size.width * size.height).map {
        Cell(coordiante: .init(x: $0 / size.width, y: $0 % size.width), content: .undecided, state: .unrevealed )
    }
}

let checks: [Coordinate] = Array( [
    .init(x: 0, y: 0),
    .init(x: 0, y: 1),
    .init(x: 0, y: 2),
    .init(x: 1, y: 0),
    .init(x: 1, y: 1),
    .init(x: 1, y: 2),
    .init(x: 2, y: 0),
    .init(x: 2, y: 1),
    .init(x: 2, y: 2)
])//.shuffled())
let b = Board(size: Size(3,3), numberOfMines: 1)
    .alter(
        .unveal(.coordinate(checks[0])),
        .unveal(.coordinate(checks[1])),
        .unveal(.coordinate(checks[2])),
        .unveal(.coordinate(checks[3])),
        .unveal(.coordinate(checks[4])),
        .unveal(.coordinate(checks[5])),
        .unveal(.coordinate(checks[6])),
        .unveal(.coordinate(checks[7])),
        .unveal(.coordinate(checks[8])))

print((0..<20).reduce("\n\n", { x, _ in x + "=" }))
b.allCells.sorted { $0.coordiante.y < $1.coordiante.y }.sorted { $0.coordiante.x < $1.coordiante.x}.forEach { print($0) }
print(b.allCells.count)
print(b.state)
print((0..<20).reduce("\n\n", { x, _ in x + "+" }))

b.neigbouringCells(of: .init(x: 2, y: 0)).forEach { print($0) }
//}

//: [Next](@next)
