//: [Previous](@previous)

struct Life {
    enum Change {
        case process
        case set (_Set); enum _Set {
            case cells([Cell])
        }
    }
    struct Cell: Hashable, Equatable {
        init(coordinate: Coordinate, state:State = .alive) {
            self.coordinate = coordinate
            self.state = state
        }
        let coordinate: Coordinate; struct Coordinate: Equatable, Hashable {  let x, y: Int   }
        let state     : State;        enum State     :            Hashable { case alive, dead }
    }
    init(coordinates: [Cell.Coordinate]) { self.init(cells:coordinates.map { Cell(coordinate:$0) }) }
    init(cells      : [Cell           ]) { self.init(step:0, cells: cells, prevoius: nil)           }
    func alter(_ cs: Change...) -> Self { cs.reduce(self) { $0.alter($1) } }
    func alter(_ c : Change   ) -> Self {
        switch c {
        case let .set(.cells(cells)): return .init(step:step + 1, cells: cells, prevoius:self)
        case     .process           : return alter(.set(.cells(applyRules())))
        }
    }
    func debug(_ exe: (Self) -> ()) -> Self { exe(self); return self }
    let step    : Int
    let cells   : [Cell]
    var previous: Life? { _previous?.first }; private let _previous: [Life]?
}
private extension Life {
    init(step: Int, cells: [Cell] = [], prevoius:Life?) {
        self.step = step
        self.cells = cells
        self._previous = prevoius != nil ? [prevoius!] : []
    }
    func applyRules() -> [Life.Cell] {
        Array(Set(cells.flatMap { neighbors(for:$0) }))
            .map {
                let neigbours = neighbors(for: $0)
                let isAlive:Bool
                switch (neigbours.filter { n in n.state == .alive }.count, $0.state) {
                case (0...1, .alive): isAlive = false
                case (2...3, .alive): isAlive = true
                case (4...8, _     ): isAlive = false
                case (3,      .dead): isAlive = true
                case (_,      _    ): isAlive = false
                }
                return .init(coordinate: .init(x: $0.coordinate.x, y: $0.coordinate.y), state: isAlive ? .alive : .dead)
            }.filter {
                $0.state == .alive
        }
    }
    func neighbors(for cell: Life.Cell) -> [Life.Cell] {
        [
            cells.first(where: { $0.coordinate.x == cell.coordinate.x - 1 && $0.coordinate.y == cell.coordinate.y - 1 }) ?? .init(coordinate: .init(x: cell.coordinate.x - 1, y: cell.coordinate.y - 1), state: .dead),
            cells.first(where: { $0.coordinate.x == cell.coordinate.x     && $0.coordinate.y == cell.coordinate.y - 1 }) ?? .init(coordinate: .init(x: cell.coordinate.x    , y: cell.coordinate.y - 1), state: .dead),
            cells.first(where: { $0.coordinate.x == cell.coordinate.x + 1 && $0.coordinate.y == cell.coordinate.y - 1 }) ?? .init(coordinate: .init(x: cell.coordinate.x + 1, y: cell.coordinate.y - 1), state: .dead),
            cells.first(where: { $0.coordinate.x == cell.coordinate.x - 1 && $0.coordinate.y == cell.coordinate.y     }) ?? .init(coordinate: .init(x: cell.coordinate.x - 1, y: cell.coordinate.y    ), state: .dead),
            cells.first(where: { $0.coordinate.x == cell.coordinate.x + 1 && $0.coordinate.y == cell.coordinate.y     }) ?? .init(coordinate: .init(x: cell.coordinate.x + 1, y: cell.coordinate.y    ), state: .dead),
            cells.first(where: { $0.coordinate.x == cell.coordinate.x - 1 && $0.coordinate.y == cell.coordinate.y + 1 }) ?? .init(coordinate: .init(x: cell.coordinate.x - 1, y: cell.coordinate.y + 1), state: .dead),
            cells.first(where: { $0.coordinate.x == cell.coordinate.x     && $0.coordinate.y == cell.coordinate.y + 1 }) ?? .init(coordinate: .init(x: cell.coordinate.x    , y: cell.coordinate.y + 1), state: .dead),
            cells.first(where: { $0.coordinate.x == cell.coordinate.x + 1 && $0.coordinate.y == cell.coordinate.y + 1 }) ?? .init(coordinate: .init(x: cell.coordinate.x + 1, y: cell.coordinate.y + 1), state: .dead),
        ]
    }
}
extension Life: CustomStringConvertible {
    var description: String {
        let s = cells.reduce("") { p, c in p + "(\(c.coordinate.x), \(c.coordinate.y))\n" }
        return "step \(step):" + (s != "" ? "\n" + s : " no cell is alive")
    }
}

Life(coordinates: [
    .init(x: -2, y: 0),
    .init(x: -1, y: 0),
    .init(x: -2, y: 1),
    .init(x: -1, y: 1),
    .init(x: -2, y: 2)]
)               .debug({ print($0) })
.alter(.process).debug({ print($0) })
.alter(.process).debug({ print($0) })
.alter(.process).debug({ print($0) })
.alter(.process).debug({ print($0) })
.alter(.process).debug({ print($0) })

print("========================")

Life(coordinates: [
    // Glider
    .init(x: 1, y: 0),
    .init(x: 2, y: 1),
    .init(x: 0, y: 2),
    .init(x: 1, y: 2),
    .init(x: 2, y: 2)]
)               .debug({ print($0) })
.alter(.process).debug({ print($0) })
.alter(.process).debug({ print($0) })
.alter(.process).debug({ print($0) })
.alter(.process).debug({ print($0) })
.alter(.process).debug({ print($0) })
.alter(.process).debug({ print($0) })
.alter(.process).debug({ print($0) })
.alter(.process).debug({ print($0) })
.alter(.process).debug({ print($0) })
.alter(.process).debug({ print($0) })
.alter(.process).debug({ print($0) })
.alter(.process).debug({ print($0) })
.alter(.process).debug({ print($0) })
.alter(.process).debug({ print($0) })
//: [Next](@next)
