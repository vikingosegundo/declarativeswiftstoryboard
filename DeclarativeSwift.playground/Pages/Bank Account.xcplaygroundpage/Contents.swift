//: [Previous](@previous)

enum Currency {
    case euro, dollar
    typealias Conversion = (src:Currency, dst:Currency, clc:(Double) -> Double)
    typealias Amount = (amount:Double, of:Currency)
}

final class Bank {
    enum Change {
        case set(_Set); enum _Set {
            case exchangerates([Currency.Conversion]) }
        case register(_Register); enum _Register {
        case observer(Observing) }
    }
    convenience init(id:Bank.ID) { self.init(id,[:], [], []) }
    func process(transaction:Bank.Transaction) {
        let id: Bank.Account.ID
        switch transaction {
        case let .withdraw(_,i): id = i
        case let .add     (_,i): id = i
        }
        let pre = (accounts[id] ?? Account(defaultCurrency, rates))
        let account = pre.alter(transaction)
        accounts[id] = account
        notifyObservers(of:transaction, with:id, for:account, wasChanged:(pre.total.amount != account.total.amount) || (pre.currency != account.currency))
    }
    func configure(_ c:Change) {
        switch c {
        case .set(.exchangerates(let r)): rates = r
        case .register(.observer(let o)): observers.append(o)
        }
    }
    private init(_ id:Bank.ID,_ a:[Account.ID:Account], _ o:[Observing], _ r:[Currency.Conversion]) { self.id = id; accounts = a; observers = o; rates = r }
    
    let id: Bank.ID
    private var accounts: [Bank.Account.ID:Bank.Account]
    private var observers: [Observing]
    private var defaultCurrency: Currency = .euro
    private var rates: [Currency.Conversion] {
        didSet { accounts = accounts.keys.reduce([:]) { var x = $0; x[$1] = accounts[$1]!.configure(.set(.exchangerates(rates))); return x } }
    }
}
extension Bank { // observer
    typealias ID = String
    typealias Observing = (Account.ID, Transaction, Transaction.Result) -> ()
    private func notifyObservers(of transaction:Transaction, with id:Account.ID, for account:Account, wasChanged:Bool) {
        observers.forEach { $0(id, transaction, wasChanged ? .changed(account) : .unchanged(account)) }
    }
}
extension Bank {
    enum Transaction {
        case add(Currency.Amount, to: Account.ID)
        case withdraw(Currency.Amount, from: Account.ID)
        enum Result {
            case   changed(Account)
            case unchanged(Account)
        }
    }
    struct Account {
        enum Change {
            case set(_Set); enum _Set {
                case exchangerates([Currency.Conversion])
            }
        }
        typealias ID = String
        let currency: Currency
        let amounts: [Currency:Double]
        let exchangerates: [Currency.Conversion]
        init(_ c:Currency,_ r:[Currency.Conversion]) { self.init(c, [:], r) }
        func alter(_ c:Transaction) -> Self {
            switch c {
            case .add     (let amount,   to:_): return .init(currency,     add(amount: (amount:amount.amount >= 0 ? amount.amount : 0, of:amount.of),   to:amounts), exchangerates)
            case .withdraw(let amount, from:_): return .init(currency,withdraw(amount: (amount:amount.amount >= 0 ? amount.amount : 0, of:amount.of), from:amounts), exchangerates)
            }
        }
        func configure(_ c:Change) -> Self{
            switch c {
            case .set(.exchangerates(let rates)): return .init(currency, amounts, rates)
            }
        }
        var total: Currency.Amount {
            ( amount:amounts.reduce(0.0) { p, a in (p + ((a.key == currency)
                                                            ? a.value // no conversion neeed
                                                            : exchangerates.first(where: { r in r.src == a.key && r.dst == currency })?.clc(a.value) ?? 0.0)) }, // if possible, convert, otherwise ommit
                  of:currency
            )
        }
        private
        init(_ c:Currency,_ a:[Currency:Double],_ r:[Currency.Conversion]) { currency = c; amounts = a; exchangerates = r }
    }
    private static func add(amount:Currency.Amount, to amounts:[Currency:Double]) -> [Currency:Double] {
        var amounts = amounts
        !amounts.keys.contains(amount.of)
            ? amounts[amount.of] = 0.0
            : ()
        amounts[amount.of] = amounts[amount.of]! + amount.amount
        return amounts
    }
    private static func withdraw(amount:Currency.Amount, from amounts:[Currency:Double]) -> [Currency:Double] {
        var amounts = amounts
        !amounts.keys.contains(amount.of)
            ? amounts[amount.of] = 0.0
            : ()
        amounts[amount.of] = amounts[amount.of]! - amount.amount
        return amounts
    }
}



let bankA = Bank(id: "a")
let bankB = Bank(id: "b")
[bankA, bankB].forEach { bank in
    bank.configure(
        .register(.observer({ id, change, result in
            switch result {
            case   .changed(let account): print("<\(id) @ \(bank.id)> new total: \(account.total)")
            case .unchanged(let account): print("<\(id) @ \(bank.id)> unchanged: \(account.total)")
            }
        }))
    )
}


bankA.configure(.set(.exchangerates([
    (src:.euro,   dst:.dollar, clc:{ $0 * 1.11 }),
    (src:.dollar, dst:.euro,   clc:{ $0 * 0.90 })] )))
bankA.process(transaction: .add((amount: 10.0, of:.euro  ), to: "001"))
bankB.process(transaction: .add((amount: 10.0, of:.euro  ), to: "002"))
bankA.process(transaction: .add((amount: 10.0, of:.dollar), to: "001"))
bankA.process(transaction: .add((amount:-10.0, of:.dollar), to: "001"))
bankA.configure(.set(.exchangerates([
    (src:.euro,   dst:.dollar, clc:{ $0 * 1.13 }),
    (src:.dollar, dst:.euro,   clc:{ $0 * 0.88 })] )))
bankA.process(transaction: .add((amount: 10.0, of:.dollar), to: "001"))
bankA.configure(.set(.exchangerates([
    (src:.euro,   dst:.dollar, clc:{ $0 * 1.15 }),
    (src:.dollar, dst:.euro,   clc:{ $0 * 0.87 })] )))
bankA.process(transaction: .add((amount: 00.0, of:.dollar), to: "001"))
bankA.process(transaction: .withdraw((amount: 10.0, of: .dollar), from: "001"))

//: [Next](@next)
