//: [Previous](@previous)

import Foundation

var x = 0
for i in 1..<6 {
    x = x + i
}

let y = (1..<6).reduce(0)  { $0 + $1 }
let z = (1..<6).reduce("") { $0 + " \($1)" }
    .trimmingCharacters(in: .whitespaces)

let u = (1..<10).reduce(""){ (1...$1).reduce($0){ $0 + " \($1*$1)" } + "\n" }
    .trimmingCharacters(in: .whitespaces)
    .replacingOccurrences(of: "\n ", with: "\n")

print(x)
print(y)
print(z)
print(u)

print(1 << 6)
print(1 << 3)
print(1 << 2)

//: [Next](@next)
