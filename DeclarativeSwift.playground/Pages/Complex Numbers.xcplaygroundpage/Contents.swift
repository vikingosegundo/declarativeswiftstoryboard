//: [Previous](@previous)

import Foundation

func alter(_ c:ComplexNumber, by changes: ComplexNumber.Change...) -> ComplexNumber {
    changes.reduce(c) { $0.alter(by:$1) }
}

struct ComplexNumber {
    enum Change { // DSL for interacting with Complex Numpers
        case adding       (   ComplexNumber)
        case substracting (   ComplexNumber)
        case multiplying  (by:ComplexNumber)
        case dividing     (by:ComplexNumber)
        case raising        (_Raising); enum _Raising {
            case it              (_It); enum _It      {
                case to       (_Power); enum _Power   {
                    case power(of:UInt)
                }
            }
        }
    }
    init(real:Double, imaginary:Double) { self.init(real, imaginary) }
    
    let real     : Double
    let imaginary: Double
    var absolute : Double { sqrt(real * real + imaginary * imaginary) }
    
    func alter(by changes:Change...) -> Self { changes.reduce(self) { $0.alter(by: $1) } }
}

extension ComplexNumber {
    private
    init(_ real:Double,_ imaginary:Double) {
        self.real      = real
        self.imaginary = imaginary
    }
    private
    func alter(by change:Change) -> Self { // Implemetation of complex numer logic
        switch change {
        case let .adding                (   cnb   ): return .init(  real + cnb.real, imaginary + cnb.imaginary)
        case let .substracting          (   cnb   ): return .init(  real - cnb.real, imaginary -  cnb.imaginary)
        case let .multiplying           (by:cnb   ): return .init( (real * cnb.real) - (imaginary * cnb.imaginary), real * cnb.imaginary + cnb.real * imaginary)
        case let .dividing              (by:cnb   ): return .init(((real * cnb.real) + (imaginary * cnb.imaginary)) / (cnb.real * cnb.real + cnb.imaginary * cnb.imaginary),(cnb.real * imaginary - real * cnb.imaginary) / (cnb.real * cnb.real + cnb.imaginary * cnb.imaginary))
        case let .raising(.it(.to(.power(of:exp)))): return (0..<(exp - 1)).reduce(self) { p, _ in p.alter(by:.multiplying(by:self)) }
        }
    }
}

extension ComplexNumber: CustomStringConvertible {
    var description: String {
        switch (real >= 0, imaginary >= 0) {
        case (false, false): return "-(\(abs(real)) + \(abs(imaginary))i)"
        case (true , false): return "\(      real ) - \(abs(imaginary))i"
        case (_    ,  true): return "\(      real ) + \(    imaginary )i"
        }
    }
}

extension ComplexNumber: Equatable {}

// Modern math notation frontend for DSL notation
func abs(_ c:ComplexNumber) -> Double { c.absolute }
func ^ (base: ComplexNumber, exp:UInt         ) -> ComplexNumber { base.alter(by:.raising(.it(.to(.power(of:exp))))) }
func * (lhs : ComplexNumber, rhs:ComplexNumber) -> ComplexNumber {  lhs.alter(by:           .multiplying(by:rhs   )) }
func / (lhs : ComplexNumber, rhs:ComplexNumber) -> ComplexNumber {  lhs.alter(by:              .dividing(by:rhs   )) }
func + (lhs : ComplexNumber, rhs:ComplexNumber) -> ComplexNumber {  lhs.alter(by:                .adding(   rhs   )) }
func - (lhs : ComplexNumber, rhs:ComplexNumber) -> ComplexNumber {  lhs.alter(by:          .substracting(   rhs   )) }

infix operator ^: ExponentiationPrecedence
infix operator *: MultiplicationPrecedence
infix operator /: MultiplicationPrecedence
infix operator +: AdditionPrecedence
infix operator -: AdditionPrecedence

precedencegroup ExponentiationPrecedence {
    associativity: right
    higherThan: MultiplicationPrecedence
}

// Example Usage
let x = ComplexNumber(real: -2, imaginary: -5)
let y = ComplexNumber(real: -3, imaginary: 7)
let z0 = alter(x, by:.raising(.it(.to(.power(of:2)))),   // x^2
                    .multiplying(by:                    // *
                        alter(y, by:.adding(y))),       // (y + y)
                    .substracting(x))                   // - x
let z1 = x^2 * (y + y) - x
let m = x * y
print(m)
print(z0)
print(z1)
print(z0 == z1)
print(abs(z0))

//: [Next](@next)
