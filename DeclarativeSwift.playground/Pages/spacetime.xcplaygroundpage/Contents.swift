//: [Previous](@previous)

import Foundation
func daysBetween(start: Date, end: Date) -> Int {
   Calendar.current.dateComponents([.day], from: start, to: end).day!
}

func add(days:Int, to date:Date) -> Date { Calendar.current.date(byAdding: DateComponents(day:days), to: date)! }
let c0:DateComponents = {
    DateComponents(year:1978, month:12, day:12)
}()
let c1:DateComponents = {
    DateComponents(year:2000, month:1, day:1)
}()
let d0 = Calendar.current.date(from:c0)
let d1 = Calendar.current.date(from:c1)

print(add(days:daysBetween(start:d0!, end:d1!), to:d1!))
//: [Next](@next)

