//: [Previous](@previous)

import Foundation

enum Currency {
    case euro, dollar
    typealias Conversion = (src:Currency, dst:Currency, clc:(Double) -> Double)
    typealias Amount = (amount:Double, of:Currency)
}

final class Bank {
    typealias ID = String
    typealias Observing = (Account.ID, Transaction, Transaction.Result) -> ()
    final class Network {
        enum Transaction {}
        typealias ID = String
        init (id:ID, banks b:[Bank] = []) { self.id = id; banks = b }
        func register(bank:Bank) { banks.append(bank) }
        func process(transaction:Transaction) {}
        var banks: [Bank]
        let id: ID
    }
    struct Account{
        enum Transaction {
            case add(amount:Currency.Amount)
            case withdraw(Currency.Amount, by:User)
        }
        typealias ID = String
        let user:User
        let id: ID
        let amounts: [Currency:Currency.Amount]
        func process(_ t:Transaction) -> Self {
            switch t {
            case let .add     (amount  ): var amounts = amounts; amounts[amount.of] = Currency.Amount(amount:(amounts[amount.of]?.amount ?? 0.0) + (amount.amount > 0.0 ? amount.amount : 0.0) , of:amount.of);  return .init(user: user, id: id, amounts: amounts)
            case let .withdraw(amount,u): var amounts = amounts; amounts[amount.of] = Currency.Amount(amount:(amounts[amount.of]?.amount ?? 0.0) - ((amount.amount > 0.0 && user.id == u.id) ? amount.amount : 0.0) , of:amount.of);  return .init(user: user, id: id, amounts: amounts)
            }
        }
    }
    struct User { let id:UUID }
    enum Change {
        case registerAccount(for:User, with:Account.ID)
        case join(network:Bank.Network)
        case set(_Set); enum _Set {
            case exchangerates([Currency.Conversion]) }
        case register(_Register); enum _Register {
        case observer(Observing) }
    }
    convenience init(id:ID) { self.init(id,[]) }
    private init(_ id:ID, _ a:[Account]) { self.id = id; accounts = a }
    func alter(_ c:Change) {
        switch c {
        case let .registerAccount(for:user,with:id): accounts.filter{ $0.id == id }.count == 0 ? accounts.append(Account(user: user, id: id, amounts: [:])) : ()
        case let .join(network:network): network.register(bank:self)
            
        }
    }
    var accounts: [Account]
    var id: Bank.ID
}

extension Bank {
    enum Transaction {
        case add(Currency.Amount, to: Account.ID)
        case withdraw(Currency.Amount, from: Account.ID, by: User)
        enum Result {
            case   changed(Account)
            case unchanged(Account)
        }
    }
    func process(_ t:Transaction) {
        switch t {
        case let .add     (amount,   to: id          ): let a = accounts.first(where: { $0.id == id })?.process(.add     (amount: amount )); accounts = accounts.filter({ $0.id != id }) + [a].compactMap{$0}
        case let .withdraw(amount, from: id, by: user): let a = accounts.first(where: { $0.id == id })?.process(.withdraw(amount, by:user)); accounts = accounts.filter({ $0.id != id }) + [a].compactMap{$0}
        }
    }
    
    func configure(_ c:Change) {
        switch c {
        case .set(.exchangerates(let r)): rates = r
        case .register(.observer(let o)): observers.append(o)
        }
    }
}
let user0 = Bank.User(id: UUID())
let user1 = Bank.User(id: UUID())

let bankA = Bank(id:"a")
let bankB = Bank(id:"b")

[bankA, bankB].forEach {
    
}

let network0 = Bank.Network(id:"0")
bankA.alter(.join(network:network0))
bankB.alter(.join(network:network0))

bankA.alter(.registerAccount(for: user0, with:"111"))
bankB.alter(.registerAccount(for: user1, with:"222"))
bankA.process(.add(( 100.0,.euro), to:"111"))
bankA.process(.add(( 100.0,.euro), to:"111"))
bankA.process(.add((-100.0,.euro), to:"111"))
bankA.process(.add(( 100.0,.dollar), to:"111"))
bankA.process(.withdraw((99.0,.euro), from: "111", by: user0))
bankA.process(.withdraw((99.0,.euro), from: "111", by: user1))

print(bankA.accounts)
//: [Next](@next)
