//
//  DoublePendulum.swift
//  Pendulum
//
//  Created by Manuel Meyer on 03.05.22.
//

import Foundation

struct DoublePendulum {
    enum Change {
        case tick
        case pause
        case unpause
        case set(_Set); enum _Set {
            case masses   ((Double,Double))
            case radii    ((Double,Double))
            case gravity  (Double)
            case dampening(Double)
        }
    }
    let ballDiameter: Double
    let hue         : Double
    let paused      : Bool
    var ancestor: Self? { _a.first }
    init( radii r: (Double, Double),
         masses m: (Double, Double),
         angles a: (Double, Double),
      dampening d: Double = 0.0,
        gravity g: Double = 9.801,
     ballDiameter: Double = 6,
              hue: Double = 0.0,
           paused: Bool   =  false
    ) {
        self.init(r,m,a,(0,0),d,g,ballDiameter,hue,paused,[])
    }
    func alter(_ changes:  Change...) -> Self { changes.reduce(self) { $0.alter($1) } }
    func alter(_ changes: [Change]  ) -> Self { changes.reduce(self) { $0.alter($1) } }
    func bobPoints() -> (
        (Double,Double),
        (Double,Double)
    ) {
        let p0 = (       r.0 * sin(a.0),        r.0 * cos(a.0))
        let p1 = (p0.0 + r.1 * sin(a.1), p0.1 + r.1 * cos(a.1))
        return (p0, p1)
    }
    private let r : (Double, Double)
    private let m : (Double, Double)
    private let a : (Double, Double)
    private let av: (Double, Double)
    private let g :  Double
    private let d :  Double
    private let _a: [Self]
}

private
extension DoublePendulum {
    init(      _ r: (Double, Double),
               _ m: (Double, Double),
               _ a: (Double, Double),
              _ av: (Double, Double),
               _ d: Double,
               _ g: Double,
    _ ballDiameter: Double,
             _ hue: Double,
          _ paused: Bool,
              _ _a: [Self]
    ) {
        // math
        self.r = r
        self.m = m
        self.a = a
        self.av = av
        self.g = g
        self.d = d
        //style & operations
        self.ballDiameter = ballDiameter
        self.hue          = hue
        self.paused       = paused
        self._a = _a
    }
    func alter(_ cmd:Change) -> Self {
        switch (paused,cmd) {
        case (false,       .tick        ): return advance()
        case (true,     .unpause        ): return .init(r,m,a,av,d,g,ballDiameter,hue,false , [self])
        case (false,      .pause        ): return .init(r,m,a,av,d,g,ballDiameter,hue,true  , [self])
        case (_, .set(.radii    (let r))): return .init(r,m,a,av,d,g,ballDiameter,hue,paused, [self])
        case (_, .set(.masses   (let m))): return .init(r,m,a,av,d,g,ballDiameter,hue,paused, [self])
        case (_, .set(.dampening(let d))): return .init(r,m,a,av,d,g,ballDiameter,hue,paused, [self])
        case (_, .set(.gravity  (let g))): return .init(r,m,a,av,d,g,ballDiameter,hue,paused, [self])
        case (_, _                      ): return self
        }
    }
    func advance() -> Self {
        let damp = (1.0 - d)
        let acc = calc()
        let av  = ((av.0 + acc.0) * damp, (av.1 + acc.1) * damp)
        let a   = (a.0 + av.0, a.1 + av.1)
        return .init(r,m,a,av,d,g,ballDiameter,hue,paused,[self])
    }
    func calc() -> (Double, Double) { // -> (pendulum arm 0, pendulum arm 1)
        //|      |<------------------ pendulum arm 0 ----------------->| |<----------- pendulum arm 1 ----------->|
        let n0 = (-g * (2 * m.0 + m.1) * sin(a.0),                        2 * sin(a.0 - a.1)                      )
        let n1 = (-m.1 * g * sin(a.0 - 2 * a.1),                          av.0 * av.0 * r.0 * (m.0 + m.1)         )
        let n2 = (-2 * sin(a.0 - a.1) * m.1,                              g * (m.0 + m.1) * cos(a.0)              )
        let n3 = (av.1 * av.1 * r.1 + av.0 * av.0 * r.0 * cos(a.0 - a.1), av.1 * av.1 * r.1 * m.1 * cos(a.0 - a.1))
        
        let d0 = r.0 * (2 * m.0 + m.1 - m.1 * cos(2 * a.0 - 2 * a.1))
        let d1 = r.1 * (2 * m.0 + m.1 - m.1 * cos(2 * a.0 - 2 * a.1))
        
        return (
            (n0.0 +  n1.0 + (n2.0 * n3.0) / d0).truncatingRemainder(dividingBy:.pi * 2) / 360.0,
            (n0.1 * (n1.1 +  n2.1 + n3.1) / d1).truncatingRemainder(dividingBy:.pi * 2) / 360.0)
    }
}

private let baseAngle = (1.0 + .pi, 1.0 + .pi)
private let offset    = 1.0e-12
private let masses    = (25.0, 25.0)
private let radii     = (50.0, 50.0)
private let dampening = 0.0002
private let hues      = (0.0, 0.66) // red & blue

let p0 = DoublePendulum(
    radii    : radii,
    masses   : masses,
    angles   : (baseAngle.0, baseAngle.1),
    dampening: dampening,
    hue      : hues.0
)






let d0 = Date()
let p10 = p0.alter( (0..<1024).map { _ in .tick } )
let d1 = Date()

print((d1.timeIntervalSince1970 - d0.timeIntervalSince1970)/1024.0)

var p = p10
repeat {
    print (p.ancestor!.bobPoints().1)
    p = p.ancestor!
} while (p.ancestor != nil)

//
//
//
//
//// paused pendulums do not advance
//let p1 = p0.alter(.pause)// paused
//let p2 = p0.alter(.tick)
//let p3 = p1.alter(.tick) // paused
//
//p1.bobPoints().0.0.isNearlyEqual(to:p0.bobPoints().0.0)
//p1.bobPoints().0.1.isNearlyEqual(to:p0.bobPoints().0.1)
//p1.bobPoints().1.0.isNearlyEqual(to:p0.bobPoints().1.0)
//p1.bobPoints().1.1.isNearlyEqual(to:p0.bobPoints().1.1)
//
//p1.bobPoints().0.0.isNotNearlyEqual(to:p2.bobPoints().0.0)
//p1.bobPoints().0.1.isNotNearlyEqual(to:p2.bobPoints().0.1)
//p1.bobPoints().1.0.isNotNearlyEqual(to:p2.bobPoints().1.0)
//p1.bobPoints().1.1.isNotNearlyEqual(to:p2.bobPoints().1.1)
//
//p1.bobPoints().0.0.isNearlyEqual(to:p3.bobPoints().0.0)
//p1.bobPoints().0.1.isNearlyEqual(to:p3.bobPoints().0.1)
//p1.bobPoints().1.0.isNearlyEqual(to:p3.bobPoints().1.0)
//p1.bobPoints().1.1.isNearlyEqual(to:p3.bobPoints().1.1)
//
//p2.bobPoints().0.0.isNotNearlyEqual(to:p0.bobPoints().0.0)
//p2.bobPoints().0.1.isNotNearlyEqual(to:p0.bobPoints().0.1)
//p2.bobPoints().1.0.isNotNearlyEqual(to:p0.bobPoints().1.0)
//p2.bobPoints().1.1.isNotNearlyEqual(to:p0.bobPoints().1.1)
//
//// changing masses alters trajectory
//let p4 = p0
//    .alter(.set(.masses((42.0, 23.0))))
//    .alter(.tick)
//let p5 = p0
//    .alter(.tick)
//
//p4.bobPoints().0.0.isNotNearlyEqual(to:p5.bobPoints().0.0)
//p4.bobPoints().0.1.isNotNearlyEqual(to:p5.bobPoints().0.1)
//p4.bobPoints().1.0.isNotNearlyEqual(to:p5.bobPoints().1.0)
//p4.bobPoints().1.1.isNotNearlyEqual(to:p5.bobPoints().1.1)
//
//
//// changing gravity alters trajectory
//let p6 = p0
//    .alter(.set(.gravity(30)))
//    .alter(.tick)
//let p7 = p0
//    .alter(.tick)
//
//p6.bobPoints().0.0.isNotNearlyEqual(to:p7.bobPoints().0.0)
//p6.bobPoints().0.1.isNotNearlyEqual(to:p7.bobPoints().0.1)
//p6.bobPoints().1.0.isNotNearlyEqual(to:p7.bobPoints().1.0)
//p6.bobPoints().1.1.isNotNearlyEqual(to:p7.bobPoints().1.1)
//
//
//// changing radii alters trajectory
//let p8 = p0
//    .alter(.set(.radii((10, 90))))
//    .alter(.tick)
//let p9 = p0
//    .alter(.tick)
//
//p8.bobPoints().0.0.isNotNearlyEqual(to:p9.bobPoints().0.0)
//p8.bobPoints().0.1.isNotNearlyEqual(to:p9.bobPoints().0.1)
//p8.bobPoints().1.0.isNotNearlyEqual(to:p9.bobPoints().1.0)
//p8.bobPoints().1.1.isNotNearlyEqual(to:p9.bobPoints().1.1)
