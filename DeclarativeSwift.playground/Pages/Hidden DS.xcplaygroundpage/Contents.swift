//: [Previous](@previous)

import UIKit
import PlaygroundSupport
import Foundation
import Darwin

enum TableViewHandler<T> {
    enum Command<T> {
        case register(AnyClass?)
        case data([T])
        case selected((UITableView, UITableViewCell, IndexPath, T) -> ())
        case cell    ((UITableView, UITableViewCell, IndexPath, String, T) -> UITableViewCell)
        case add(datapoint:T)
        case remove(at:IndexPath)
        case get(_Get); enum _Get {
            case tableview((UITableView) -> ())
            case id((String) -> ())
        }
    }
    static func create(for tv:UITableView = UITableView(frame: CGRect.zero)) -> (TableViewHandler.Command<T>) -> () {
        let ds: DS<T> = DS()
        ds.tableView = tv
        return {
            switch $0 {
            case       .register(let    c ): ds.tableView.register(c, forCellReuseIdentifier:ds.cellID)
            case           .data(let data ): ds.data     = data
            case  .add(datapoint:let    d ): ds.data     = ds.data + [d]
            case      .remove(at:let   ip ): ds.data     = Array(ds.data.enumerated().filter{ i, d in i != ip.row }.map{$1})
            case       .selected(let    s ): ds.selected = s
            case           .cell(let    c ): ds.cell     = c
            case .get(.tableview(let  get)): get(ds.tableView)
            case .get(       .id(let  get)): get(ds.cellID)
            }
        }
    }
}

private
extension TableViewHandler {
    final class DS<T>: NSObject, UITableViewDataSource, UITableViewDelegate {
        func tableView(_ tableView: UITableView, numberOfRowsInSection  section: Int) -> Int             { data.count }
        func tableView(_ tableView: UITableView, cellForRowAt   indexPath: IndexPath) -> UITableViewCell { cell(tableView, tableView.dequeueReusableCell(withIdentifier: cellID, for: indexPath),indexPath,cellID ,data[indexPath.row]) }
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)                    { selected(tableView, tableView.cellForRow(at: indexPath)! ,indexPath, data[indexPath.row]) }
        var tableView: UITableView! { didSet { tableView?.dataSource = self; tableView?.delegate   = self } }
        let cellID = UUID().uuidString
        var data    : [T] = [] { didSet { tableView?.reloadData() } }
        var selected: (UITableView, UITableViewCell, IndexPath,          T) -> ()              = { _,_,_,_ in }
        var cell    : (UITableView, UITableViewCell, IndexPath, String,  T) -> UITableViewCell = { let c = $0.dequeueReusableCell(withIdentifier: $3, for: $2); _ = $4 ; return c }
    }
}

func populate(_ tv: UITableView,
              _ c : UITableViewCell,
              _ ip: IndexPath,
              _ id: String,
              _ d : Int) -> UITableViewCell
{
    c.textLabel?.text="\(d)"
    return c
}

let tvHandler = TableViewHandler<Int>.create()
tvHandler(.register(UITableViewCell.self))
tvHandler(.get(.tableview({ tv           in tv.frame = CGRect(origin:.zero, size:.init(width:200, height:300)) } )))
tvHandler(.get(.tableview({ tv           in PlaygroundPage.current.liveView = tv                               } )))
tvHandler(.selected(      { tv,c,ip,   d in tv.deselectRow(at:ip, animated:true)                               } ))
tvHandler(    .cell(      { tv,c,ip,id,d in populate(tv, c, ip, id, d)                                         } ))
tvHandler(    .data(                         (1 ..< 4).map{$0}                                                   ))
DispatchQueue.main.asyncAfter(deadline:.now() + .milliseconds(1500)) { tvHandler(   .add(datapoint:4))                }
DispatchQueue.main.asyncAfter(deadline:.now() + .milliseconds(1700)) { tvHandler(   .add(datapoint:5))                }
DispatchQueue.main.asyncAfter(deadline:.now() + .milliseconds(2300)) { tvHandler(   .add(datapoint:6))                }
DispatchQueue.main.asyncAfter(deadline:.now() + .milliseconds(4000)) { tvHandler(.remove(at:.init(row:2, section:0))) }



//: [Next](@next)
