//: [Previous](@previous)

import Foundation

struct Person:Identifiable {
    let id: UUID
    let name: String
    
    enum Change {
        case by(_By); enum _By {
            case setting(_Set); enum _Set {
                case name(to:String)
            }
        }
    }
    init(name: String) { self.id = UUID(); self.name = name }
    
    func alter(_ c:Change...) -> Self { c.reduce(self) { $0.alter($1) } }
    private
    init(_ id:UUID,_ name:String) {
        self.id = id
        self.name = name
    }
    private
    func alter(_ c:Change) -> Self {
        switch c {
        case let .by(.setting(.name(to:name))): return .init(id, name)
        }
    }
}

struct TimeSlip: Identifiable {
    let id: UUID
    let hours:Double
    let person:Person
    
    init(hours:Double, person:Person) {
        self.init(id: UUID(), hours: hours, person: person)
    }
    private
    init(id:UUID, hours:Double, person:Person) {
        self.id = id
        self.hours = hours
        self.person = person
    }
}

enum Resource: Identifiable {
    var id: UUID {
        switch self {
        case .person(let p): return p.id
        }
    }
    case person(Person)
}

struct Task:Identifiable {
    let id: UUID
    let state:State
    let title: String
    let resources: [Resource]
    let recoredTime: [TimeSlip]
    enum State {
        case undefined
        case ongoing
        case stopped(finished:Bool) }
    enum Change {
        case by(_By); enum _By {
            case setting(_Set); enum _Set {
                case title(to:String)
                case state(to:State) }
            case adding(_Add); enum _Add {
                case resource(Resource)
                case timeslip(TimeSlip) }
            case removing(_Remove); enum _Remove {
                case resource(Resource)
                case timeslip(TimeSlip)
            }
        }
    }
    
    init(title:String) { self.init(UUID(),.undefined, title, [], []) }
    func altered(_ c:Change...) -> Self { c.reduce(self) { $0.alter($1) } }

    private
    func alter(_ c:Change) -> Self {
        switch c {
        case let .by( .setting(.state(to:new))): return .init(id, new,   title, resources,                          recoredTime                          )
        case let .by( .setting(.title(to:new))): return .init(id, state, new,   resources,                          recoredTime                          )
        case let .by(  .adding(.resource(r))  ): return .init(id, state, title, resources + [r],                    recoredTime                          )
        case let .by(  .adding(.timeslip(t))  ): return .init(id, state, title, resources,                          recoredTime + [t]                    )
        case let .by(.removing(.resource(r))  ): return .init(id, state, title, resources.filter { $0.id != r.id }, recoredTime                          )
        case let .by(.removing(.timeslip(t))  ): return .init(id, state, title, resources,                          recoredTime.filter { $0.id != t.id } )
        }
    }
        
    private init(_ id:UUID,_ state:State,_ title:String, _ resources:[Resource],_ recoredTime:[TimeSlip]){
        self.id = id
        self.state = state
        self.title = title
        self.resources = resources
        self.recoredTime = recoredTime
    }
}

struct Project:Identifiable {
    let id: UUID
    let title:String
    let description:String
    let tasks:[Task]
    
    enum Change {
        case by(_By); enum _By{
        case adding  (_Add   ); enum _Add    { case task(Task) }
        case removing(_Remove); enum _Remove { case task(Task) }
        case updating(_Update); enum _Update { case task(Task) }
        }
    }
    init(title:String) {
        self.init(UUID(),title, "", [])
    }
    
    func altered(_ c:Change...) -> Self { c.reduce(self){ $0.altered($1) } }

    private
    init(_ id:UUID,_ title:String,_ description:String, _ tasks:[Task]) {
        self.id = id
        self.title = title
        self.description = description
        self.tasks = tasks
    }
    private
    func altered(_ c:Change) -> Self {
        switch c {
        case let .by(.adding  (.task(t))): return .init(id,title, description, tasks + [t])
        case let .by(.removing(.task(t))): return .init(id,title, description, tasks.filter({ $0.id != t.id }))
        case let .by(.updating(.task(t))): return altered(.by(.removing(.task(t))), .by(.adding(.task(t))))
        }
    }
}

struct State {
    let persons:[Person]
    let projects:[Project]
    enum Change {
        case by(_By); enum _By {
            case adding  (_Add   ); enum _Add    { case person(Person), project(Project) }
            case removing(_Remove); enum _Remove { case person(Person), project(Project) }
            case updating(_Update); enum _Update { case person(Person), project(Project) }
        }
    }
    init() {
        self.init(persons: [], projects: [])
    }
    private
    init(persons:[Person], projects:[Project]) {
        self.persons = persons
        self.projects = projects
    }
    func alter(_ c:Change...) -> Self { c.reduce(self){ $0.alter($1) } }
    func alter(_ c:Change) -> Self {
        switch c {
        case let .by(.adding  (.person (p))): return .init(persons: persons + [p],                   projects: projects                         )
        case let .by(.adding  (.project(p))): return .init(persons: persons,                         projects: projects + [p]                   )
        case let .by(.removing(.person (p))): return .init(persons: persons.filter{ $0.id != p.id }, projects: projects                         )
        case let .by(.removing(.project(p))): return .init(persons: persons,                         projects: projects.filter{ $0.id != p.id } )
        case let .by(.updating(.person (p))): return alter(.by(.removing(.person (p))), .by(.adding(.person (p))))
        case let .by(.updating(.project(p))): return alter(.by(.removing(.project(p))), .by(.adding(.project(p))))
        }
    }
}


typealias Access   = (                    ) -> State
typealias Change   = ( State.Change...    ) -> ()
typealias Reset    = (                    ) -> ()
typealias Callback = ( @escaping () -> () ) -> ()
typealias Destroy  = (                    ) -> ()

typealias Store = ( state: Access, change: Change, reset: Reset, updated: Callback, destroy: Destroy )


func state  (in store: Store                        ) -> State { store.state()                    }
func change (_  store: Store, _ cs: State.Change... )          { cs.forEach { store.change($0)  } }
func reset  (_  store: Store                        )          { store.reset();                   }
func destroy(_  store: inout Store!                 )          { store.destroy(); store = nil     }


func change(_ state: State, _ change: [ State.Change ] ) -> State { change.reduce(state) {$0.alter($1) } }

import Foundation.NSFileManager

func createStore(
    pathInDocs   p: String      = "state.json",
    fileManager fm: FileManager = .default
) -> Store
{
    var state = State()
    var callbacks: [() -> ()] = []
    return (
        state   : { state },
        change  : { state     = change(state, $0) },
        reset   : { state     = State()           },
        updated : { callbacks = callbacks + [$0]  },
        destroy : {  }
    )
}

//MARK: -

let store = createStore()

let t0 = Task(title: "A first task")
let t1 = Task(title: "A second task")
let p0 = Person(name: "Manuel")

store.change(
    .by(.adding(
        .project(Project(title: "A Project")
                    .altered(
                        .by(.adding(.task(t0))),
                        .by(.adding(.task(t1)))))))
)
print(store.state().projects)
print("----")
let project = store.state().projects.first!

change(store,
       .by(
        .updating(
            .project(project
                        .altered(
                            .by(.removing(.task(t1))),
                            .by(.updating(.task(t0.altered(
                                .by(
                                    .adding(.timeslip(TimeSlip(hours: 2, person:p0))))))))))))
)
print(store.state().projects.first!.tasks)
print("----")
print(store.state().projects.first!.tasks)


//: [Next](@next)
