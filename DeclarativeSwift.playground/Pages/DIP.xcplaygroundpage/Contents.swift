//: [Previous](@previous)
import Foundation

//MARK: - Data Structures
enum Empire {
    case britain, france, germany, russia, ottoman, austria, italy
}
enum Unit:Equatable {
    case army (Empire)
    case fleet(Empire)
}
struct Territory {
    init(name:String, kind:Kind, occupiedBy:Unit? = nil) {
        self.name = name
        self.kind = kind
        self.occupiedBy = occupiedBy
    }
    enum Kind { case land, sea }
    enum CoastKind {
        case unified
        case fragmented([(String, [Territory])])
    }
    enum Border {
        case land   (Territory, Territory)
        case coastal(Territory, Territory, Territory.CoastKind)
        case sea    (Territory, Territory)
        case channel(Territory)
    }
    let name:String
    let kind: Kind
    let occupiedBy:Unit?
    enum Change { case occupy(Unit?) }
    func execute(_ change: Change) -> Self {
        switch change {
        case .occupy(let unit): return .init(name: name, kind: kind, occupiedBy: unit)
        }
    }
}
final class Board {
    var territories: [Territory]
    let borders    : [Territory.Border]
    enum Change {
        case move(Move)
        case hold(Territory, with:Unit, of:Empire)
        enum Move{
            case  army(of:Empire, from:Territory, to:Territory)
            case fleet(of:Empire, from:Territory, to:Territory)
        }
    }
    init(territories t:[Territory], borders b:[Territory.Border]) { territories = t; borders = b }
    func execute(command:Change)  {
        switch command {
        case let .move( .army(empire, from:from, to:to)): move(unit: .army(empire),from:from,to:to)
        case let .move(.fleet(empire, from:from, to:to)): move(unit:.fleet(empire),from:from,to:to)
        case let .hold(territory, with:unit,  of:empire): print("hold \(territory.name) with:\(unit) of \(empire)")
        }
    }
    func connected(left:Territory, right:Territory) -> Bool {
        guard
            let from = territories.first(where: {$0.name ==  left.name}),
            let to   = territories.first(where: {$0.name == right.name}) else { return false }
        return borders.first { border in
            switch border {
            case let .land(l, r): return ((l.name == from.name && r.name == to.name) || (r.name == from.name && l.name == to.name))
            case let .sea (l, r): return ((l.name == from.name && r.name == to.name) || (r.name == from.name && l.name == to.name))
            default: return false
            }
        } != nil
    }
    private func move(unit:Unit, from:Territory, to:Territory)  {
        switch (unit, from.kind, to.kind) {
        case let ( .army(empire),.land,.land): connected(left:from,right:to) ? moveArmy (for:empire,fromLand:from,toLand:to) : ()
        case let (.fleet(empire), _ , .sea): connected(left:from,right:to) ? moveFleet(for:empire,fromLand:from,toLand:to) : ()
        case let (.fleet(empire), .sea, _): connected(left:from,right:to) ? moveFleet(for:empire,fromLand:from,toLand:to) : ()
        default: ()
        }
    }
    private func moveArmy(for empire:Empire, fromLand:Territory, toLand:Territory) {
        guard
            let from = territories.firstIndex(where:{$0.name == fromLand.name && $0.occupiedBy == .army(empire)}),
            let to   = territories.firstIndex(where:{$0.name == toLand.name })
        else { return }
        let  territories = Array(territories.prefix(from)) + [fromLand.execute(.occupy(nil))]           + Array(territories.suffix(territories.count - 1 - from))
        update(territories:Array(territories.prefix(to  )) + [  toLand.execute(.occupy(.army(empire)))] + Array(territories.suffix(territories.count - 1 - to )))
    }
    private func moveFleet(for empire:Empire, fromLand:Territory, toLand:Territory) {
        guard
            let from = territories.firstIndex(where:{$0.name == fromLand.name && $0.occupiedBy == .fleet(empire)}),
            let to   = territories.firstIndex(where:{$0.name == toLand.name })
        else { return }
        let  territories = Array(territories.prefix(from)) + [fromLand.execute(.occupy(nil))]            + Array(territories.suffix(territories.count - 1 - from))
        update(territories:Array(territories.prefix(to  )) + [  toLand.execute(.occupy(.fleet(empire)))] + Array(territories.suffix(territories.count - 1 - to)))
    }
    private func update(territories t:[Territory]) { territories = t }
}
//MARK: - Dev Print
func print(board b:Board, fullmode:Bool = false, ommitUnoccupied:Bool = true) {
    zip(b.territories,b.territories.map(\.occupiedBy)).forEach {
        switch $0.1 {
        case .none : ommitUnoccupied ? () : print("\($0.0.name)")
        case .army (let e): print("\($0.0.name)\t (army: \(e))")
        case .fleet(let e): print("\($0.0.name)\t (fleet: \(e))")
        }
    }
    if(fullmode) {
        borders.forEach {
            switch $0 {
            case let .land   (l, r   ): print("land   : \(l.name) <-> \(r.name)")
            case let .sea    (l, r   ): print("sea    : \(l.name) <-> \(r.name)")
            case let .coastal(l, r, k): {
                switch k {
                case     .unified              : print("coast  : \(l.name) <-> \(r.name)")
                case let .fragmented(fragments): print("coast  : \(l.name) <-> \(r.name)" + "\(fragments.map({ $0.0 + ": " + ($0.1.reduce("") { $0 + "," + $1.name }.trimmingCharacters(in: CharacterSet(charactersIn: ",")))}))")} }()
                case let .channel(t)           : print("channel: \(t.name)")
            }
        }
    }
    print("------")
}
//MARK: - Empires
//MARK: Great Britain
let clyde     = Territory(name:"Clyde"    , kind:.land)
let edinburgh = Territory(name:"Edinburgh", kind:.land)
let liverpool = Territory(name:"Liverpool", kind:.land)
let yorkshire = Territory(name:"Yorkshire", kind:.land)
let wales     = Territory(name:"Wales"    , kind:.land)
let london    = Territory(name:"London"   , kind:.land)
let gb        = [ clyde, edinburgh, liverpool, yorkshire, wales, london ]
//MARK: France
let brest      = Territory(name:"Brest"     , kind:.land)
let picardy    = Territory(name:"Picardy"   , kind:.land)
let paris      = Territory(name:"Paris"     , kind:.land)
let gascony    = Territory(name:"Gascony"   , kind:.land)
let burgundy   = Territory(name:"Burgundy"  , kind:.land)
let marseilles = Territory(name:"Marseilles", kind:.land)
let france     = [ brest, picardy, paris, gascony, burgundy, marseilles]
// MARK: Germany
let ruhr    = Territory(name:"Ruhr"   , kind:.land, occupiedBy:.army(.germany))
let kiel    = Territory(name:"Kiel"   , kind:.land                            )
let berlin  = Territory(name:"Berlin" , kind:.land, occupiedBy:.army(.germany))
let prussia = Territory(name:"Prussia", kind:.land                            )
let munich  = Territory(name:"Munich" , kind:.land                            )
let silesia = Territory(name:"Silesia", kind:.land                            )
let germany = [ ruhr, kiel, berlin, prussia, munich, silesia ]
// MARK: Russia
let finland      = Territory(name:"Finland"       , kind:.land)
let stpetersburg = Territory(name:"St. Petersburg", kind:.land)
let livonia      = Territory(name:"Livonia"       , kind:.land)
let warsaw       = Territory(name:"Warsaw"        , kind:.land)
let moscow       = Territory(name:"Moscow"        , kind:.land)
let ukraine      = Territory(name:"Ukraine"       , kind:.land)
let sevestapol   = Territory(name:"Sevestapol"    , kind:.land)
let russia       = [ finland, stpetersburg, livonia, warsaw, moscow, ukraine, sevestapol ]
// MARK: Italy
let piedmont = Territory(name:"Piemonte", kind:.land)
let venice   = Territory(name:"Venice"  , kind:.land)
let tuscany  = Territory(name:"Tuscany" , kind:.land)
let rome     = Territory(name:"Rome"    , kind:.land)
let apulia   = Territory(name:"Apulia"  , kind:.land)
let naples   = Territory(name:"Naples"  , kind:.land)
let italy    = [ piedmont, venice, tuscany, rome, apulia, naples ]
// MARK: Austria
let tyrolia  = Territory(name:"Tyrolia" , kind:.land)
let bohemia  = Territory(name:"Bohemia" , kind:.land)
let galicia  = Territory(name:"Galicia" , kind:.land)
let vienna   = Territory(name:"Vienna"  , kind:.land)
let trieste  = Territory(name:"Trieste" , kind:.land)
let budapest = Territory(name:"Budapest", kind:.land)
let austria  = [ tyrolia , bohemia , galicia , vienna  , trieste , budapest ]
// MARK: Ottoman
let constantinople = Territory(name:"Constantinople", kind:.land)
let ankara         = Territory(name:"Ankara"        , kind:.land)
let smyrna         = Territory(name:"Smyrna"        , kind:.land)
let armenia        = Territory(name:"Armenia"       , kind:.land)
let syria          = Territory(name:"Syria"         , kind:.land)
let ottoman        = [ constantinople, ankara, smyrna, armenia, syria ]
//MARK: - Free Territories
//MARK: Scandinavia
let denmark         = Territory(name:"Denmark", kind:.land)
let norway          = Territory(name:"Norway" , kind:.land)
let sweden          = Territory(name:"Sweden" , kind:.land)
let freeScandinavia = [ denmark, norway, sweden]
//MARK: BeNeLux
let belgium     = Territory(name:"Belgium", kind:.land)
let holland     = Territory(name:"Holland", kind:.land)
let freeBenelux = [ belgium, holland ]
//MARK: Hispania
let portugal     = Territory(name:"Portugal", kind:.land)
let spain        = Territory(name:"Spain"   , kind:.land)
let freeHispania = [ portugal, spain ]
//MARK: Africa
let northafrica = Territory(name:"North Africa", kind:.land)
let tunis       = Territory(name:"Tunis"       , kind:.land)
let freeAfrica  = [ northafrica, tunis ]
// MARK: - Balkans
let albania     = Territory(name:"Albania" , kind:.land)
let serbia      = Territory(name:"Serbia"  , kind:.land)
let rumania     = Territory(name:"Rumania" , kind:.land)
let greece      = Territory(name:"Greece"  , kind:.land)
let bulgaria    = Territory(name:"Bulgaria", kind:.land)
let freeBalkans = [ albania, serbia, rumania, greece, bulgaria ]
//MARK: - Oceans & Seas
//MARK: West Ocean
let northatlantic  = Territory(name:"North Atlantic" , kind:.sea                             )
let midatlantic    = Territory(name:"Mid-Atlantic"   , kind:.sea, occupiedBy:.fleet(.britain))
let irishsea       = Territory(name:"Irish Sea"      , kind:.sea                             )
let englishchannel = Territory(name:"English Channel", kind:.sea                             )
let westOcean      = [ northatlantic, midatlantic, irishsea, englishchannel ]
//MARK: North Ocean
let norwegiansea   = Territory(name:"Norwegian Sea"  , kind:.sea)
let barentssea     = Territory(name:"Barents Sea"    , kind:.sea)
let northsea       = Territory(name:"North Sea"      , kind:.sea)
let skagerrak      = Territory(name:"Skagerrak"      , kind:.sea)
let helgolandbight = Territory(name:"Helgoland Bight", kind:.sea)
let northOcean     = [ norwegiansea, barentssea, northsea, skagerrak, helgolandbight ]
//MARK: Baltic Sea
let balticsea      = Territory(name:"Baltic Sea"     , kind:.sea)
let gulfofbothnia  = Territory(name:"Gulf of Bothnia", kind:.sea)
let baltic         = [ balticsea, gulfofbothnia ]
//MARK: Mediterrean Sea
let westernmediterrean = Territory(name:"Western Mediterrean Sea", kind:.sea)
let gulfoflyon         = Territory(name:"Gulf of Lyon"           , kind:.sea)
let tyrrhenian         = Territory(name:"Tyrrhenian Sea"         , kind:.sea)
let ioniansea          = Territory(name:"Ionian Sea"             , kind:.sea)
let adriaticsea        = Territory(name:"Adriatic Sea"           , kind:.sea)
let aegeansea          = Territory(name:"Aegean Sea"             , kind:.sea)
let easternmediterrean = Territory(name:"Eastern Mediterrean Sea", kind:.sea)
let mediterrean        = [ westernmediterrean, gulfoflyon, tyrrhenian, ioniansea, adriaticsea, aegeansea, easternmediterrean ]
//MARK: Black Sea
let blackSea = Territory(name:"Black Sea", kind:.sea)
//MARK: All Seas
let seas     = {
      westOcean
    + northOcean
    + baltic
    + mediterrean
    + [blackSea]
}()

//MARK: - Coast lines
let gbCoastLine: [Territory.Border] = [
    .coastal(clyde,     northatlantic,  .unified),
    .coastal(clyde,     norwegiansea,   .unified),
    .coastal(edinburgh, norwegiansea,   .unified),
    .coastal(edinburgh, northsea,       .unified),
    .coastal(liverpool, northatlantic,  .unified),
    .coastal(liverpool, irishsea,       .unified),
    .coastal(yorkshire, northsea,       .unified),
    .coastal(wales,     irishsea,       .unified),
    .coastal(wales,     englishchannel, .unified),
    .coastal(london,    northsea,       .unified),
    .coastal(london,    englishchannel, .unified),
]
let scandinaviaGermanNorthRussianCoastLine: [Territory.Border] = [
    .coastal(denmark,      northsea,       .unified                                ),
    .coastal(denmark,      skagerrak,      .unified                                ),
    .coastal(denmark,      helgolandbight, .unified                                ),
    .coastal(denmark,      balticsea,      .unified                                ),
    .coastal(norway,       norwegiansea,   .unified                                ),
    .coastal(norway,       barentssea,     .unified                                ),
    .coastal(norway,       northsea,       .unified                                ),
    .coastal(norway,       skagerrak,      .unified                                ),
    .coastal(sweden,       balticsea,      .unified                                ),
    .coastal(sweden,       gulfofbothnia,  .unified                                ),
    .coastal(finland,      gulfofbothnia,  .unified                                ),
    .coastal(stpetersburg, gulfofbothnia,  .fragmented([("South",[gulfofbothnia])])),
    .coastal(stpetersburg, barentssea,     .fragmented([("North",[barentssea   ])])),
    .coastal(livonia,      balticsea,      .unified                                ),
    .coastal(livonia,      gulfofbothnia,  .unified                                ),
    .coastal(prussia,      balticsea,      .unified                                ),
    .coastal(berlin,       balticsea,      .unified                                ),
    .coastal(kiel,         balticsea,      .unified                                ),
    .coastal(kiel,         helgolandbight, .unified                                ),
]
let beneluxCoastLine: [Territory.Border] = [
    .coastal(belgium, englishchannel, .unified),
    .coastal(belgium, northsea,       .unified),
    .coastal(holland, northsea,       .unified),
    .coastal(holland, helgolandbight, .unified),
]
let frenchCoastLine: [Territory.Border] = [
    .coastal(brest,   midatlantic,    .unified),
    .coastal(brest,   englishchannel, .unified),
    .coastal(picardy, englishchannel, .unified),
    .coastal(gascony, midatlantic,    .unified),
]
let hispanicCoastLine: [Territory.Border] = [
    .coastal(portugal, midatlantic, .unified),
    .coastal(spain,    midatlantic, .fragmented([("north", [midatlantic                                ]),
                                                 ("south", [midatlantic,westernmediterrean,gulfoflyon])])),
]
//MARK: - Territories
let territories = {
      gb
    + france
    + germany
    + russia
    + italy
    + austria
    + ottoman
    + freeScandinavia
    + freeHispania
    + freeBenelux
    + freeAfrica
    + freeBalkans
    + seas
}()
//MARK: - Connections & Borders
let connectionsGB            : [Territory.Border] = [
    .land(clyde,     edinburgh),
    .land(clyde,     liverpool),
    .land(edinburgh, liverpool),
    .land(edinburgh, yorkshire),
    .land(yorkshire, wales    ),
    .land(yorkshire, london   ),
]
let connectionsFrance        : [Territory.Border] = [
    .land(brest,    picardy   ),
    .land(brest,    paris     ),
    .land(brest,    gascony   ),
    .land(picardy,  paris     ),
    .land(picardy,  burgundy  ),
    .land(paris,    gascony   ),
    .land(paris,    burgundy  ),
    .land(gascony,  burgundy  ),
    .land(gascony,  marseilles),
    .land(burgundy, marseilles),
]
let connectionsGermany       : [Territory.Border] = [
    .land(ruhr,    kiel   ),
    .land(ruhr,    munich ),
    .land(kiel,    berlin ),
    .land(kiel,    munich ),
    .land(berlin,  prussia),
    .land(berlin,  munich ),
    .land(berlin,  silesia),
    .land(prussia, silesia),
    .land(munich,  silesia),
]
let connectionsRussia        : [Territory.Border] = [
    .land(finland,      stpetersburg),
    .land(stpetersburg, livonia     ),
    .land(stpetersburg, moscow      ),
    .land(livonia,      warsaw      ),
    .land(livonia,      moscow      ),
    .land(warsaw,       moscow      ),
    .land(moscow,       ukraine     ),
    .land(moscow,       sevestapol  ),
    .land(ukraine,      sevestapol  ),
]
let connectionsItaly         : [Territory.Border] = [
    .land(piedmont, venice ),
    .land(piedmont, tuscany),
    .land(venice,   tuscany),
    .land(venice,   rome   ),
    .land(venice,   apulia ),
    .land(tuscany,  rome   ),
    .land(rome,     apulia ),
    .land(rome,     naples ),
    .land(apulia,   naples ),
]
let connectionsAustria       : [Territory.Border] = [
    .land(tyrolia, bohemia ),
    .land(tyrolia, vienna  ),
    .land(tyrolia, trieste ),
    .land(bohemia, galicia ),
    .land(bohemia, vienna  ),
    .land(galicia, budapest),
    .land(vienna,  trieste ),
    .land(vienna,  budapest),
    .land(trieste, budapest),
]
let connectionsOttoman       : [Territory.Border] = [
    .land(constantinople, ankara ),
    .land(constantinople, smyrna ),
    .land(ankara,         smyrna ),
    .land(ankara,         armenia),
    .land(smyrna,         syria  ),
    .land(armenia,        syria  ),
]
let borderBeneluxGermany     : [Territory.Border] = [
    .land(ruhr, belgium),
    .land(ruhr, holland)
]
let bordersGermanyAustria    : [Territory.Border] = [
    .land(munich,  tyrolia),
    .land(munich,  bohemia),
    .land(silesia, bohemia),
    .land(silesia, galicia),
]
let borderAustriaBalkan      : [Territory.Border] = [
    .land(galicia,  rumania),
    .land(trieste,  albania),
    .land(trieste,  serbia ),
    .land(budapest, serbia ),
    .land(budapest, rumania),
]
let borderBalkanOttoman      : [Territory.Border] = [
    .land(bulgaria, constantinople)
]
let connectionsBenelux       : [Territory.Border] = [
    .land(belgium, holland)
]
let connectionsScandinavia   : [Territory.Border] = [
    .land(denmark, sweden),
    .land(norway,  sweden),
]
let connectionsHispania      : [Territory.Border] = [
    .land(portugal, spain)
]
let connectionsAfrica        : [Territory.Border] = [
    .land(northafrica, tunis)
]
let connectionsBalkan        : [Territory.Border] = [
    .land(albania, serbia  ),
    .land(albania, greece  ),
    .land(serbia,  rumania ),
    .land(serbia,  greece  ),
    .land(serbia,  bulgaria),
    .land(rumania, bulgaria),
    .land(greece,  bulgaria),
]
let connectionsWestOcean     : [Territory.Border] = [
    .sea(northatlantic, midatlantic   ),
    .sea(northatlantic, irishsea      ),
    .sea(midatlantic,   irishsea      ),
    .sea(midatlantic,   englishchannel),
]
let connectionsNorthOcean    : [Territory.Border] = [
    .sea(norwegiansea, barentssea    ),
    .sea(norwegiansea, northsea      ),
    .sea(northsea,     skagerrak     ),
    .sea(northsea,     helgolandbight),
]
let connectionsBaltic        : [Territory.Border] = [
    .sea(balticsea, gulfofbothnia)
]
let connectionsMediterrean   : [Territory.Border] = [
    .sea(westernmediterrean, gulfoflyon        ),
    .sea(westernmediterrean, tyrrhenian        ),
    .sea(gulfoflyon,         tyrrhenian        ),
    .sea(tyrrhenian,         ioniansea         ),
    .sea(ioniansea,          adriaticsea       ),
    .sea(ioniansea,          aegeansea         ),
    .sea(ioniansea,          easternmediterrean),
    .sea(aegeansea,          easternmediterrean),
    .sea(aegeansea,          constantinople   ),

]
let borderAtlanticMediterrean: [Territory.Border] = [
    .sea(midatlantic, westernmediterrean)
]
let coasts                   : [Territory.Border] = {
      gbCoastLine
    + scandinaviaGermanNorthRussianCoastLine
    + beneluxCoastLine
    + frenchCoastLine
    + hispanicCoastLine
}()
let channels                 : [Territory.Border] = [
    .channel(kiel          ),
    .channel(denmark       ),
    .channel(constantinople)
]
let borders                  : [Territory.Border] = {
      connectionsGB
    + connectionsFrance
    + connectionsGermany
    + connectionsRussia
    + connectionsItaly
    + connectionsAustria
    + connectionsOttoman
    + connectionsScandinavia
    + connectionsBenelux
    + connectionsHispania
    + connectionsAfrica
    + connectionsBalkan
    + borderBeneluxGermany
    + bordersGermanyAustria
    + borderAustriaBalkan
    + borderBalkanOttoman
    + connectionsWestOcean
    + connectionsNorthOcean
    + connectionsBaltic
    + connectionsMediterrean
    + borderAtlanticMediterrean
    + coasts
    + channels
}()
//MARK: - Test
let board = Board(territories:territories, borders:borders)
board.execute(command:.move(.army(of:.germany,from:berlin,  to:silesia       )) )
board.execute(command:.move(.army(of:.germany,from:silesia, to:galicia       )) )
board.execute(command:.move(.army(of:.germany,from:galicia, to:rumania       )) )
board.execute(command:.move(.army(of:.germany,from:rumania, to:bulgaria      )) )
board.execute(command:.move(.army(of:.germany,from:bulgaria,to:constantinople)) )

board.execute(command:.move(.fleet(of:.britain,from:midatlantic,       to:westernmediterrean)) )
board.execute(command:.move(.fleet(of:.britain,from:westernmediterrean,to:tyrrhenian        )) )
board.execute(command:.move(.fleet(of:.britain,from:tyrrhenian,        to:ioniansea         )) )
board.execute(command:.move(.fleet(of:.britain,from:ioniansea,         to:aegeansea         )) )
board.execute(command:.move(.fleet(of:.britain,from:aegeansea,         to:constantinople    )) )


print(board:board)
print("++++++++++++++++++++++")
//print(board:b, fullmode:true, ommitUnoccupied:false)
//: [Next](@next)
