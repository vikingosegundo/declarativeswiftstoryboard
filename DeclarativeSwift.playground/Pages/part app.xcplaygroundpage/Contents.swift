//: [Previous](@previous)

enum AdderError: Error {
    case mustBePositive
}

func createAdder(x:Int) -> (Int) -> Int  {
    var value = x
    return {
        value = value + $0
        return value
    }
}

let adder = createAdder(x:1)
print(adder(4))
print(adder(1))

func times(_ x:Int) -> (Int) -> Int {
    return { y in
        return x * y
    }
}
let times3 = times(3)
print("=======================")
print(times3(5))
print("=======================")
let times15 = times(3)(5)
print("=======================")
print(times15)
print("=======================")

func times() -> (Int) -> (Int) -> Int {
    return { x in
        return {y in
            return x * y
        }
    }
}
let timesXY = times()
print(timesXY(10)(3))

let times15Y = times()(15)
print(times15Y(3))

typealias Stack<T> = (push:(T) -> (), peek:() -> (T?), pop:() -> (T?))
func createStack<T>() -> Stack<T> {
    var array:[T] = []
    return (push: { array.append($0) },
            peek: { return array.count > 0 ? array[array.count - 1] : nil},
            pop : { return array.count > 0 ? array.remove(at: array.count - 1) : nil })
}
let s:Stack<Int> = createStack()
s.push(1)
s.push(2)
s.push(3)
print("pop  " + String(describing:s.pop()))
print("peek " + String(describing:s.peek()))
print("pop  " + String(describing:s.pop()))
print("peek " + String(describing:s.peek()))
print("pop  " + String(describing:s.pop()))
print("peek " + String(describing:s.peek()))
print("pop  " + String(describing:s.pop()))

protocol Message {}
struct CallMessage:Message {
    enum Kind {
        case call
        case hangUp
    }
    let msg:Kind
    let number:String?
}
struct ContactMessage:Message {
    enum Kind {
        case add
        case search
    }
    let msg:Kind
    let searchString:String?
    let search:Search?
    let contact:Contact?
}
struct Contact {}
enum Search {
    case `for`(For)
    enum For {
        case name
    }
}
func message() -> (
    phone:() -> (
        call:  (String) -> CallMessage,
        hangup:(      ) -> CallMessage),
    contacts:() -> (
        add:   (  Contact                  ) -> ContactMessage,
        search:(_ for: String , _ in:Search) -> ContactMessage))
{
    (
        phone: { (
        call: { .init(msg:.call,   number:$0 ) },
        hangup: { .init(msg:.hangUp, number:nil) }) },
    contacts: { (
            add: { _   in ContactMessage(msg:.add,    searchString:nil, search:nil, contact:Contact()) },
        search: { s,i in ContactMessage(msg:.search, searchString:s,   search:i,   contact:nil)       }) })
}
let msg0 = message()
    .phone()
    .call("1-23-45")
let msg1 = message()
    .phone()
    .hangup()
let msg2 = message().contacts().add(Contact())
let msgs:[Message] = [msg0, msg1,msg2]
func app() {
    msgs.forEach { m in
        switch m {
        case let m as    CallMessage: featurePhone  (msg: m)
        case let m as ContactMessage: featureContacts(msg: m)
        default:
            ()
        }
    }
}
func featurePhone(msg:CallMessage) {
    switch (msg.msg, msg.number) {
    case let (.call,  .some(nr)): print("call \(nr)")
    case     (.call,  .none    ): print("cant call without number")
    case     (.hangUp, _       ): print("hangup current call")
    }
}
func featureContacts(msg:ContactMessage) {
    switch (msg.msg, msg.search, msg.searchString, msg.contact) {
    case let (.add   , _         , _           ,.some(c)): print("add \(c)")
    case let (.search,.some(kind),.some(string), _      ): print("search for \(kind): \(string)")
    default: ()
    }
}
app()
//: [Next](@next)
