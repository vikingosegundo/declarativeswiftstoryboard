//: [Previous](@previous)

import Foundation

struct Order {
    enum State {
        case foodSelection
        case inPreparation
        case inDelivery(by:Driver)
        case delivered
    }
    let restaurant:Restaurant
    let customer:Customer
    let food :[Food]
    let state:State
    
    init(from f:Restaurant, to t: Customer) {
        self.init(f, t, [], .foodSelection)
    }
    private init(_ from:Restaurant,_ to: Customer,_ food:[Food],_ state: State ) {
        self.restaurant = from
        self.customer = to
        self.food = food
        self.state = state
    }
}
extension Order {
    enum Change {
        case adding  (Adding); enum Adding  { case food(Food)      }
        case setting(Setting); enum Setting { case state(to:State) }
    }
    func alter (by cs:[Change] ) -> Self { cs.reduce(self) { $0.alter($1) } }
    func alter (by cs:Change...) -> Self { cs.reduce(self) { $0.alter($1) } }
    private
    func alter(_ c:Change) -> Self {
        switch c {
        case .adding (.food (let f)):
            return .init(restaurant,customer,food + [f],state)
        case .setting(.state(let s)):
            return .init(restaurant,customer,food,transition(to:s))
        }
    }
    private
    func transition(to newState:Order.State) -> Order.State {
        if addressIsEmpty() { return state }
        switch (state,newState) {
        case (.foodSelection,.inPreparation): return newState
        case (.inPreparation,.inDelivery   ): return newState
        case (.inDelivery   ,.delivered    ): return newState
        default: return state
        }
    }
    private
    func addressIsEmpty() -> Bool {
        return customer.address.street == ""
        || customer.address.city == ""
    }
}
extension Order {
    var driver:Driver? {
        switch state {
        case let .inDelivery(by: d): return d
        default: return nil
        }
    }
}
struct Customer {
    init(name n:String) {
        self.init(n, .init(street:"", city:""))
    }
    private
    init(_ n:String,_ a:Address) {
        name = n
        address = a
    }
    let name: String
    let address:Address
}
extension Customer {
    enum Change {
        case setting(Setting); enum Setting {
            case name(String)
            case address(Address)
        }
    }
    func alter(_ c:Change) -> Self {
        switch c {
        case .setting(.name   (let n)): return .init(n,   address)
        case .setting(.address(let a)): return .init(name,a      )
        }
    }
}
struct Restaurant {
    init(name n:String) {
        self.init(n,Address(street: "", city: ""))
    }
    private
    init(_ name:String,_ address:Address) {
        self.name = name
        self.address = address
    }
    let name: String
    let address:Address
}
extension Restaurant {
    enum Change {
        case setting(Setting); enum Setting {
            case name(String)
            case address(Address)
        }
    }
    func alter(by c:Change) -> Self {
        switch c {
        case .setting(.name   (let name   )): return .init(name,address)
        case .setting(.address(let address)): return .init(name,address)
        }
    }
}
struct Address {
    let street: String
    let city: String
}
extension Address {
    enum Change {
        case setting(Setting); enum Setting {
            case city(String)
            case street(String)
        }
    }
    func alter(_ c:Change) -> Self {
        switch c {
        case .setting(.city  (let c)): return .init(street:street,city:c   )
        case .setting(.street(let s)): return .init(street:s,     city:city)
        }
    }
}
struct Driver {
    let name: String
}
extension Driver {
    enum Change {
        case setting(Setting); enum Setting {
            case name(String)
        }
    }
    func alter(_ c:Change) -> Self {
        switch c {
        case .setting(.name(let name)): return .init(name: name)
        }
    }
}
struct Food {
    let name: String
}
extension Food {
    enum Change {
        case setting(Setting); enum Setting {
            case name(String)
        }
    }
    func alter(_ c:Change) -> Self {
        switch c {
        case .setting(.name(let name)): return .init(name: name)
        }
    }
}

var customer = Customer(name:"Alice")
customer = customer.alter(.setting(.address(Address(street: "a nother street", city: "A City"))))
var restaurant = Restaurant(name:"manu's kitchen")
        .alter(
            by:
                .setting(
                    .address(Address(street:"Main Street",
                                       city:"A City"))))
let driver = Driver(name:"Bob")
var order = Order(from: restaurant, to: customer)
order = order.alter(
        by:
            .adding(.food(Food(name:"Pizza Hawaii")))
    )
order = order.alter(by: .setting(.state(to: .inPreparation)))
order = order.alter(by: .setting(.state(to:.inDelivery(by: driver))))
print(order)

print(order.driver!)
order = order.alter(by: .setting(.state(to: .delivered)))
print(order)

//: [Next](@next)
