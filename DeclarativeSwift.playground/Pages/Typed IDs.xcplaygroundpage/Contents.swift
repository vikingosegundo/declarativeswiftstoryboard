//: [Previous](@previous)

import Foundation

protocol TypeSafeIdentifiable: Identifiable {
    associatedtype IDType: Hashable
    associatedtype ID = Identifier<IDType, Self>
}
extension TypeSafeIdentifiable {
    fileprivate static var hasherInput: String { .init(describing: self) }
}
struct Identifier<Value: Hashable, _Owner: TypeSafeIdentifiable>: Hashable {
    let value: Value
    func hash(into hasher: inout Hasher) {
        hasher.combine(_Owner.hasherInput)
        hasher.combine(value)
    }
}
extension Identifier: ExpressibleByIntegerLiteral where Value == Int {
    init(integerLiteral value: Value) {
        self.init(value: value)
    }
}
private typealias ExpressibleByStringLiteral = Swift.ExpressibleByStringLiteral
    & ExpressibleByExtendedGraphemeClusterLiteral
    & ExpressibleByUnicodeScalarLiteral

extension Identifier: ExpressibleByStringLiteral where Value == String {
    typealias ExtendedGraphemeClusterLiteralType = Value
    typealias UnicodeScalarLiteralType = Value

    init(stringLiteral value: Value) {
        self.init(value: value)
    }
}
struct User: TypeSafeIdentifiable {
    typealias IDType = UUID
    let name: String
    let id: ID
}

struct Post: TypeSafeIdentifiable {
    typealias IDType = Int
    let title: String
    let id: ID
    let userID: User.ID
}

struct Comment: TypeSafeIdentifiable {
    typealias IDType = Int

    let id: ID
    let postID: Post.ID
    let userID: User.ID
}

let u = User(name:"user",id:UUID())
let p = Post(id:0, userID: u.id)
let c = Comment(id:0, posdID: p.id, userID:u.id)
print(u)
print(p)
print(c)
//: [Next](@next)
