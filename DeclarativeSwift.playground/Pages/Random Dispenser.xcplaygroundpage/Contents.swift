//: [Previous](@previous)

import Foundation

extension String {
    static func *(lhs: String, rhs: Int) -> String { (0..<rhs).reduce("") { p,_ in  p + lhs } }
    static func *(lhs: Int, rhs: String) -> String { rhs * lhs }
}
typealias _Stack<T> = (
    push  : (T) -> (),
    pop   : ( ) -> T?,
    peek  : ( ) -> T?,
    string: ( ) -> String
)
func createStack<T>(with array:[T] = [], divider: (top: String, bottom: String) = (top:"-", bottom:"=")) -> _Stack<T> {
    var array:[T] = array
    var typeDescription: String { String(describing:T.self) }
    var topDivider     : String {
        array.count > 0
        ? "\(3*divider.top) " +       "Stack<\(typeDescription)>: \(array.count) elements " + "\(3*divider.top)" + "\n"
        : "\(3*divider.top) " + "Empty Stack<\(typeDescription)> "                          + "\(3*divider.top)" + "\n"
    }
    var bottomDivider  : String { divider.bottom * max((topDivider.count - 1), 0) + "\n" }
    return (
          push: { array.append($0)                                                                               },
           pop: { array.count > 0 ? array.remove(at:array.count - 1) : nil                                       },
          peek: { array.last                                                                                     },
        string: { topDivider + array.reversed().reduce("") { $0 + String(reflecting:$1) + "\n" } + bottomDivider }
    )
}
func createRandomDispenser<T>(with data: [T]) -> () -> T? {
    var state = data.shuffled()
    return {
        state.popLast()
    }
}
func createRandomStackDispenser<T>(with data: [T]) -> () -> T? {
    let state = createStack(with:data.shuffled())
    return {
        state.pop()
    }
}
func createStackDispenser<T>(with data: [T]) -> () -> T? {
    let state = createStack(with:data)
    return {
        state.pop()
    }
}

struct Stack<T:Codable> {
    init(             )       { self.init(createStack()) }
    init(_ s:_Stack<T>)       { stack = s                }
    func push(_ el: T )       { stack.push(el)           }
    func pop (        ) -> T? { stack.pop()              }
    func peek(        ) -> T? { stack.peek()             }
    private let stack: _Stack<T>
}
extension Stack: CustomStringConvertible {
    var description: String { stack.string() }
}
extension Stack {
    struct Transcoder: Codable {
        enum CodingKeys: String, CodingKey { case elements }
        let stack: Stack<T>
        init(stack s:Stack<T>) { stack = s }
        init(from decoder: Decoder) throws {
            stack = Stack(createStack(with:try decoder.container(keyedBy:CodingKeys.self).decode(Array<T>.self,forKey:.elements)))
        }
        func encode(to encoder: Encoder) throws {
            var container = encoder.container(keyedBy: CodingKeys.self)
            var elements: [T] = []
            while let x = stack.pop() { elements.append(x) }
            try container.encode(elements, forKey: .elements)
        }
    }
}

let s:_Stack<String> = createStack()
s.push("one")
s.push("two")
s.push("three")

let o = Stack(s)
o.push("four")

let t = Stack.Transcoder(stack: o)
let jsonData = try! JSONEncoder().encode(t)

let o2 = (try! JSONDecoder().decode(Stack<String>.Transcoder.self, from: jsonData)).stack
print(try! JSONDecoder().decode(Stack<String>.Transcoder.self, from: jsonData)
)
//: [Next](@next)
