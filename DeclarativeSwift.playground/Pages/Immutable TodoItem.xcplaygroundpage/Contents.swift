    //: [Previous](@previous)
import Foundation.NSUUID

extension Date {
    static var     today: Date { Date().midnight      }
    static var yesterday: Date { Date.today.dayBefore }
    static var  tomorrow: Date { Date.today.dayAfter  }
    
    var dayBefore: Date { cal.date(byAdding: .day, value: -1, to: self)! }
    var  dayAfter: Date { cal.date(byAdding: .day, value:  1, to: self)! }
    var  midnight: Date { cal.date(bySettingHour:  0, minute: 0, second: 0, of: self)!}
    var      noon: Date { cal.date(bySettingHour: 12, minute: 0, second: 0, of: self)!}

    private var cal: Calendar { Calendar.current }
}


struct TodoItem: Identifiable, Hashable, Codable {
    enum Change {
        case text(String)
        case completed(Bool)
        case due(Date?)
//        case fork([Change])
    }
    
    let text        : String
    let completed   : Bool
    let id          : UUID
    let dueDate     : Date?
    let alterDate   : Date?
    let creationDate: Date

    init(text: String) { self.init(text, false, UUID(), nil, Date(), nil) }
    
    init(text: String, _ chages:Change...) {
        var temp = TodoItem(text:text).alter(chages)
        self.id           = temp.id
        self.text         = temp.text
        self.completed    = temp.completed
        self.dueDate      = temp.dueDate
        self.alterDate    = temp.alterDate
        self.creationDate = temp.creationDate
    }

    func alter(_ changes: Change...) -> TodoItem { alter(changes) }
    func alter(_ changes: [Change] ) -> TodoItem { changes.reduce(self) { $0.alter($1) } }

    private init (_ text: String, _ completed: Bool, _ id: UUID, _ dueDate:Date?, _ creationDate: Date, _ alterDate:Date?) {
        self.text = text
        self.completed = completed
        self.id = id
        self.creationDate = creationDate
        self.dueDate = dueDate
        self.alterDate = alterDate
    }
    
    private func alter(_ change:Change) -> TodoItem {
        return item(with: change)
    }

    private func item(with change: Change) -> TodoItem {
        let altered = Date()
        switch change {
        case .text     (let text     ): return TodoItem( text, completed, id    , dueDate, creationDate, altered  )
        case .completed(let completed): return TodoItem( text, completed, id    , dueDate, creationDate, altered  )
        case .due      (let dueDate  ): return TodoItem( text, completed, id    , dueDate, creationDate, altered  )
//        case .fork     (let changes  ): return TodoItem( text, completed, UUID(), dueDate, creationDate, alterDate).alter(changes)
        }
    }
}

func bruttoEqual(lhs: TodoItem, rhs: TodoItem) -> Bool {
    lhs.text      == rhs.text
 && lhs.dueDate   == rhs.dueDate
 && lhs.completed == rhs.completed
}


struct Item {
    private var internalItem: TodoItem = .init(text: "")
    var id:UUID { get {internalItem.id}}
    var text: String {
        set { internalItem = internalItem.alter(.text(newValue))}
        get { internalItem.text }
    }
    var dueDate: Date? {
        set { internalItem = internalItem.alter(.due(newValue))}
        get { internalItem.dueDate }
    }
    var completed: Bool {
        set { internalItem = internalItem.alter(.completed(newValue))}
        get { internalItem.completed }
    }
    var alterDate: Date? {
        get { internalItem.alterDate }
    }
    var creationDate: Date {
        get { internalItem.creationDate }
    }
}
let t0 = TodoItem(text: "buy beer", .due(.tomorrow))
//let t1 = t0.alter(.due(.tomorrow))
//print(t0.dueDate == nil)
print(t0)

var t1 = Item()
t1.text = "Coffee!!"
t1.dueDate = .tomorrow
t1.completed = true

print(t1)
print(t1.completed)

//: [Next](@next)
