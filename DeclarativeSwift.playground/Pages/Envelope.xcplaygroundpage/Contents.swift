import UIKit
// no nesting and annotating of enums
enum MSG {
    case startApp
    case appStarted
    
    case startCall
    case callStarted
    case callFailedStarting
    
    case stopCall
    case callStopped
    case callFailedStopping
}
typealias Envelope = ( msg: MSG, attachment:Any? )
func envelope(_ msg: MSG, _ attachment: Any? = nil) -> Envelope { (msg:msg, attachment:attachment) }
var app: ((Envelope) -> ())!
func createApp() -> (Envelope) -> () {
    let features = [
        createStartup(out: { app($0) } ),
        createCalling(out: { app($0) } )]
    return { msg in
        print(String(describing: msg.msg) + "\t\t\t" + (msg.attachment != nil ? String(describing: msg.attachment!) : ""))
        features.forEach { (f) in f(msg) }
    }
}
func createCalling(out: @escaping (Envelope) -> ()) -> (Envelope) -> () { {
        switch ($0.msg, $0.attachment) {
        case (.startCall, let handle as String): out( .callStarted |> handle             )
        case (.startCall, _                   ): out(              |>.callFailedStarting )
        case (.stopCall,  let handle as String): out( .callStopped |> handle             )
        case (.stopCall,  _                   ): out(              |>.callFailedStopping )
        default: break
        }
    }
}
func createStartup(out: @escaping (Envelope) -> ()) -> (Envelope) -> () { {
        switch $0.msg {
        case .startApp: out(envelope(.appStarted))
        default: break
        }
    }
}
prefix operator |>
infix  operator |>
extension MSG { static prefix func |> (message: Self                ) -> Envelope { envelope(message)             } }
extension MSG { static        func |> (message: Self, attachment:Any) -> Envelope { envelope(message, attachment) } }

app = createApp()
app(           |>.startApp  )
app(.startCall |> "11-22-33")
app(.stopCall  |> "11-22-33")
app(           |>.startCall )
app(           |>.stopCall  )
