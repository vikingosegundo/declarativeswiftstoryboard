//: [Previous](@previous)

fileprivate typealias _Error = Error

struct Normalized: Equatable {
    enum Error:_Error { case outOfBounds(String), limitsMismatch(String) }
    init(_ val:Double, lowerLimit ll: Double = 0.0, upperLimit ul: Double = 1.0) throws {
        guard  ll < ul                else { throw Error.limitsMismatch("Normalized ERROR: limits mismatch    :\t(\(ll) \(ul))") }
        guard (ll...ul).contains(val) else { throw Error.outOfBounds   ("Normalized ERROR: value out of bounds:\t\(val) (\(ll) \(ul))")    }
        value = val;
    }
    private let value: Double
}

let x = try? Normalized(1.5)
let y = try? Normalized(1.0)
let xx = try? Normalized(1.0, lowerLimit: 1.0, upperLimit: 1.1)
let xy = try? Normalized(1.5, lowerLimit: 1.0, upperLimit: 2.0)

print("x?: " + (x != nil ? String(describing: x!) : "nope"))
print("y?: " + (y != nil ? String(describing: y!) : "nope"))
print("x == y: \(x == y)")
print("xx:", xx ?? "")
print("xy:", xy ?? "")
//: [Next](@next)
