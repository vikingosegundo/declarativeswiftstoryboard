//: [Previous](@previous)

import Foundation


enum User {
    case one
    case two
}

struct Address: Identifiable, Equatable  {
    
    enum Change {
        case set(Set)
        case add(Add)
        case remove(Remove)
        case reassign(Reassign)
        
        enum Set {
            case street (to:String)
            case city   (to:String)
            case zip    (to:String)
            case country(to:String)
        }
        enum Add      { case tag(name: String) }
        enum Remove   { case tag(named:String) }
        enum Reassign { case owner(to:User)    }
    }
    
    init(owner:User,tag:String? = nil) { self.init(UUID(), owner, nil, nil, nil, nil, tag != nil ? [tag!] : []) }
    
    private init(_ id:UUID, _ owner:User,_ street: String?, _ city: String?,_ country:String?, _ zip: String?, _ tags: [String]) {
        self.id = id
        self.owner = owner
        self.street = street
        self.city = city
        self.country = country
        self.zip = zip
        self.tags = tags
    }
    let id    : UUID
    let owner :User
    let street: String?
    let city  : String?
    let country:String?
    let zip   : String?
    let tags  : [String]

    func alter(_ changes:Change...) -> Self { changes.reduce(self) { $0.alter($1) } }

    private func alter(_ change:Change) -> Self {
        switch change {
        case let .reassign(.owner(owner  )):return .init(id,owner,street,city,country,zip,tags                  )
        case let .set(    .street(street )):return .init(id,owner,street,city,country,zip,tags                  )
        case let .set(      .city(city   )):return .init(id,owner,street,city,country,zip,tags                  )
        case let .set(   .country(country)):return .init(id,owner,street,city,country,zip,tags                  )
        case let .set(       .zip(zip    )):return .init(id,owner,street,city,country,zip,tags                  )
        case let .add(       .tag(t      )):return .init(id,owner,street,city,country,zip,tags + [t]            )
        case let .remove(    .tag(t      )):return .init(id,owner,street,city,country,zip,tags.filter({$0 != t}))
        }
    }
    var description: String { "\(tags.reduce("\n") { $0 + "[\($1)]" }) \(street != nil ? street! : "") \(zip != nil ? zip! : "") \(city != nil ? city! : "") \(country != nil ? country! : "") <\(self.owner)>" }
}

struct Contact:Identifiable, CustomStringConvertible {
    enum Change {
        case set(Set)
        case add(Add)
        case update(Update)
        case remove(Remove)
        case reassign(Reassign)
        enum Reassign {
            case address(Address, to:User)
        }
        
        enum Set {
            case family(name:String)
            case personal(name:String)
        }
        enum Add    { case address(Address) }
        enum Update {
            case address   (Address)
            case addresses([Address])
        }
        enum Remove {
            case addresses(tagged:String)
            case tag(named:String)
        }
    }
    let id: UUID
    let familyName:  String?
    let personalName:String?
    let addresses: [Address]
    
    init() { self.init(UUID(),nil, nil, []) }
    
    private init(_ id: UUID, _ familyName:String?, _ personalName:String?, _ adresses: [Address]) {
        self.id = id
        self.familyName = familyName
        self.personalName = personalName
        self.addresses = adresses
    }
    
    func alter(_ changes:Change...) -> Self { changes.reduce(self) { $0.alter($1) } }
    private func alter(_ change:Change) -> Self {
        switch change {
        case let .set     (.family   (  familyName   )): return .init(id, familyName, personalName, addresses                                                                                               )
        case let .set     (.personal (personalName   )): return .init(id, familyName, personalName, addresses                                                                                               )
        case let .add     (.address  (address        )): return .init(id, familyName, personalName, addresses                                                 + [address]                                   )
        case let .update  (.address  (address        )): return .init(id, familyName, personalName, addresses.filter { $0.id != address.id                  } + [address]                                   )
        case let .update  (.addresses(updated        )): return .init(id, familyName, personalName, addresses.filter { !addresses.map(\.id).contains($0.id) } + updated                                     )
        case let .reassign(.address  (address,to:user)): return .init(id, familyName, personalName, addresses.filter { $0.id != address.id                  } + [address.alter(.reassign(.owner(to:user)))] )
        case let .remove  (.addresses(tagged:tag     )): return .init(id, familyName, personalName, addresses.filter { !$0.tags.contains(tag)               }                                               )
        case let .remove  (.tag      (       tag     )): return .init(id, familyName, personalName, addresses.map    { $0.alter(.remove(.tag(named:tag)))   }                                               )
        }
    }
    
    var description: String { "\(familyName ?? "") \(personalName ?? "") \((addresses.count > 0) ? addresses.reduce("") { $0 + "\($1.description)" } : "")" }
}

func newContact() -> Contact { Contact() }
func new(owner:User, tagged:String? = nil) -> Address { Address(owner:owner, tag:tagged) }


let c0 = newContact()
    .alter(
        .set(.family(name:"Meyer")),
        .set(.personal(name:"Manuel")),
        .add(
            .address(new(owner:.one)
                .alter(
                    .set(.street (to:"Oude Blekerstraat 99")),
                    .set(.city   (to:"Groningen"           )),
                    .set(.zip    (to:"9988 GW"             )),
                    .set(.country(to:"The Netherlands"     )),
                    .add(.tag  (name:"work"                )),
                    .add(.tag  (name:"private"             ))))),
        .add(
            .address(new(owner:.two,tagged:"summer house")
                .alter(
                    .set(.city   (to:"London"  )),
                    .set(.zip    (to:"SW1A 1AA")),
                    .set(.country(to:"UK"      )),
                    .add(.tag  (name:"work"    ))))),
        .add(
            .address(new(owner:.two,tagged:"summer house")
                .alter(
                    .set(.city (to:"Vatican City")),
                    .set(.zip  (to:"00120"       )),
                    .add(.tag(name:"private"     )))))
    )


let c1 = c0.alter(.remove(.addresses(tagged:"summer house")) )
let c2 = c0.alter(.remove(.tag(named:"summer house")) )
let c3 = c0.alter(.update(.address(c0.addresses.first!.alter(.reassign(.owner(to:.two))))) )
let c4 = c0.alter(.reassign(.address(c0.addresses.first!,to:.two)) )

print(c0)
print("====")
//print(c1)
//print("====")
//print(c2)
//print("====")
print(c3)
print("====")
print(c4)
//: [Next](@next)
