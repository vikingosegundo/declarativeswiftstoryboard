//: [Previous](@previous)

struct Arithmetic {
    enum CMD {
        case inc
        case dec
        case add(UInt)
        case sub(UInt)
        case mul(UInt)
        case div(UInt)
        case _mul(UInt, UInt)
        case _div(UInt, UInt)
    }
    init() { self.init(0) }
    let value:UInt
    func execute(_ cmds:CMD...) -> Self { cmds.reduce(self) { $0.execute($1)} }
}

private extension Arithmetic {
    init(_ v:UInt) { value = v }
    func execute(_ cmd:CMD) -> Self {   // · axiomatic, ↶ recursive, ⇾ forward from alias
        switch cmd {
        case     .inc      : return .init(value + 1) // ·
        case     .dec      : return .init(value - 1) // ·
        case let .add (x  ): return x == 0 ? self : execute(.inc, .add(x - 1))         // ↶
        case let .sub (x  ): return x == 0 ? self : execute(.dec, .sub(x - 1))         // ↶
        case let ._mul(x,y): return y == 1 ? self : execute(.add(x), ._mul(x, y - 1))  // ↶
        case let ._div(x,y): return                                                    // ↶
            x < y
            ? .init()
            : .init()
                .execute( // 1 + ((x - y)/ y)
                    .inc,                   // 1
                    .add(                   // +
                        Self.init()         // (
                            .execute(
                                .add(x),    // (x
                                .sub(y),    // - y)
                                .div(y)     // / y
                            ).value         // )
                    )
                )
        case let .mul(x): return execute(._mul(value, x))   // ⇾
        case let .div(x): return execute(._div(value, x))   // ⇾
        }
    }
}

// EXAMPLES
let zero = Arithmetic()
let a = zero.execute(.add(1024), .div(64), .mul( 8)         )
let b = zero.execute(.add(   3), .sub( 1), .add( 5), .mul(2))
let c = zero.execute(.add( 125), .mul( 2), .div(50)         )
let d = zero.execute(.add( 125), .div(50), .mul( 2)         )
print("((1024 / 64) * 8) = \(a.value)")
print("(((3-1) + 5) * 2) = \(b.value)")
print("((125 * 2) / 50) = \(c.value)")
print("((125 / 50) * 2) = \(d.value)")
/* Examples Output
 ((1024 / 56) * 8) = 128
 (((3-1) + 5) * 2) = 14
 ((125 * 2) / 50) = 5
 ((125 / 50) * 2) = 4
 */

//: [Next](@next)
