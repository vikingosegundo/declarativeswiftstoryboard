//: [Previous](@previous)

indirect enum Note {
    case note (Pitch, Duration)
    case triplet((Pitch, Pitch, Pitch), Duration)
    case chord([Pitch], Duration)
    case rest(Duration)
    case hold(Note)
    indirect enum Pitch {
        case a,b,c,d,e,f,g
        case flat (Pitch)
        case sharp(Pitch)
    }
    indirect enum Duration {
        case ⅟
        case ½
        case ¼
        case ⅛
        case halfed(Duration)
        case joined(Duration, Duration)
    }
}
prefix operator ♪; prefix func ♪(n: (Note.Pitch, Note.Duration)                        ) -> Note { .note(n.0, n.1)    }
prefix operator ▿; prefix func ▿(x: (Note.Duration, (Note.Pitch,Note.Pitch,Note.Pitch))) -> Note { .triplet(x.1, x.0) }
prefix operator ▬; prefix func ▬(d: Note.Duration                                      ) -> Note { .rest(d)           }
prefix operator ♯; prefix func ♯(note: Note.Pitch                                      ) -> Note.Pitch { .sharp(note) }
prefix operator ♭; prefix func ♭(note: Note.Pitch                                      ) -> Note.Pitch { .flat (note) }
prefix operator ⩀; prefix func ⩀(note: Note                                            ) -> Note       { .hold (note) }
    
let x = [ ♪(.c,.¼),  ♪(.d,.¼),  ♪(.e,.¼), ♪(.f,.¼),  ♪(.g,.¼),  ♪(.a,.¼),  ♪(.b,.¼) ]
let y = [ ♪(.c,.¼),  ♪(.d,.¼), ♪(♭.e,.¼), ♪(.f,.¼), ♪(♭.g,.¼),  ♪(.a,.¼), ♪(♭.b,.¼) ]
let z = [ ♪(.e,.¼), ♪(♯.f,.¼), ♪(♯.g,.¼), ♪(.a,.¼),  ♪(.b,.¼), ♪(♯.c,.¼), ♪(♯.d,.¼) ]

let s = [
    ▿(.⅛,(.g,.g,.g)), ⩀(♪( .e,.½)), ▬(.⅛),
    ▿(.⅛,(.f,.f,.f)), ⩀(♪(♭.d,.½)), ▬(.⅛)
]

//: [Next](@next)

