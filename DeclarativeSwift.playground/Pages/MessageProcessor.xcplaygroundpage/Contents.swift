//: [Previous](@previous)

//import Foundation

enum Message {
    case noop
    case print(String)
    case execute(() ->())
}

protocol Logger: AnyObject {
    func log(_ s: String)
    var line: Int { get set }
}

extension Logger {
    func log(_ s: String) { }
}
final
class DefaultLogger: Logger { var line = 0 }

func process(logger l: Logger = DefaultLogger()) -> (Message) -> () {
    func printLogger() -> Logger {
        final
        class PrintLogger: Logger {
            var line = 0
            func log(_ s: String) {
                print("[\(line)]> \(s)")
            }
        }
        return PrintLogger()
    }
    let logger = (l is DefaultLogger) ? printLogger() : l
    var m: Message = .noop {
        didSet {
            switch m {
            case .noop            : logger.log("no operation"                      )
            case .print  (let   s): logger.log("incoming message: \(s)"            )
            case .execute(let exe): logger.log("execute: "); exe()
            }
            logger.line = logger.line + 1
        }
    }
    return { msg in m = msg }
}

let processor = process()

processor( .print("hey ho — let's go!")     )
processor( .noop                            )
processor( .execute { print("Make it so.")} )
processor( .print("Energize")               )



//: [Next](@next)
