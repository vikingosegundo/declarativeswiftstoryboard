//: [Previous](@previous)

import Foundation

struct FBState {
    let fizzCount    :Int
    let buzzCount    :Int
    let fizzBuzzCount:Int
    enum Change {
        case count(_Count);  enum _Count {
            case up(_Up)  ;  enum _Up    {
                case fizz,
                     buzz,
                     fizzBuzz } }
    }
    init() { self.init(fizzCount: 0, buzzCount: 0, fizzBuzzCount: 0) }
    func alter(_ change:Change) -> Self {
        switch change {
        case .count(.up(.fizz    )): return .init(fizzCount: fizzCount + 1, buzzCount: buzzCount,     fizzBuzzCount: fizzBuzzCount    )
        case .count(.up(.buzz    )): return .init(fizzCount: fizzCount,     buzzCount: buzzCount + 1, fizzBuzzCount: fizzBuzzCount    )
        case .count(.up(.fizzBuzz)): return .init(fizzCount: fizzCount,     buzzCount: buzzCount,     fizzBuzzCount: fizzBuzzCount + 1)
        }
    }
    private
    init( fizzCount f:Int, buzzCount b:Int, fizzBuzzCount fb:Int) {
        fizzCount     = f
        buzzCount     = b
        fizzBuzzCount = fb
    }
}

func fizzbuzz(_ range: Range<Int> = (1..<101), fizzValue: Int = 3, buzzValue: Int = 5, state:FBState = FBState()) -> String {
    var state = state
    func fizz() -> String { "Fizz" }
    func buzz() -> String { "Buzz" }
    func concatenate(_ x: String,_ l: () -> String...) -> String { x + l.reduce("") { $0 + $1() } + ", " }
    return range.reduce("") { (p, n) in
        switch (n % fizzValue, n % buzzValue) {
        case (0, 0): state = state.alter(.count(.up(.fizzBuzz))) ; return concatenate(p, fizz, buzz)
        case (0, _): state = state.alter(.count(.up(.fizz    ))) ; return concatenate(p, fizz      )
        case (_, 0): state = state.alter(.count(.up(    .buzz))) ; return concatenate(p,       buzz)
        case (_, _):                                               return concatenate(p, {"\(n)"}  )
        }
    }
    .trimmingCharacters(in: .whitespacesAndNewlines + .punctuationCharacters)
    .appending("\n\n")
    .appending("fizzes    : \(state.fizzCount    )\n")
    .appending("buzzes    : \(state.buzzCount    )\n")
    .appending("fizzbuzzes: \(state.fizzBuzzCount)\n")
}
func +(lhs:CharacterSet, rhs:CharacterSet) -> CharacterSet { lhs.union       (rhs) }
func -(lhs:CharacterSet, rhs:CharacterSet) -> CharacterSet { lhs.intersection(rhs) }

print(fizzbuzz(1..<151))

//: [Next](@next)
