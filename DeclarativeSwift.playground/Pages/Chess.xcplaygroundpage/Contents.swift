//: [Previous](@previous)

enum Chess {
    enum MoveDescriptor {
        case move(Piece, from:Coordinate, to:Coordinate)
    }
    typealias Move = (MoveDescriptor, () -> ())
    typealias Coordinate = (v:Int,h:Int)
    enum Player:Equatable { case white, black}
    enum Piece:Equatable {
        case pawn  (Player)
        case rook  (Player)
        case knight(Player)
        case bishop(Player)
        case queen (Player)
        case king  (Player)
    }
    struct Board: CustomStringConvertible {
        fileprivate let fields: [Field]
        var description: String { sortedFields(for:self).enumerated().reduce("") { $0 + "\(charFor(field:$1.element))" + (($1.offset % 8 == 7) ? "\n" : " ") } }
    }
    static func createPopulatedBoard() -> Board {
        [   populateWhitePawns,populateWhiteRooks,populateWhiteKnights,populateWhiteBishops,populateWhiteKing,populateWhiteQueen,
            populateBlackPawns,populateBlackRooks,populateBlackKnights,populateBlackBishops,populateBlackKing,populateBlackQueen
        ].reduce(Chess.createEmptyBoard()) {
            $1($0)
        }
    }
}
fileprivate extension Chess.Board {
     enum Change {
        case update(Update); enum Update {
            case field(with:Chess.Field)
        }
    }
    func alter(_ cs:Change...) -> Self { cs.reduce(self) {  $0.alter($1) } }
}
private extension Chess.Board {
    func alter(_ c:Change) -> Self {
        switch c {
        case let .update(.field(with:field)):
            return .init(fields:fields.filter{ $0.coordinate.v != field.coordinate.v || $0.coordinate.h != field.coordinate.h } + [field])
        }
    }
}
fileprivate extension Chess {
    struct Field {
        let coordinate: Coordinate
        let value:Value
        enum Value:Equatable {
            case empty
            case occupied(with:Piece)
        }
    }
    static func allCoordiantes() -> [Coordinate] { (0..<64).map { (v:$0/8,h:$0%8) } }
    static func createEmptyBoard() -> Board { .init(fields:allCoordiantes().map { Field(coordinate:$0, value:.empty) }) }
    static func allPossibleWhitePawnMoveDescriptions() -> [MoveDescriptor] {
          (0..<64).map { .move(.pawn(.white), from:($0/8,$0%8), to:($0/8+1,$0%8)) }
        + (0..<64).map { .move(.pawn(.white), from:($0/8,$0%8), to:($0/8+2,$0%8)) }
        + (0..<64).map { .move(.pawn(.white), from:($0/8,$0%8), to:($0/8+1,$0%8-1)) }
        + (0..<64).map { .move(.pawn(.white), from:($0/8,$0%8), to:($0/8+1,$0%8+1)) }
    }
    static func allPossibleBlackPawnMoveDescriptions() -> [MoveDescriptor] {
          (0..<64).map { .move(.pawn(.black), from:($0/8,$0%8), to:($0/8-1,$0%8)) }
        + (0..<64).map { .move(.pawn(.black), from:($0/8,$0%8), to:($0/8-2,$0%8)) }
        + (0..<64).map { .move(.pawn(.black), from:($0/8,$0%8), to:($0/8-1,$0%8-1)) }
        + (0..<64).map { .move(.pawn(.black), from:($0/8,$0%8), to:($0/8-1,$0%8+1)) }
    }
    
}
fileprivate
func charFor(field: Chess.Field) -> String.Element {
    switch field.value {
    case .empty                         : return " "
    case .occupied(with:.pawn  (.white)): return "♙"
    case .occupied(with:.pawn  (.black)): return "♟"
    case .occupied(with:.rook  (.white)): return "♖"
    case .occupied(with:.rook  (.black)): return "♜"
    case .occupied(with:.knight(.white)): return "♘"
    case .occupied(with:.knight(.black)): return "♞"
    case .occupied(with:.bishop(.white)): return "♗"
    case .occupied(with:.bishop(.black)): return "♝"
    case .occupied(with:.king  (.white)): return "♔"
    case .occupied(with:.king  (.black)): return "♚"
    case .occupied(with:.queen (.white)): return "♕"
    case .occupied(with:.queen (.black)): return "♛"
    }
}
fileprivate
func populateWhitePawns(on board:Chess.Board) -> Chess.Board {
    var board = board
    (0..<8).forEach { board = board.alter(.update(.field(with:.init(coordinate:($0,1), value:.occupied(with:.pawn(.white)))))) }
    return board
}
fileprivate
func populateWhiteRooks(on board:Chess.Board) -> Chess.Board {
    var board = board
    board = board.alter(.update(.field(with:.init(coordinate:(0,0), value:.occupied(with:.rook(.white))))))
    board = board.alter(.update(.field(with:.init(coordinate:(7,0), value:.occupied(with:.rook(.white))))))
    return board
}
fileprivate
func populateWhiteKnights(on board:Chess.Board) -> Chess.Board {
    var board = board
    board = board.alter(.update(.field(with:.init(coordinate:(1,0), value:.occupied(with:.knight(.white))))))
    board = board.alter(.update(.field(with:.init(coordinate:(6,0), value:.occupied(with:.knight(.white))))))
    return board
}
fileprivate
func populateWhiteBishops(on board:Chess.Board) -> Chess.Board {
    var board = board
    board = board.alter(.update(.field(with:.init(coordinate:(2,0), value:.occupied(with:.bishop(.white))))))
    board = board.alter(.update(.field(with:.init(coordinate:(5,0), value:.occupied(with:.bishop(.white))))))
    return board
}
fileprivate
func populateWhiteKing(on board:Chess.Board) -> Chess.Board {
    var board = board
    board = board.alter(.update(.field(with:.init(coordinate:(3,0), value:.occupied(with:.king(.white))))))
    return board
}
fileprivate
func populateWhiteQueen(on board:Chess.Board) -> Chess.Board {
    var board = board
    board = board.alter(.update(.field(with:.init(coordinate:(4,0), value:.occupied(with:.queen(.white))))))
    return board
}
fileprivate
func populateBlackPawns(on board:Chess.Board) -> Chess.Board {
    var board = board
    (0..<8).forEach { board = board.alter(.update(.field(with:.init(coordinate:($0,6), value:.occupied(with:.pawn(.black)))))) }
    return board
}
fileprivate
func populateBlackRooks(on board:Chess.Board) -> Chess.Board {
    var board = board
    board = board.alter(.update(.field(with:.init(coordinate:(0,7), value:.occupied(with:.rook(.black))))))
    board = board.alter(.update(.field(with:.init(coordinate:(7,7), value:.occupied(with:.rook(.black))))))
    return board
}
fileprivate
func populateBlackKnights(on board:Chess.Board) -> Chess.Board {
    var board = board
    board = board.alter(.update(.field(with:.init(coordinate:(1,7), value:.occupied(with:.knight(.black))))))
    board = board.alter(.update(.field(with:.init(coordinate:(6,7), value:.occupied(with:.knight(.black))))))
    return board
}
fileprivate
func populateBlackBishops(on board:Chess.Board) -> Chess.Board {
    var board = board
    board = board.alter(.update(.field(with:.init(coordinate:(2,7), value:.occupied(with:.bishop(.black))))))
    board = board.alter(.update(.field(with:.init(coordinate:(5,7), value:.occupied(with:.bishop(.black))))))
    return board
}
fileprivate
func populateBlackKing(on board:Chess.Board) -> Chess.Board {
    var board = board
    board = board.alter(.update(.field(with:.init(coordinate:(3,7), value:.occupied(with:.king(.black))))))
    return board
}
fileprivate
func populateBlackQueen(on board:Chess.Board) -> Chess.Board {
    var board = board
    board = board.alter(.update(.field(with:.init(coordinate:(4,7), value:.occupied(with:.queen(.black))))))
    return board
}
fileprivate
func sortedFields(for board:Chess.Board) -> [Chess.Field] {
    board.fields.sorted {
        $0.coordinate.h == $1.coordinate.h
            ? $0.coordinate.v < $1.coordinate.v
            : $0.coordinate.h < $1.coordinate.h
    }
}

let b = Chess.createPopulatedBoard()
    .alter(
        .update(.field(with:.init(coordinate:(4,1),value:.empty                         ))),
        .update(.field(with:.init(coordinate:(4,3),value:.occupied(with:  .pawn(.white))))),
        .update(.field(with:.init(coordinate:(4,6),value:.empty                         ))),
        .update(.field(with:.init(coordinate:(4,4),value:.occupied(with:  .pawn(.black))))),
        .update(.field(with:.init(coordinate:(3,1),value:.empty                         ))),
        .update(.field(with:.init(coordinate:(3,2),value:.occupied(with:  .pawn(.white))))),
        .update(.field(with:.init(coordinate:(1,7),value:.empty                         ))),
        .update(.field(with:.init(coordinate:(2,5),value:.occupied(with:.knight(.black)))))
    )
print(b)
//: [Next](@next)

//: [Next](@next)
