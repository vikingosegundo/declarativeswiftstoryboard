//: [Previous](@previous)

typealias Move = (
    id      : String,       // id for selection
    position: (Int, Int),   // position on board
    execute : () -> ()      // execute move on board
)
typealias ExclusitivityToken = (redeem:() -> Bool, invalidate:() -> ())
func createMove(position:(Int,Int), token:ExclusitivityToken) -> Move {
    (
        id      : "check(\(position.0).\(position.1)))",
        position: (position.0,position.1),
        execute : { print(token.redeem()) }
    )
}
func createToken() -> ExclusitivityToken {
    var _token = true
    return (
        redeem    : { defer { _token = false }; return _token },
        invalidate: {         _token = false                  })
}

let t0 = createToken()
let t1 = createToken()

let m0 = createMove(position:(0,0),token:t0)
let m1 = createMove(position:(0,1),token:t0)
let m2 = createMove(position:(1,0),token:t0)
let n0 = createMove(position:(1,1),token:t1)
let n1 = createMove(position:(0,1),token:t1)

m1.execute()
m0.execute()
m2.execute()
n0.execute()
n1.execute()
//: [Next](@next)
