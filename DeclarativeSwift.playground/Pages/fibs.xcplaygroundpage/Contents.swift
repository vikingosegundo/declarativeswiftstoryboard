//: [Previous](@previous)
extension Bool {
    var isTrue :Bool { self == true  }
    var isFalse:Bool { self == false }
}
enum FibsError:Error {
    case InputTooSmall(String)
}
func fibs(n:Int, print p:Bool = false) throws -> [(Int, Int)] {
    guard n > 1 else { throw FibsError.InputTooSmall("parameter n must be greater than or equal 2") }
    return (2..<n)
        .reduce( [ (0,1),(1,1) ] ) { $0 + [($1, $0[$1 - 2].1 + $0[$1 - 1].1)] }
        .map { p.isTrue ? print("(\($0.0)): \($0.1)") : (); return $0  }
}

try? fibs(n:12, print:true)

//: [Next](@next)
