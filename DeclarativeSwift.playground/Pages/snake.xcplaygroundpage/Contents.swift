//: [Previous](@previous)

typealias Coordiante = (x:Int, y:Int)
typealias Size = (width:Int, height:Int)
struct Snake {
    init (head:Coordiante) { self.init(head, [], .north) }
    private init(_ head:Coordiante, _ tail:[Coordiante], _ facing:Facing) {
        self.head = head.x > 0 && head.y > 0 ? head : (1,1)
        self.tail = tail
        self.facing = facing
    }
    enum Facing { case north, east, south, west }
    enum Move   { case forward, right, left     }
    enum Change { case move(Move), grow         }
    
    func alter(_ change:Change) -> Self {
        switch change {
        case .move(let direction): return {
            switch (facing, direction) {
            case (.north, .forward): return .init((head.x,     head.y - 1), Array(([head] + tail).prefix(tail.count > 0 ? tail.count  : 0)) ,.north)
            case (.east,  .forward): return .init((head.x + 1, head.y    ), Array(([head] + tail).prefix(tail.count > 0 ? tail.count  : 0)) ,.east)
            case (.south, .forward): return .init((head.x,     head.y + 1), Array(([head] + tail).prefix(tail.count > 0 ? tail.count  : 0)) ,.south)
            case (.west,  .forward): return .init((head.x - 1, head.y    ), Array(([head] + tail).prefix(tail.count > 0 ? tail.count  : 0)) ,.west)
                
            case (.north,   .right): return .init((head.x,     head.y + 1), Array(([head] + tail).prefix(tail.count > 0 ? tail.count  : 0)) ,.east)
            case (.east,    .right): return .init((head.x + 1, head.y    ), Array(([head] + tail).prefix(tail.count > 0 ? tail.count  : 0)) ,.south)
            case (.south,   .right): return .init((head.x - 1, head.y    ), Array(([head] + tail).prefix(tail.count > 0 ? tail.count  : 0)) ,.west)
            case (.west,    .right): return .init((head.x,     head.y - 1), Array(([head] + tail).prefix(tail.count > 0 ? tail.count  : 0)) ,.north)
                
            case (.north,    .left): return .init((head.x - 1, head.y    ), Array(([head] + tail).prefix(tail.count > 0 ? tail.count  : 0)) ,.west)
            case (.east,     .left): return .init((head.x,     head.y + 1), Array(([head] + tail).prefix(tail.count > 0 ? tail.count  : 0)) ,.north)
            case (.south,    .left): return .init((head.x + 1, head.y    ), Array(([head] + tail).prefix(tail.count > 0 ? tail.count  : 0)) ,.east)
            case (.west,     .left): return .init((head.x,     head.y - 1), Array(([head] + tail).prefix(tail.count > 0 ? tail.count  : 0)) ,.south)
            }
        }()
        case .grow: return .init(head, tail.last != nil ? tail + [tail.last!] : tail + [head] ,facing)
        }
    }
    let head:Coordiante
    let tail:[Coordiante]
    let facing:Facing
}
final class Game {
    let size: Size
    var snake:Snake
    var state:State = .ongoing
    enum State {
        case ongoing
        case ended
    }
    init(size s:Size) {
        snake = Snake(head: (s.width / 2, s.height / 2))
        size = s
    }
    func move(_ move:Snake.Move) {
        guard self.state == .ongoing else { return }
        switch move {
        case .forward: snake = snake.alter(.move(.forward)); checkOutOfBounds() ? { state = .ended }() : ();
        case .right  : snake = snake.alter(.move(.right  )); checkOutOfBounds() ? { state = .ended }() : ();
        case .left   : snake = snake.alter(.move(.left   )); checkOutOfBounds() ? { state = .ended }() : ();
        }
    }
    private func checkOutOfBounds() -> Bool {
           (snake.head.x == 0              || snake.head.y == 0              )
        || (snake.head.x == size.width - 1 || snake.head.y == size.height - 1)
    }
}

let game = Game(size: (10,10))
game.snake.head
game.move(.right)
game.move(.forward)
game.snake.head
game.state
game.move(.forward)
game.snake.head
game.state
game.move(.forward)
game.snake.head
game.state
game.move(.forward)
game.snake.head
game.state
game.move(.forward)
game.snake.head
game.state

//: [Next](@next)
