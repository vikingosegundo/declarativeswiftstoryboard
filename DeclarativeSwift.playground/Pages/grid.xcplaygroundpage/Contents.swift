//: [Previous](@previous)

import Foundation

func cube(width w:Int, height h:Int, depth d:Int) -> () -> (Int,Int,Int) {
    var i = -1
    return {
        i = (i + 1) % (w * h * d)
        return (i / ( w * h ), (i / w) % h, i % h)
    }
}
let w = 3
let h = 3
let d = 3

let c = cube(width: w, height: h, depth:d)
var x = 0
for _ in 0 ..< w*h*d {
    x = x+1
    print("\(x) \(c())")
}





print("******")
fileprivate
func grid(width w:Int, height h:Int) -> () -> (Int,Int) { var i = -1; return { i = (i + 1) % (w * h    ); return (i / w        ,              i % h) } }

let g = grid(width: w, height: h)

for _ in 0 ..< w*h {
    print(g())
}
//: [Next](@next)
